#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <climits>
#include <utility>
#include <iomanip>
#define all(v) v.begin(), v.end()
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef pair<int,int> pii;
typedef vector<pii> vpii;

int N, s = 0;

int minleft = INT_MAX, sz = INT_MAX;

void Do(int N, int _s, int left){
    if(N <= 0){
        if(abs(N) < minleft){
            minleft = abs(N);
            sz = _s;
        }else if(abs(N) == minleft && _s < sz){
            minleft = abs(N);
            sz = _s;

        }
        return;
    }
    Do(N - 500 ,_s + 1, 0);
    Do(N - 200, _s + 1, 0);
    Do(N - 100, _s + 1, 0);
}

int main()
{
    cin >> N;
    while(N > 500){
        N -= 500;
        ++s;
    }
    Do(N, s, 0);
    cout << sz << endl;
    /*
    if(N > 400){
        N -= 500;
        ++s;
    }*/


}

