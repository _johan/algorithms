#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <climits>
#include <utility>
#include <iomanip>
#define all(v) v.begin(), v.end()
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef pair<int,int> pii;
typedef vector<pii> vpii;
typedef vector<bool> vb;
typedef vector<vi> vvi;
typedef vector<pii> vvpii;
int N, K, M;
vpii meetings;
vb hasinvent;
vi start;
vi ans;
vvpii edges;

bool Do(int i, int t, int startt){
    bool ret = false;
    for(int j = 0; j < edges[i].size(); ++j){
        bool found = false;
        if(find(start.begin(), start.end(), edges[i][j].second) == start.end()){
            found = true;
        }
        if(edges[i][j].first >= startt && edges[i][j].first <= t && !found){
            if(!Do(edges[i][j].second, edges[i][j].first, startt)){
                return false;
            }
        }
        if(found){
            ret = max((int)Do(edges[i][j].second, edges[i][j].first, startt), (int)ret);
        }
    }
    return ret;
}

int main()
{
    cin >> N >> K;
    hasinvent = vb(N);
    meetings = vpii(K);
    edges = vvpii(N);
    for(int i = 0; i < K; ++i){
        cin >> meetings[i].first >> meetings[i].second;
        meetings[i].first--;
        meetings[i].second--;
        edges[meetings[i].first].push_back(make_pair(i, meetings[i].second));
        edges[meetings[i].second].push_back(make_pair(i,(meetings[i].first));
    }

    cin >> M;
    start = vi(M);
    ans.reserve(M);
    for(int i = 0; i < M; ++i){
        int index;
        cin >> index;
        index--;
        hasinvent[index] = true;
        start[i] = index;
    }
    //Check for each "node" if it's start

    /*vb hinv(N, false);
    for(int i = 0; i < M; ++i){
        fill(hinv.begin(), hinv.end(), false);
        hinv[start[i]] = true;
        for(int j = 0; j < K; ++j){
            hinv[meetings[j].first] = max((int)hinv[meetings[j].second], (int)hinv[meetings[j].first]);
            hinv[meetings[j].second] = hinv[meetings[j].first];
            bool found = true;
            for(int c = 0; c < N; ++c){
                if(hinv[c] != hasinvent[c]){
                    found = false;
                    break;
                }
            }
            if(found){
                ans.push_back(start[i]);
                break;
            }
        }
    }*/
    vb hinv(N, false);
    for(int i = 0; i < M; ++i){
        //Måste hitta första mötet med någon i starts
        //Hitta en startedge
        fill(hinv.begin(), hinv.end(), false);
        hinv[start[i]] = true;
        for(int j = 0; j < edges[start[i]].size(); ++j){
            if(find(start.begin(), start.end(), edges[start[i]][j]) != start.end()){
                //j is potential start index
                bool valid = true;
                for(int k = edges[start[i]][j].first; k < K; ++k){
                    bool met = max((int)hinv[meetings[k].first], (int)hinv[meetings[k].second]);
                    vi::iterator fit = find(start.begin(), start.end(), meetings[k].first);
                    vi::iterator sit = find(start.begin(), start.end(), meetings[k].second);
                    if(met && fit != start.end() && sit != start.end()){
                        //This is valid
                    }else if(met){
                        //Break
                        valid = false;
                        break;
                    }
                }
                if(valid){
                    ans.push_back(start[i]);
                    break;
                }
            }
        }
    }
    for(int i = 0; i < ans.size(); ++i){
        cout << ans[i]+1 << endl;
    }
}

