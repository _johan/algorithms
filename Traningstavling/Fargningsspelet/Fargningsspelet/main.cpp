#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <climits>
#include <utility>
#include <iomanip>
#include <map>
#define all(v) v.begin(), v.end()
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef pair<int,int> pii;
typedef vector<pii> vpii;
typedef vector<vi> vvi;

int P1 = 0, P2 = 1, E = 3;
int N;
map<vector<int>, int> drag;
//map<vector<int>, int> dp;

inline int alt(int p){
    if(p == P1){
        return P2;
    }
    return P1;
}
int t;

vvi dp;
int Do(vi gb, int t, int P){
    if(dp[P][t] != -1){
        return dp[P][t];
    }
    /*
    map<vi,int>::iterator mip = drag.find(gb);
    if(mip != drag.end()){
        return mip->second;
    }*/
    //Lagg overallt som är tillgängligt
    int ret = -1, d = 0;
    if(P == P2)
        ret = 1;
    for(int i = 0; i < N; ++i){
        int minindex = i-1, maxindex = i+1;
        if(t == )
        if(gb[i] == E && gb[max(i-1, 0)] != P && gb[min(i+1, N-1)] != P){
            gb[i] = P;
            int val = Do(gb,t+1, alt(P));
            gb[i] = E;
            if(P == P1 && val > ret){
                ret = val;
                d = i;
            }else if(P == P2 && val < ret){
                ret = val;
            }
        }
    }
    dp[P][t] = ret;
    if(P == P1){
        drag[gb] = d;
    }
    return ret;
}
int main()
{
    cin >> N >> t;
    dp = vvi(2, vi(N+1, -1));
    vi gb(N, E);
    int s;
    cin >> s;
    if(s == -1){
        return 0;
    }
    --s;
    gb[s] = P2;
    Do(gb, 0, P1);
    int m = drag[gb];
    gb[m] = P1;
    cout << m +1<< endl;
    while(cin >> s){
        if(s == -1){
            return 0;
        }
        s--;
        gb[s] = P2;
        int dr = drag[gb];
        gb[dr] = P1;
        cout << dr+1 << endl;
    }
}

