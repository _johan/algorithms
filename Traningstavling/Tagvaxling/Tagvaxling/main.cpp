#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <climits>
#include <utility>
#include <iomanip>
#define all(v) v.begin(), v.end()
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef pair<int,int> pii;
typedef vector<pii> vpii;

int N;
vi desired;
bool found = false;

void printarr(vi &v){
    for(int i : v){
        cout << i << " ";
    }
    cout << endl;
}
/*
void Do(vi &cur, vi &queue, int pos){
    if(cur.size() >= N){
        //Is this correct?
        printarr(cur);
        for(int i = 0; i < N; ++i){
            if(cur[i]+1 != desired[i]){
                return;
            }
        }
        found = true;
        return;
    }
    if(pos < N-1){
        //Last train cant go queue
        queue.push_back(pos);
        Do(cur, queue, pos+1);
        queue.pop_back();
    }
    //Add from queue
    if(queue.size() > 0){
        cur.push_back(queue.front());
        int val = queue.front();
        queue.erase(queue.begin());
        Do(cur, queue, pos);
        queue.insert(queue.begin(), val);
        cur.pop_back();
    }
    if(pos < N){
        cur.push_back(pos);
        Do(cur, queue, pos+1);
        cur.pop_back();
    }
    /*
    cur.push_back(pos);
    Do(cur, queue, pos+1);
    cur.pop_back();
    if(queue.size() > 0){
        cur.push_back(queue.front());
        queue.pop_back();
        Do(cur, queue, pos+1);
    }
    queue.push_back(pos+1);
    Do(cur, queue, pos+1);
    queue.pop_back();*/
    //Could check for correct so far
//}

int main()
{
    cin >> N;
    desired = vi(N);
    for(int i = 0; i < N; ++i){
        cin >> desired[i];
    }

    vi trains(N);
    for(int i = 0; i < N; ++i){
        trains[i] = i+1;
    }
    //trains.reserve(N);
    vi qu;
    qu.reserve(N);
    int occ = 0;
    for(int i = 0; i < N; ++i){
        if(trains[i] != desired[i]){
            //Move all trains in front to queue
            if(qu.size() > 0 && qu.back() == desired[i]){
                qu.pop_back();
                ++occ;
                continue;
            }
            int j = i;
            while(trains[j] != desired[i]){
                if(j >= N){
                    cout << "NEJ" << endl;
                    return 0;
                }
                if(trains[j] == -1){
                    ++j;
                    continue;
                }
                qu.push_back(trains[j]);
                trains[j] = -1;
                ++j;
            }
            trains[j] = -1;
        }
    }
    cout << "JA" << endl;
    /*Do(trains, qu, 0);
    if(found){
        cout << "JA" << endl;
        return 0;
    }
    cout << "NEJ" << endl;
*/}

