#include "kingdom.h"
#include <stdio.h>
#include <vector>
#include <iostream>
//#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

int N, P;
vector<int> calls;

void parts(int R[]) {
	calls.assign(R, R + N);
}

int main() {
	int ignore = scanf("%d%d", &N, &P);
	calls.resize(N);
	int *C = new int[N],* F = new int[N - 1], *T = new int[N - 1];
	for (int i = 0; i < N; i++) ignore = scanf("%d", C + i);
	for (int i = 0; i < N-1; i++) ignore = scanf("%d", F + i);
	for (int i = 0; i < N-1; i++) ignore = scanf("%d", T + i);
	int ret = division(N,P,C,F,T);
	printf("%d\n", ret);
	if (ret) {
		for (int i = 0; i < N; ++i) {
			printf("%d ", calls[i]);
		}
		puts("");
	}
	system("PAUSE");
}
