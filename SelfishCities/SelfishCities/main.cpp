#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <climits>
//http://www.spoj.com/problems/SCITIES/
struct Trip{
	Trip() : x(0), y(0), z(0){ };
	Trip(int _x, int _y, int _z): x(_x), y(_y), z(_z){};
	int x;
	int y;
	int z;
};

bool comp(const Trip& lhs, const Trip &rhs){
	if (lhs.z == rhs.z){
		if (lhs.x == rhs.x){
			return lhs.y > rhs.y;
		}
		return lhs.x > rhs.x;
	}
	return lhs.z > rhs.z;
}



int Do(std::vector<Trip> &test, std::vector<bool> &bought, std::vector<bool> &sold, int numBought, int numSold, int tot, int lowerbound){
	if (numSold == sold.size()){
		return tot;
	}
	if (numBought == bought.size()){
		return tot;
	}
	int add = tot;
	for (int i = 0; i < test.size(); i++){
		if (!sold[test[i].x - lowerbound] && !bought[test[i].y - lowerbound]){
			sold[test[i].x - lowerbound] = true;
			bought[test[i].y - lowerbound] = true;
			add = std::max(Do(test, bought, sold, numBought++, numSold++, tot + test[i].z, lowerbound), add);
			sold[test[i].x - lowerbound] = false;
			bought[test[i].y - lowerbound] = false;
		}
	}
	return add;
}

int main(){
	int N;
	std::cin >> N;
	std::vector<std::vector<Trip>> tests(N);
	std::vector<std::pair<int, int>> pair(N);
	std::vector<std::pair<int, int>> ulbound(N, std::make_pair(INT_MAX, 0));
	std::vector<int> val(N, 0);
	for (int i = 0; i < N; i++){
		int f, l;
		std::cin >> f >> l;
		pair[i] = std::make_pair(f, l);
		int c1 = 1, c2 = 1, g = 1;
		while (true){
			std::cin >> c1 >> c2 >> g;
			if (c1 == 0 && c2 == 0 && g == 0){
				break;
			}
			c1--;
			c2--;
			ulbound[i].first = std::min(ulbound[i].first, std::min(c1, c2));
			ulbound[i].second = std::max(ulbound[i].second, std::max(c1, c2));
			tests[i].push_back(Trip(c1, c2, g));
		}
	}

	/*
	for (int i = 0; i < N; i++){
		std::sort(tests[i].begin(), tests[i].end(), comp);
		int orglbound = ulbound[i].first;
		ulbound[i].second -= orglbound;
		ulbound[i].first -= orglbound;
		std::vector<bool> bought(ulbound[i].second+1, false);
		std::vector<bool> sold(ulbound[i].second+1, false);
		for (int c = 0; c < tests[i].size(); c++){
			if (!sold[tests[i][c].y] && !bought[tests[i][c].x]){
				val[i] += tests[i][c].z;
				sold[tests[i][c].y] = true;
				bought[tests[i][c].x] = true;
			}
		}
	}
	*/
	for (int i = 0; i < N; i++){
		int orglbound = ulbound[i].first;
		ulbound[i].second -= orglbound;
		ulbound[i].first -= orglbound;

		std::vector<bool> b(ulbound[i].second+1, false);
		std::vector<bool> s(ulbound[i].second+1, false);
		std::cout << Do(tests[i], b, s, 0,0,0, ulbound[i].first) << std::endl;
	}
#ifdef WIN32
	system("PAUSE");
#endif
}