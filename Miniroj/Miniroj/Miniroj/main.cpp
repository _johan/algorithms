#include <iostream>
#include <string>
#include <set>
#include <vector>

using namespace std;


set<vector<bool>> mines;


void Do(int pos, string &curgb, vector<bool> &mine){
	if (pos >= curgb.size()){
		//See if valid
		bool valid = true;
		for (int i = 0; i < curgb.size(); i++){
			if (curgb[i] > '0'){
				valid = false;
			}
		}
		if (valid){
			mines.insert(mine);
		}
		return;
	}
	Do(pos + 1, curgb, mine);
	if (pos > 0 && pos < curgb.size()-1){
		if (curgb[pos - 1] >= '1' && curgb[pos] >= '1' && curgb[pos + 1] >= '1'){
			--curgb[pos - 1];
			--curgb[pos];
			--curgb[pos + 1];
			mine[pos] = true;
			Do(pos + 1, curgb, mine);
			++curgb[pos - 1];
			++curgb[pos];
			++curgb[pos + 1];
			mine[pos] = false;
		}
	}
	else if (pos > 0){
		if (curgb[pos - 1] >= '1' && curgb[pos] >= '1'){
			--curgb[pos - 1];
			--curgb[pos];
			mine[pos] = true;
			Do(pos + 1, curgb, mine);
			++curgb[pos - 1];
			++curgb[pos];
			mine[pos] = false;
		}
	}
	else if (pos < curgb.size() - 1){
		if (curgb[pos] >= '1' && curgb[pos + 1] >= '1'){
			--curgb[pos];
			--curgb[pos + 1];
			mine[pos] = true;
			Do(pos + 1, curgb, mine);
			++curgb[pos];
			++curgb[pos + 1];
			mine[pos] = false;
		}
	}
}

int main(){
	string in;
	std::cin >> in;
	std::vector<bool> b(in.size());
	Do(0, in, b);
	vector<int> res(in.size(), 0);
	for (vector<bool> vb : mines){
		for (int i = 0; i < vb.size(); i++){
			if (vb[i]){
				++res[i];
			}
		}
	}
	for (int i = 0; i < res.size(); i++){
		if (res[i] == 0){
			std::cout << 'S';
		}
		else if (res[i] == 1){
			cout << 'X';
		}
		else{
			cout << 'O';
		}
	}
	cout << endl;
#ifdef WIN32
	system("PAUSE");
#endif
}