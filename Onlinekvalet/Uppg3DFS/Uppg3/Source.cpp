#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <algorithm>
std::set<std::string>::iterator it;
/*
int Do(std::vector<std::vector<char>> &e, std::string t, std::vector<bool> used, int pos){
    //Flytta runt bokstaven
    if(pos == e.size()){
        for(int i = 0; i < used.size(); i++){
            if(!used[i]){
                return 0;
            }
        }
        return 1;
    }*/
    /*if(used[pos]){
        for(int i = 0; i < t.size(); i++){
            if(!t[i]){
                return 0;
            }
        }
        return 1;
    }*/
    /*int success = 0;
    for(int c = 0; c < e.size(); c++){
        bool failed = true;
        if(used[c])
            continue;
        for(int i = 0; i < e[c].size(); i++){
            if(e[c][i] == t[pos]){
                failed = false;
                used[c] = true;
                success = Do(e, t, used, pos+1);
                if(success == 1){
                    return 1;
                }
                used[c] = false;
            }
        }*/
       /* if(failed){
            return 0;
        }*/
    /*}

    for(int i = 0; i < used.size(); i++){
        if(!used[i]){
            return 0;
        }
    }
    return 1;


}
*/



bool ispossible(std::vector<std::vector<int>> &charpos, std::vector<bool> &tarnused, std::vector<bool> &used, int pos){
    bool ret = false;

    if(pos == charpos.size()){
        ret = true;
        /*for(unsigned int i = 0; i < used.size(); i++){
            if(!used[i]){
                ret = false;
                break;
            }
        }*/
        return ret;
    }
    if(ret){
        return ret;
    }

    //if(ispossible(charpos, tarnused, used, pos+1))
    //    return true;
    if(used[pos]){
        return false;
    }
    for(unsigned int i = 0; i < charpos[pos].size(); i++){
        //Use this for this char
        if(tarnused[charpos[pos][i]]){
            continue;
        }
        tarnused[charpos[pos][i]] = true;
        used[pos] = true;
        if(ispossible(charpos, tarnused, used, pos+1))
            return true;
        tarnused[charpos[pos][i]] = false;
        used[pos] = false;
    }
    return false;
}

bool compvec(const std::vector<int> &lhs, const std::vector<int> &rhs){
    if(lhs.size() == rhs.size()){
        return &lhs < &rhs;
    }
    return lhs.size() < rhs.size();
}


int main(){
    std::cout.sync_with_stdio(false);
    int N, K, M;
    //std::cout << "Maxsize: " << std::numeric_limits<std::streamsize>::max() << std::endl;
    std::cin >> N >> K >> M;
    std::vector<std::string> tarn(N);
    std::vector<std::string> word(M);
    //std::map<std::string,int> word;
    for(int i = 0; i < N; i++){
        std::string s;
        std::cin >> s;
        std::sort(s.begin(), s.end());
        tarn[i] = s;
    }
    for(int i = 0; i < M; i++){
        std::string s;
        std::cin >> s;
        std::sort(s.begin(), s.end());
        word[i] = s;
        //std::cin>>word[i];
        //word.insert(s);
    }
    //for(std::string s : word){
    //    std::cout << s << std::endl;
    //}
    std::vector<std::vector<int>> char_pos(N);
    for(int i = 0; i < N; i++){
        char_pos[i].reserve(N);
    }
    std::vector<bool> used(N, false);
    std::vector<bool> tarnused(N, false);
    std::map<std::vector<int>, int> same;
   // std::map<std::vector<std::vector<int>>, bool> map;
   // std::map<std::vector<std::vector<int>>, bool>::iterator mip;
    int total = 0;
    for(std::string string : word){
        for(int o = 0; o < N; o++){
            for(int j = 0; j < N; j++){
                std::size_t found = tarn[j].find(string[o]);
                if(found != std::string::npos){
                    //This letter exists on this
                    char_pos[o].push_back(j);
                    tarnused[j] = true;
                    //break;
                }
            }
            same[char_pos[o]]++;
        }
        bool _do = true;
        for(int o = 0; o < N; o++){
            if(!tarnused[o])
                _do = false;
            if(char_pos[o].size() == 0)
                _do = false;
            tarnused[o] = false;
        }
        for(std::pair<std::vector<int>, int> p : same){
            if(p.first.size() < p.second){
                _do = false;
            }
        }

       /* for(int o = 0; o < N; o++){
            for(int k = 0; k < N; k++){
                bool exists = false;
                if(o == k)
                    continue;
                for(int l = 0; l < N; l++){
                    if(char_pos[o][k] == char_pos[k][l]){
                        exists = true;
                    }
                }
                if(exists){

                }
                //if(std::find(char_pos[o].begin(), char_pos[o].end, char_pos[k][o])){
                    //Om den inte existerar
                //}
            }
        }*/
        if(_do){
            std::sort(char_pos.begin(), char_pos.end(), compvec);
            if(ispossible(char_pos, tarnused, used, 0)){
                total++;
            }
        }/*else{
            std::cout << "Do is false" << std::endl;
        }*/

        for(int o = 0; o < N; o++){
            used[o] = false;
            tarnused[o] = false;
        }
        same.clear();
        char_pos.clear();
        char_pos.resize(N);
        //Vilka ställen finns bokstaven på?
        //Om den bara finns på en är den tärningen använd
    }

        //Om alla har en bokstav med och alla bokstäver existerar
        //Now check if we can combine these to have one be true in all fields
        //Finns det något sätt de här kan bli utvalda att fungera?
    std::cout << total << std::endl;
    //std::cout << "notallfull: " << _allfull << std::endl;
   // Do(tarn, init, word, 0, 0);
    //std::cout << M - word.size()  << std::endl;
    //Om ett ord ska finnas måste en av deras bokstav finnas på varje tärning
}
