#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <algorithm>
#include <queue>
#include <map>
#include <cmath>
#include <functional>
#include <unordered_map>
#include <stack>

using namespace std;

#define range(x,s,e) for(int x=s;x<e;++x)
#define all(v) v.begin(), v.end()
typedef unsigned long long ull;
typedef long long ll;
typedef vector<int> vi;
typedef vector< vi > vvi;
typedef vector<bool> vb;
typedef pair<int, int> pii;
typedef pair<ll, ll> pll;
typedef vector<pii> vpii;

const double pi = 3.14159265358979323846;

#define MAX(a,b) ((a > b) ? a : b)
#define MIN(a,b) ((a < b) ? a : b)

#define vectorMax(v) (max_element(all(v)) - v.begin());

/*

^ y
|
|       x
L------->

*/

int n;
ull k;

vector<pair<ll,ll>> houses;
vector<vector<pair<ll, int>>> h; //alla (y-pos, i) på den x sorterad efter stigande y

pll startPos;
pll goalPos;
int startX;
int startI;
int start;
int goal;
vector<bool> vis;
struct State
{
    int x;
    int i;
    int id;

    State(int x, int i, int id) : x(x), i(i), id(id) {}
};
bool Can(ull k)
{
    fill(all(vis), false);

    stack<State> q;
    q.push(State(startX, startI, start));
    vis[start] = true;
    while (!q.empty())
    {
        int x = q.top().x;
        int id = q.top().id;
        int curi = q.top().i;
        ll y = h[x][q.top().i].first;
        q.pop();

        if (id == goal)
            return true;

        /*if (vis[id])
            continue;
        vis[id] = true;*/


        if (x != 0)
        {
            int lower = MAX(0, lower_bound(all(h[x - 1]), make_pair(y, (1<<30))) - h[x-1].begin() - 1);
            range(i, lower, h[x-1].size())
            {
                auto& o = h[x - 1][i];

                ll oy = o.first;
                if (curi != h[x].size() - 1) //cur inte överst
                    if (oy >= h[x][curi + 1].first - 1)
                        break; //vi tar oss inte till det här eller något ovanför

                if (vis[o.second])
                    continue;

                if (i != h[x - 1].size() - 1) //o inte överst
                    if (h[x - 1][i + 1].first < y + 2) //blockerar den ovanför?
                        continue;

                if (llabs(oy - y) < k)
                {
                    vis[o.second] = true;
                    q.push(State(x - 1, i, o.second));
                }
                else if (oy > y)
                {
                    break;
                }
            }
        }
        if (x < h.size() - 1)
        {
            int lower = MAX(0, lower_bound(all(h[x + 1]), make_pair(y, (1 << 30))) - h[x + 1].begin() - 1);
            range(i, lower, h[x + 1].size())
            {
                auto& o = h[x + 1][i];

                ll oy = o.first;
                if (curi != h[x].size() - 1) //cur inte överst
                    if (oy >= h[x][curi + 1].first - 1)
                        break; //vi tar oss inte till det här eller något ovanför

                if (vis[o.second])
                    continue;

                if (i != h[x + 1].size() - 1) //o inte överst
                    if (h[x + 1][i + 1].first < y + 2) //blockerar den ovanför?
                        continue;

                if (llabs(oy - y) < k)
                {
                    vis[o.second] = true;
                    q.push(State(x + 1, i, o.second));
                }
                else if (oy > y)
                {
                    break;
                }
            }
        }
    }

    return false;
}

//#include "HighResTimer.h"

int main()
{
    std::ios::sync_with_stdio(false);
    cin.tie(0);
    cout.tie(0);

    cin >> n >> k;

    houses.resize(n);
    vis.resize(n);
    range(i, 0, n)
    {
        ll x, y;
        cin >> x;
        cin >> y;
        houses[i].first = x;
        houses[i].second = y;
    }

    //VE::HighResTimer timer;
    //timer.Start();

    startPos = houses[0];
    goalPos = houses.back();

    /*if (abs(startPos.first - goalPos.first) + 1 > n)
    {
        cout << "NATT";
        //system("pause");
        return 0;
    }*/

    sort(all(houses));
    vi indices(n);
    int rank = 0;
    indices[0] = rank;
    range(i, 1, n)
    {
        if (houses[i].first == houses[i - 1].first) //husen är på samma x
        {
            indices[i] = indices[i - 1];
        }
        else if (houses[i].first - houses[i - 1].first > 1) //mellanrum mellan husen, måste bevaras
        {   //Varför behåller du mellanrummen? Om det är ett mellanrum borde det väl gå att exita direkt?
            rank += 2;
            indices[i] = rank;
        }
        else //de är bredvid varann
        {
            ++rank;
            indices[i] = rank;
        }
    }

    h = vector<vector<pair<ll, int>>>(rank + 1);
    int goalX;
    range(i, 0, n)
    {
        h[indices[i]].push_back(pair<ll, int>(houses[i].second, i));
        if (houses[i] == startPos)
        {
            startX = indices[i];
            startI = h[indices[i]].size() - 1;
            start = i;
        }
        if (houses[i] == goalPos)
        {
            goalX = indices[i];
            goal = i;
        }
    }

    range(i, MIN(startX, goalX), MAX(startX, goalX))
    {
        if (h[i].empty())
        {
            cout << "NATT" << endl;
            return 0;
        }
    }

    ull l = 0;
    ull r = k;

    while (l != r)
    {
        ull m = (l + r) / 2;
        bool b = Can(m + 1);
        if (b)
            r = m;
        else
            l = m + 1;
    }

    if (l < k)
        cout << l << endl;
    else
        cout << "NATT" << endl;

    //cout << "Time: " << timer.GetElapsed() << endl;

    //system("pause");
    return 0;
}
