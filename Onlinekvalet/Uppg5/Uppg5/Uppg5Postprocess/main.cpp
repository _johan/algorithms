#include <iostream>
#include <fstream>
#include <string>


int main()
{
    std::string inputfile;
    std::string output;
    std::cout << "Filename?" << std::endl;
    std::cin >> inputfile;
    std::ifstream ifs(inputfile);
    if(!ifs.is_open()){
        std::cout << "File failed to open" << std::endl;
    }
    std::string s;
    std::string former;
    int same = 1;
    while(std::getline(ifs, s)){
        if(s.compare("") != 0 && s.compare(former) == 0){
            same++;
        }else if(s.compare("") != 0 && s.compare(former) != 0){
            if(same > 1){
                output.append((std::string)"for " + std::to_string(same) + (std::string)" {\n" + former + (std::string)"\n}\n");
            }else{
                output.append(former);
                output.append("\n");
            }
            same = 1;
            former = s;
        }
    }
    if(same > 1){
        output.append((std::string)"for " + std::to_string(same) + (std::string)" {\n" + former + (std::string)"\n}\n");
    }else{
        output.append(former);
        output.append("\n");
    }
    ifs.close();
    std::ofstream ofs("output");
    ofs << output;
    ofs.close();
    return 0;
}

