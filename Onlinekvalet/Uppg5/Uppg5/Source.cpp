#include <iostream>
#include <vector>
#include <fstream>
#include <set>
#include <unistd.h>
typedef long long ll;
/*
Vill inte att vis ska ha olika fält beroende på hur den står. Det blir senare isf
*/
enum MoveType{
    TURN_LEFT,
    TURN_RIGHT,
    FORWARD
};

enum CurRot{
    NORTH,
    SOUTH,
    EAST,
    WEST
};

struct TMove{
    static std::string conv(MoveType m){
        if(m == TURN_LEFT){
            return "right";
        }else if(m == TURN_RIGHT){
            return "left";
        }else{
            return "forward";
        }
    }
};

void RotatePair(std::pair<int,int> &p, MoveType m){
    if(m == FORWARD)
        return;
    if(p.first != 0){
        //It's currently in the xdirection
        if(m == TURN_LEFT){
            p.second = p.first;
            p.first = 0;
        }else{
            //m == RIGHT
            p.second = -p.first;
            p.first = 0;
        }
    }else{
        //It's currently in the ydirection
        if(m == TURN_LEFT){
            p.first = -p.second;
            p.second = 0;
        }else{
            p.first = p.second;
            p.second = 0;
        }
    }
}

void RotateRot(CurRot &rot, MoveType m){
    std::pair<int,int> p;
    if(rot == NORTH){
        p.second = -1;
    }else if(rot == SOUTH){
        p.second = 1;
    }else if(rot == EAST){
        p.first = 1;
    }else{
        p.first = -1;
    }
    RotatePair(p, m);
    if(p.first == 1){
        rot = CurRot::EAST;
    }else if(p.first == -1){
        rot = CurRot::WEST;
    }else if(p.second == -1){
        rot = CurRot::NORTH;
    }else{
        rot = CurRot::SOUTH;
    }
}

struct Path{
    friend bool operator< (const Path& lhs, const Path& rhs){
        if(lhs.moves.size() != rhs.moves.size()){
            return lhs.moves.size() < rhs.moves.size();
        }
        if(lhs.current_forward.first != rhs.current_forward.first){
            return lhs.current_forward.first < rhs.current_forward.first;
        }
        if(lhs.current_forward.second != rhs.current_forward.second){
            return lhs.current_forward.second < rhs.current_forward.second;
        }

        for(int i = 0; i < lhs.moves.size(); i++){
            for(int x = 0; x < rhs.moves.size(); x++){
                if(lhs.moves[i] != rhs.moves[x]){
                    return lhs.moves[i] <= rhs.moves[x];
                }
            }
        }
        return false;
    }

    Path(){}
    Path(Path &p, MoveType extramove){
        moves = p.moves;
        //if(m == -1)
        current_forward = p.current_forward;
        //else
        //    current_forward = m;
        moves.push_back(extramove);
        if(extramove != MoveType::FORWARD){
            //This means we want to "turn"
            RotatePair(current_forward, extramove);
        }
        //If extramove is turning, change current_forward
    }
    void addMove(MoveType extramove){
        moves.push_back(extramove);
        if(extramove != MoveType::FORWARD){
            //This means we want to "turn"
            RotatePair(current_forward, extramove);
        }

    }

    std::vector<MoveType> moves;
    std::pair<int,int> current_forward;
    //std::vector<std::vector<bool>>
};

std::set<Path> completedPaths;

int H, W;

inline bool insidegraph(std::pair<int,int> &pos){
    if(pos.first >= 0 && pos.first < W && pos.second >= 0 && pos.second < H){
        return true;
    }
    return false;
}

template<class T>
inline const T& getIndex(const std::pair<int,int> &pos, const std::vector<std::vector<T>> &gb){
    return gb[pos.second][pos.first];
}
std::pair<int,int> AddPair(const std::pair<int,int> &p1, const std::pair<int,int> &p2){
    return std::make_pair(p1.first + p2.first, p1.second + p2.second);
}

CurRot GetCorresponding(CurRot mt){
    if(mt == CurRot::NORTH)
        return CurRot::SOUTH;
    if(mt == CurRot::SOUTH)
        return CurRot::NORTH;
    if(mt == CurRot::WEST)
        return CurRot::EAST;
    return CurRot::WEST;
}
void printArr(std::vector<std::vector<char>> &gb, std::pair<int,int> pos){
    for(int y = 0; y < gb.size(); y++){
        for(int x = 0; x < gb[y].size(); x++){
            if(pos.first == x && pos.second == y){
                std::cout << "R";
            }else{
                std::cout << gb[y][x];
            }
        }
        std::cout << std::endl;
    }
}

bool DFS(Path curpath, std::pair<int,int> curpos, std::vector<std::vector<char>> &gb, std::vector<std::vector<std::vector<bool>>> &vis, const std::pair<int,int> &end, CurRot rot, int steps){
    if(curpos == end){
        //We made it
        completedPaths.insert(curpath);
        return true;
    }
    //printArr(gb, curpos);
    //std::cout << "\n\n" << std::endl;
    //sleep(1);
    vis[curpos.second][curpos.first][rot] = true;
    std::pair<int,int> npos = curpos;
    npos= AddPair(npos, curpath.current_forward);
    if(insidegraph(npos) && getIndex(npos, gb) == '.' && !getIndex(npos, vis)[rot] && !getIndex(npos, vis)[GetCorresponding(rot)]){
        if(DFS(Path(curpath, FORWARD), npos, gb, vis, end, rot, steps+1))
            return true;
    }
    //Turn left and right
    npos = curpos;
    CurRot nrot = rot;
    RotateRot(nrot, TURN_LEFT);
    if(!getIndex(npos, vis)[nrot]){
        if(DFS(Path(curpath, TURN_LEFT), curpos, gb, vis, end, nrot, steps+1))
            return true;
    }
    nrot = rot;
    RotateRot(nrot, TURN_RIGHT);
    if(!getIndex(npos, vis)[nrot]){
        if(DFS(Path(curpath, TURN_RIGHT), curpos, gb, vis, end, nrot, steps+1))
            return true;
    }
    return false;
}

int main(){
    std::ifstream in("robot_path_freedom2.in");
    std::streambuf *cinbuf = std::cin.rdbuf();
    std::cin.rdbuf(in.rdbuf());

    std::string s;
    std::cin >> s;
    std::pair<int,int> robotpos, endpos;
    CurRot rot;
    std::cin >> H >> W;
    std::vector<std::vector<char>> gb(H, std::vector<char>(W, '.'));
    for(int i = 0; i < H; i++){
        std::string r;
        std::cin >> r;
        for(int x = 0; x < W; x++){
            if(r[x] == '^' || r[x] == '>' || r[x] == '<' || r[x] == 'v'){
                //This is the robots position
                robotpos = std::make_pair(x, i);
                if(r[x] == '^'){
                    rot = NORTH;
                }else if(r[x] == '>'){
                    rot = EAST;
                }else if(r[x] == '<'){
                    rot = WEST;
                }else{
                    rot = SOUTH;
                }
            }else if(r[x] == 'M'){
                endpos = std::make_pair(x,i);
            }else if(r[x] == '#'){
                gb[i][x] = '#';
            }
        }
    }
    Path defPath;
    std::pair<int,int> initrot;
    if(rot == NORTH){
        initrot.second = -1;
    }else if(rot == SOUTH){
        initrot.second = 1;
    }else if(rot == WEST){
        initrot.first = -1;
    }else{
        initrot.first = 1;
    }
    defPath.current_forward =initrot;
    defPath.moves.reserve(H*W);
    std::vector<std::vector<std::vector<bool>>> vis(H, std::vector<std::vector<bool>>(W, std::vector<bool>(4, false)));
    DFS(defPath, robotpos, gb, vis, endpos, rot, 0);
    for(Path p : completedPaths){
        std::vector<std::pair<ll,MoveType>> mtvec;
        mtvec.reserve(p.moves.size());
        ll numlast = 0;
        MoveType last = p.moves.front();
        for(MoveType mt : p.moves){
            if(mt == last){
                numlast++;
            }else{
                mtvec.push_back(std::make_pair(numlast, last));
                numlast = 1;
                last = mt;
            }
        }
        //Combine them into a nicer format
        std::cout << "forwardtwo:\n forward \n forward \n return\n";
        std::cout << "forwardthree:\n for 3 { \n forward \n } \n return\n";
        std::cout << "main:\n";

        for(std::pair<ll, MoveType> mt : mtvec){
            std::string o = TMove::conv(mt.second);
            if(o.compare("right") == 0 && mt.first == 3){
                o = "left";
                std::cout << "left\n";
            }else if(o.compare("left") == 0 && mt.first == 3){
                o = "right";
                std::cout << "right\n";
            }else if(o.compare("forward") == 0){
                //This is forward
                //if(mt.first > 1){
                if(mt.first == 3){
                    std::cout << "call forwardthree\n";
                    mt.first-=3;
                }else if(mt.first == 2){
                    std::cout << "call forwardtwo\n";
                    mt.first-=2;
                }else if(mt.first == 1){
                    std::cout << "forward\n";
                    mt.first--;
                }else{
                    std::cout << "for " << mt.first << " {\n forward \n } \n";
                }
                //}else{
                 //   std::cout << "forward\n";
               // }
            }else{
                for(int i = 0; i < mt.first; i++){
                    std::cout << o << std::endl;
                }
            }
            //std::cout << o<< std::endl;
        }
        std::cout << "forward" << std::endl;
        std::cout << std::endl << std::endl;
    }
}
/*
robot
10 10
>.........
..........
..........
..........
..........
..........
..........
..........
..........
.........M

*/
