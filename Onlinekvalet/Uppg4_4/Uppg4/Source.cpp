#include <iostream>
#include <vector>
#include <tuple>
#include <utility>
#include <set>
#include <algorithm>
#include <climits>
#include <map>
#include <fstream>
typedef long long ll;
typedef unsigned long long ull;

struct Edge{
    //std::pair<ll,ll> lhs;
    //std::pair<ll,ll> rhs;
    ll lhs,rhs, val;
    Edge(){}
    Edge(ll lhs, ll rhs, ll val){
        this->lhs = lhs;
        this->rhs = rhs;
        this->val = val;
    }
    friend bool operator < (const Edge &lhs, const Edge &rhs){
        if(lhs.val == rhs.val){
            if(lhs.lhs == rhs.lhs){
                return lhs.rhs < rhs.rhs;
            }
            return lhs.lhs < rhs.lhs;
        }
        return lhs.val < rhs.val;
    }
};

struct it_comp{
    bool operator() (const std::set<ll>::iterator &lhs, const std::set<ll>::iterator &rhs){
        return (*lhs) < (*rhs);
    }
};

std::set<std::set<ll>::iterator, it_comp> prev;
std::set<ll> lookingfor;
bool setBoth(std::vector<std::set<ll>> &merged, std::vector<std::pair<bool, bool>> &both, ll pos, bool first){
    //prev.reserve(merged.size());
    if(first && both[pos].first){
        //This has already been set
        return false;
    }else if(!first && both[pos].second){
        return false;
    }
    if(first)
        both[pos].first = true;
    else
        both[pos].second = true;
    if(both[pos].first && both[pos].second)
        return true;
    lookingfor.insert(merged[pos].begin(), merged[pos].end());

    for(std::set<ll>::iterator it = lookingfor.begin(); it != lookingfor.end(); ++it){
        ll i = *it;
        if(first && !both[i].first){
            //This has already been set
            both[i].first = true;
            if(both[i].first && both[i].second){
                return true;
            }
            lookingfor.insert(merged[i].begin(), merged[i].end());
            lookingfor.erase(i);
            it = lookingfor.begin();
        }else if(!first && !both[i].second){
            both[i].second = true;
            if(both[i].first && both[i].second){
                return true;
            }

            lookingfor.insert(merged[i].begin(), merged[i].end());
            lookingfor.erase(i);
            it = lookingfor.begin();

        }
    }
    lookingfor.clear();
    return false;
}

int main(){
    std::cout.sync_with_stdio(false);
    //std::ifstream in("input.test");
    //std::streambuf *cinbuf = std::cin.rdbuf();
    //std::cin.rdbuf(in.rdbuf());
    ll N, K;
    std::cin >> N >> K;
    std::vector<std::pair<ll,ll>> houses(N);
    std::pair<ll,ll> startcoords, endcoords;
    for(ll i = 0; i < N; i++){
        std::cin >> houses[i].first >> houses[i].second;
    }
    //std::cout << "Done reading..." << std::endl;
    //std::cout << "num houses: " << N << std::endl;
    startcoords = houses.front();
    endcoords = houses.back();
    if(N == 1 && K != 0){
        std::cout << 0 << std::endl;
        return 0;
    }
    if(K == 0){
        std::cout << "NATT" << std::endl;
        return 0;
    }
    std::sort(houses.begin(), houses.end());
    ll shift = houses.front().first, _start = houses.front().first, _end = houses.back().first;
    ll m_size = _end - _start+1;
    startcoords.first-=shift;
    endcoords.first-=shift;
    ll lastcoord = 0, sequencestart = 0;
    bool startinsequence = false, endinsequence = false;
    for(ll i = 0; i < N; i++){
        houses[i].first -= shift;
        if(houses[i].first - lastcoord > 1){ //Doesn't work
            //We have a gap
            if(startinsequence && endinsequence){
                //This is the sequence we want
                houses.erase(houses.begin()+i, houses.end());
                break;
            }
            if((startinsequence && !endinsequence) || (!startinsequence && endinsequence)){
                std::cout << "NATT" << std::endl; //They can't reach each other
                return 0;

            }
            houses.erase(houses.begin() + sequencestart, houses.begin() + i);
            i = 0;
            sequencestart = i;
            lastcoord = houses[i].first;
        }else{
            lastcoord = houses[i].first;
        }
        if(houses[i].first == startcoords.first && houses[i].second == startcoords.second){
            startinsequence = true;
        }
        if(houses[i].first == endcoords.first && houses[i].second == endcoords.second){
            endinsequence = true;
        }
    }
    if((startinsequence && !endinsequence) || (!startinsequence && endinsequence)){
        std::cout << "NATT" << std::endl; //They can't reach each other. It's impossible
        return 0;

    }
    ll gbsize = houses.size();
    std::vector<std::set<std::pair<ll,ll>>> nodes(m_size);
    ll index = 0;
    ll startindex = 0, endindex = 0;
    for(std::pair<ll,ll> p : houses){
        if(p == startcoords){
            startindex = index;
        }else if(p == endcoords){
            endindex = index;
        }
        nodes[p.first].insert(std::make_pair(p.second, index++));
    }


    std::map<ll, std::vector<Edge>> edges;
    //edges.reserve(N*2);
    std::set<std::pair<ll,ll>>::iterator sllit;
    std::set<std::pair<ll,ll>>::iterator sllit2;
    for(ll i = 0; i < m_size-1; i++){ //i == x
        for(sllit = nodes[i].begin(); sllit != nodes[i].end(); ++sllit){
            ll upper = LONG_MAX, lower = LONG_MAX; //lower is a coord
            ll lowerindex;
            bool lowerEdgeAvail = true;
            if((sllit2 = std::next(sllit)) != nodes[i].end()){
                upper = sllit2->first;
            }
            for(std::pair<ll,ll> y2 : nodes[i+1]){
                if(y2.first == sllit->first || y2.first == sllit->first+1){
                    lowerindex = LONG_MAX;
                    lowerEdgeAvail = false;
                }
                //std::cout << sllit->first << std::endl;

                if(y2.first >= upper-1){
                    break; //y major order
                }

                if(sllit->first > y2.first && (y2.first > lower || lower == LONG_MAX) && y2.first+1 < upper){
                    //This could be a lower edge
                    lower = y2.first;
                    lowerindex = y2.second;
                }else if(y2.first+1 < upper && y2.first >= sllit->first){
                    edges[std::abs(y2.first - sllit->first)].push_back(Edge(y2.second, sllit->second, std::abs(y2.first - sllit->first)));
                }
            }
            if(lower != LONG_MAX && lowerEdgeAvail){
                edges[std::abs(lower - sllit->first)].push_back(Edge(sllit->second, lowerindex, std::abs(lower - sllit->first)));
            }
        }
    }
    //std::sort(edges.begin(), edges.end());
    std::vector<std::set<ll>> merged(index); //Negativa tal betyder att man ska gå till det indexet istället
    std::vector<std::pair<bool,bool>> containsboth(index, std::make_pair(false, false));
    containsboth[startindex].first = true;
    containsboth[endindex].second = true;
    ll time = LONG_MAX;
    for(std::pair<ll, std::vector<Edge>> _p : edges){
        if(_p.first >= K){
            std::cout << "NATT" << std::endl;
            return 0;
        }
        for(Edge p : _p.second){
            /*if(p.lhs == startindex){

            }
            if(goalstartchange.first.find(p.lhs) != goalstartchange.first.end()){
                //This has been connected to a startnode.
            }*/
            merged[p.lhs].insert(p.rhs); //Det är bara då vi "ansluter" mål, startnoden eller nod som mött dem som vi kan "vinna". När vi ansluter en målnod
            //När en nod blivit ansluten till mål eller startnod, "redirecta" deras index på alla noder till mål resp. startnoden
            //}else{
            merged[p.rhs].insert(p.lhs);

            /*
             Actually I probably only need to merge when they are start or end coords, maybe not even then and only do the containsboth vector. NVM
    */
            bool done = false;
            if(containsboth[p.lhs].first){
                done = std::max(setBoth(merged, containsboth, p.rhs, true), done);
                //metstart.push_back(p.rhs);
                //containsboth[p.rhs.first].first = true;
            }
            if(containsboth[p.rhs].first && !done){
                done = std::max(done, setBoth(merged, containsboth, p.lhs, true));
                //metstart.push_back(p.lhs);

                //containsboth[p.lhs.first].first = true;
            }
            if(containsboth[p.lhs].second && !done){
                done = std::max(done, setBoth(merged, containsboth, p.rhs, false));

                //containsboth[p.rhs.first].second = true;

            }
            if(containsboth[p.rhs].second && !done){
                done = std::max(done, setBoth(merged, containsboth, p.lhs, false));

                //containsboth[p.lhs.first].second = true;
            }
            if((containsboth[p.lhs].first && containsboth[p.lhs].second) || (containsboth[p.rhs].first && containsboth[p.rhs].second) || done){
                time = _p.first;
                if(time >= K){
                    std::cout << "NATT" << std::endl;
                    return 0;
                }
                std::cout << time << std::endl;

                return 0;
            }
        }

    }
    if(time >= K){
        std::cout << "NATT" << std::endl;
        return 0;
    }
    std::cout << time << std::endl;
}
/*
7 10
1 1
3 2
2 0
2 2
2 4
1 4
0 5
*/
/*
8 6
0 0
1 1
1 3
1 5
2 4
2 2
3 0
2 0
*/
/*
11 1000000000
0 1
1 0
1 3
1 5
1 7
1 9
2 2
2 4
2 6
2 8
0 10
*/
/*
5 5
1 2
2 2
2 0
1 5
3 6

4
*/
/*
2 3
0 0
1 2
*/
/*
20 5
0 0
0 4
0 8
1 2
1 6
2 0
2 4
2 8
3 2
3 6
4 0
4 4
4 8
5 2
5 6
6 0
6 4
6 6
7 2
7 6
*/
