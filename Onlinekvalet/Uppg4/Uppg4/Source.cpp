#include <iostream>
#include <vector>
#include <map>
#include <utility>
#include <set>
#include <climits>
#include <algorithm>
typedef long long ll;
typedef std::pair<ll, std::pair<ll,ll>> Node;
/*
struct Node{
    Node(){}

    std::pair<ll,ll> pos;
    ll val;
    ll index;
};*/
inline bool verifyInBounds(ll x, ll y, ll upperbound, ll rightbound){
    //upperbound+=2;
    //rightbound++;
    if(x >= 0 && x < rightbound && y >= 0 && y < upperbound){
        return true;
    }
    return false;
}

const ll HOUSE = -1;


inline ll check(ll x, ll y, std::vector<std::pair<ll,ll>> &gb){
    ll min = LONG_MAX;
    for(std::pair<ll,ll> c : gb){
        if(y > c.second && c.first == x){
            min = std::min(min, y - c.second );
        }
    }
    return min;
}

inline bool verify(ll x, ll y, std::vector<std::vector<ll>> &gb, ll min){
    if(x >= 0 && x < gb[0].size() && y >= 0 && y < gb.size()){
        if(gb[y][x] > min){
            gb[y][x] = min;
            return true;
        }
    }
    return false;
}

void printarr(std::vector<std::vector<ll>> &gb){
    for(std::vector<ll> vec : gb){
        for(ll l : vec){
            std::cout << l << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl<< std::endl;
}

bool compare(const std::pair<ll,ll> &lhs, const std::pair<ll,ll> &rhs){
    if(lhs.first == rhs.first){
        return lhs.second < rhs.second;
    }
    return lhs.first < rhs.first;
}

int main(){
    ll N, K;
    std::cin >> N >> K;
    std::vector<std::pair<ll,ll>>  _house_init(N);
    std::pair<ll,ll> start, end;
    ll upperbound = 0, rightbound = 0;
    for(ll i = 0; i < N; i++){
        std::pair<ll,ll> coord;
        std::cin >> coord.first >> coord.second;
        if(i == 0){
            start = coord;
        }else if(i == N-1){
            end = coord;
        }
        upperbound = std::max(upperbound, coord.second+2);
        rightbound = std::max(rightbound, coord.first+2);
        _house_init[i] = coord;
    }

    ll lastcoord = _house_init[0].first, sequencestart = 0;
    bool startinsequence = false, endinsequence = false;
    std::sort(_house_init.begin(), _house_init.end(), compare);
    for(ll i = 0; i < N; i++){
        if(_house_init[i].first - lastcoord > 1){ //Doesn't work
            //We have a gap
            if(startinsequence && endinsequence){
                //This is the sequence we want
                _house_init.erase(_house_init.begin()+i, _house_init.end());
                break;
            }
            if((startinsequence && !endinsequence) || (!startinsequence && endinsequence)){
                std::cout << "NATT" << std::endl; //They can't reach each other
                return 0;
            }
            _house_init.erase(_house_init.begin() + sequencestart, _house_init.begin() + i);
            i = 0;
            sequencestart = i;
            lastcoord = _house_init[i].first;
        }else{
            lastcoord = _house_init[i].first;
        }
        if(_house_init[i].first == start.first && _house_init[i].second == start.second){
            startinsequence = true;
        }
        if(_house_init[i].first == end.first && _house_init[i].second == end.second){
            endinsequence = true;
        }
    }
    if((startinsequence && !endinsequence) || (!startinsequence && endinsequence)){
        std::cout << "NATT" << std::endl; //They can't reach each other. It's impossible
        return 0;
    }

    ll shift = _house_init.front().first;
    rightbound = _house_init.back().first - shift + 2;
    std::vector<std::vector<ll>> gb(upperbound, std::vector<ll>(rightbound, K));
    std::vector<std::vector<bool>> vis(upperbound, std::vector<bool>(rightbound, false));
    end.first-= shift;
    start.first-= shift;
    for(ll i = 0; i < _house_init.size(); i++){
        //ll orgx = houses[i].first;
        _house_init[i].first -= shift;
        //dist[houses[i].first][houses[i].second] = (LONG_MAX);
        //gb[houses[i].first].push_back(houses[i].second);
        //vis[houses[i].first][houses[i].second] = (false);
        //if(houses[i].first == start.first && houses[i].second == start.second){
        //    dist[houses[i].first][houses[i].second] = 0;
        //    q.insert(std::make_pair(0, std::make_pair(houses[i].first, houses[i].second)));
        //}else{
        //    q.insert(std::make_pair(LONG_MAX, std::make_pair(houses[i].first, houses[i].second)));
        //}
    }
    for(ll i = 0; i < N; i++){
        gb[_house_init[i].second][_house_init[i].first] = HOUSE;
    }
    start.second++;
    end.second++;
    gb[start.second][start.first] = 0;
    ll exit = upperbound * rightbound;
    for(ll i = 0; i < exit; i++){
        ll lowestx = 0, lowesty = 0, lowestval = K;
        for(ll o = 0; o < upperbound; o++){
            for(ll k = 0; k < rightbound; k++){
                if(gb[o][k] < lowestval && !vis[o][k] && gb[o][k] != HOUSE){
                    lowestval = gb[o][k];
                    lowesty = o;
                    lowestx = k;
                }
            }
        }
        vis[lowesty][lowestx] = true;
        //std::cout << "Checking: { " << lowestx << ", " << lowesty << " }. val= " << gb[lowesty][lowestx] << std::endl;
        //printarr(gb);
        if(lowesty == end.second && lowestx == end.first){
            if(gb[lowesty][lowestx] == K){
                std::cout << "NATT" << std::endl;
                return 0;
            }
            std::cout << gb[lowesty][lowestx] << std::endl;
            return 0;
        }
        //Check neighbours
        ll low = gb[lowesty][lowestx], newx = lowestx, newy = lowesty;
        newx++;
        low = std::max(low, check(newx, newy, _house_init)-1);
        verify(newx, newy, gb, low);

        newx = lowestx, newy = lowesty, low = gb[lowesty][lowestx];
        newx--;
        low = std::max(low, check(newx, newy, _house_init)-1);
        verify(newx, newy, gb, low);


        newx = lowestx, newy = lowesty, low = gb[lowesty][lowestx];
        newy++;
        low = std::max(low, check(newx, newy, _house_init)-1);
        verify(newx, newy, gb, low);

        newx = lowestx, newy = lowesty, low = gb[lowesty][lowestx];
        newy--;
        low = std::max(low, check(newx, newy, _house_init)-1);
        verify(newx, newy, gb, low);

    }
    if(gb[end.second][end.first] == K){
        std::cout << "NATT" << std::endl;
        return 0;
    }
    std::cout << gb[end.second][end.first] << std::endl;
    return 0;

}
