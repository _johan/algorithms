#include <iostream>
#include <vector>
#include <map>
#include <utility>
#include <set>
#include <climits>
#include <algorithm>
/*
Verkar som att höga avstånd kan ha korrelation med varför det blir fel.
*/
typedef long long ll;
typedef std::pair<ll, std::pair<ll,ll>> Node;
/*
struct Node{
    Node(){}

    std::pair<ll,ll> pos;
    ll val;
    ll index;
};*/
inline bool verifyInBounds(ll x, ll y, ll upperbound, ll rightbound){
    //upperbound+=2;
    //rightbound++;
    if(x >= 0 && x < rightbound && y >= 0 && y < upperbound){
        return true;
    }
    return false;
}


void printarr(std::vector<std::vector<ll>> &gb){
    for(std::vector<ll> vec : gb){
        for(ll l : vec){
            std::cout << l << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl<< std::endl;
}

bool compare(const std::pair<ll,ll> &lhs, const std::pair<ll,ll> &rhs){
    if(lhs.first == rhs.first){
        return lhs.second < rhs.second;
    }
    return lhs.first < rhs.first;
}

int main(){
    std::cout.sync_with_stdio(false);
    ll N, K;
    std::cin >> N >> K;
    std::vector<std::pair<ll,ll>>  houses(N);
    //std::map<ll,std::vector<ll>> _house_map;
    std::pair<ll,ll> start, end;
    //ll upperbound = 0, rightbound = 0;
    for(ll i = 0; i < N; i++){
        std::pair<ll,ll> coord;
        std::cin >> coord.first >> coord.second;
        if(i == 0){
            start = coord;
        }else if(i == N-1){
            end = coord;
        }
        houses[i] = coord;
        //_house_map[coord.first].push_back(coord.second);
        //upperbound = std::max(upperbound, coord.second+2);
        //rightbound = std::max(rightbound, coord.first+2);
        //_house_init[i] = coord;
    }
    if(N == 1 && K != 0){
        std::cout << 0 << std::endl;
        return 0;
    }
    if(N == 1 && K == 0){
        std::cout << "NATT" << std::endl;
        return 0;
    }
    ll lastcoord = houses[0].first, sequencestart = 0;
    bool startinsequence = false, endinsequence = false;
    std::sort(houses.begin(), houses.end(), compare);
    for(ll i = 0; i < N; i++){
        if(houses[i].first - lastcoord > 1){ //Doesn't work
            //We have a gap
            if(startinsequence && endinsequence){
                //This is the sequence we want
                houses.erase(houses.begin()+i, houses.end());
                break;
            }
            if((startinsequence && !endinsequence) || (!startinsequence && endinsequence)){
                std::cout << "NATT" << std::endl; //They can't reach each other
                return 0;
            }
            houses.erase(houses.begin() + sequencestart, houses.begin() + i);
            i = 0;
            sequencestart = i;
            lastcoord = houses[i].first;
        }else{
            lastcoord = houses[i].first;
        }
        if(houses[i].first == start.first && houses[i].second == start.second){
            startinsequence = true;
        }
        if(houses[i].first == end.first && houses[i].second == end.second){
            endinsequence = true;
        }
    }
    if((startinsequence && !endinsequence) || (!startinsequence && endinsequence)){
        std::cout << "NATT" << std::endl; //They can't reach each other. It's impossible
        return 0;
    }

    //We now have a vector, now to combine it to mult vector
    std::vector<std::vector<ll>> gb(houses.back().first - houses.front().first+1);
    std::set<Node> q;
    std::vector<std::map<ll, ll>> dist(houses.back().first - houses.front().first+1);
    //All coords get shiftet to the left with houses.back().first
    ll shift = houses.front().first;
    end.first-= shift;
    start.first-= shift;
    for(ll i = 0; i < houses.size(); i++){
        //ll orgx = houses[i].first;
        houses[i].first -= shift;
        dist[houses[i].first][houses[i].second] = (LONG_MAX);
        gb[houses[i].first].push_back(houses[i].second);
        //vis[houses[i].first][houses[i].second] = (false);
        if(houses[i].first == start.first && houses[i].second == start.second){
            dist[houses[i].first][houses[i].second] = 0;
            q.insert(std::make_pair(0, std::make_pair(houses[i].first, houses[i].second)));
        }else{
            q.insert(std::make_pair(LONG_MAX, std::make_pair(houses[i].first, houses[i].second)));
        }
    }
    //Graphs been built
    //printarr(gb);
    std::map<ll,ll>::iterator it;
    std::set<Node>::iterator sit;
    ll time = LONG_MAX;
    while(!q.empty()){
        Node cur = *q.begin();
        if(cur.second.first == end.first && cur.second.second == end.second){
            //if(cur.first == LONG_MAX)
            time = std::min(cur.first, time);
            //break;
        }
      //  vis[cur.first][cur.second] = true;
        q.erase(q.begin());
        ll upper = LONG_MAX;
        for(ll i = 0; i < gb[cur.second.first].size(); i++){
            if(gb[cur.second.first][i] > cur.second.second && gb[cur.second.first][i]- cur.second.second < upper){
                upper = gb[cur.second.first][i];
            }
        }
        //1st is edgeweight, 2nd is height
        std::pair<ll,ll> lowerEdge = std::make_pair(-1, -1); //Is this what I want to do?
        if(cur.second.first < dist.size()-1){
            //This checks everything to the right
            for(ll i = 0; i < gb[cur.second.first+1].size(); i++){
                if(gb[cur.second.first+1][i] <= cur.second.second+1){
                    //This is a contender for loweredge on the right
                    if(lowerEdge.second < gb[cur.second.first+1][i] && gb[cur.second.first+1][i] < upper-1){
                        //This should be new lowerEdge
                        lowerEdge = std::make_pair(std::abs(cur.second.second - gb[cur.second.first+1][i]), gb[cur.second.first+1][i]);
                    }else if(gb[cur.second.first+1][i] == cur.second.second || gb[cur.second.first+1][i] == cur.second.second+1){
                        //If it's position is the same as currently our same+1 as currently, remove lower edge
                        lowerEdge = std::make_pair(-1,-1);
                    }
                }else if(gb[cur.second.first+1][i] < upper-1){
                    //This is a normal edge
                    if(dist[cur.second.first+1][gb[cur.second.first+1][i]] > std::max(cur.first ,gb[cur.second.first+1][i]- cur.second.second)){
                        sit = q.find(std::make_pair(dist[cur.second.first+1][gb[cur.second.first+1][i]], std::make_pair(cur.second.first+1, gb[cur.second.first+1][i])));
                        if(sit == q.end()){
                            continue;
                        }
                        Node n = *sit;
                        q.erase(sit);
                        dist[cur.second.first+1][gb[cur.second.first+1][i]] = std::max(cur.first ,gb[cur.second.first+1][i]- cur.second.second);
                        n.first = std::max(cur.first ,gb[cur.second.first+1][i]- cur.second.second);
                        q.insert(n);
                    }
                }
            }
            //Handle lowerEdge
            if(lowerEdge.second != -1){
                if(dist[cur.second.first+1][lowerEdge.second] > std::max(cur.first, lowerEdge.first)){
                    Node find = std::make_pair(dist[cur.second.first+1][lowerEdge.second], std::make_pair(cur.second.first+1, lowerEdge.second));
                    sit = q.find(find);
                    if(sit == q.end()){
                        continue;
                    }
                    Node n = *sit;
                    q.erase(sit);
                    dist[cur.second.first+1][lowerEdge.second] = std::max(cur.first, lowerEdge.first);
                    n.first = std::max(cur.first, lowerEdge.first);
                    q.insert(n);

                   // dist[cur.second.first+1][lowerEdge.second] = std::max(cur.first, lowerEdge.first - cur.second.second);
                }
            }
        }
        lowerEdge.first = -1; lowerEdge.second = -1;
        if(cur.second.first > 0){
            //This will check everything to the left
            //This checks everything to the right
            for(ll i = 0; i < gb[cur.second.first-1].size(); i++){
                if(gb[cur.second.first-1][i] <= cur.second.second+1){
                    //This is a contender for loweredge on the right
                    if(lowerEdge.second < gb[cur.second.first-1][i] && gb[cur.second.first-1][i] < upper-1){
                        //This should be new lowerEdge
                        lowerEdge = std::make_pair(std::abs(cur.second.second - gb[cur.second.first-1][i]), gb[cur.second.first-1][i]);
                    }else if(gb[cur.second.first-1][i] == cur.second.second || gb[cur.second.first-1][i] == cur.second.second+1){
                        //If it's position is the same as currently our same+1 as currently, remove lower edge
                        lowerEdge = std::make_pair(-1,-1);
                    }
                }else if(gb[cur.second.first-1][i] < upper-1){
                    //This is a normal edge
                    //std::cout << "Dist: " << dist[cur.second.first-1][gb[cur.second.first-1][i]] << "y: " << gb[cur.second.first-1][i] << std::endl;
                    if(dist[cur.second.first-1][gb[cur.second.first-1][i]] > std::max(cur.first, gb[cur.second.first-1][i] - cur.second.second)){
                        sit = q.find(std::make_pair(dist[cur.second.first-1][gb[cur.second.first-1][i]], std::make_pair(cur.second.first-1, gb[cur.second.first-1][i])));
                        if(sit == q.end()){
                            continue;
                        }
                        Node n = *sit;
                        q.erase(sit);
                        dist[cur.second.first-1][gb[cur.second.first-1][i]] = std::max(cur.first, gb[cur.second.first-1][i] - cur.second.second);
                        n.first = std::max(cur.first, gb[cur.second.first-1][i] - cur.second.second);
                        q.insert(n);
                    }
                }
            }
            //Handle lowerEdge
            if(lowerEdge.second != LONG_MAX){
                if(dist[cur.second.first-1][lowerEdge.second] > std::max(cur.first , lowerEdge.first)){
                    sit = q.find(std::make_pair(dist[cur.second.first-1][lowerEdge.second], std::make_pair(cur.second.first-1, lowerEdge.second)));
                    if(sit == q.end()){
                        continue;
                    }
                    Node n = *sit;
                    q.erase(sit);
                    dist[cur.second.first-1][lowerEdge.second] = std::max(cur.first , lowerEdge.first);
                    n.first = std::max(cur.first , lowerEdge.first);
                    q.insert(n);

                    //dist[cur.second.first-1][lowerEdge.second] = cur.first + lowerEdge.first - cur.second.second;
                }
            }

        }
        //do all egdees. YES
    }
    if(time >= K){
        std::cout << "NATT" << std::endl;
        return 0;
    }
    std::cout << time << std::endl;
}
/*
11 10
2 4
1 3
3 4
4 4
5 3
5 5
5 8
4 6
3 7
1 6
2 7
*/
/*
5 1000000000000
3 1000000
4 999999
4 0
4 1000000000
5 1000

KOLLA SVARET
*/
/*
3 10000
0 2
0 4
1 3
*/
/*
12 100
3 4
3 7
2 3
2 7
4 4
5 5
5 3
5 8
1 4
1 6
0 5
4 6

 * /
 /*
23 100000000000
0 0
0 2
1 1
0 2
2 2
1 3
0 4
2 4
1 5
0 6
2 6
4 0
6 0
5 1
4 2
6 2
5 3
4 4
6 4
5 5
4 6
3 1
6 6
*/
/*
15 3
4 5
3 3
4 3
3 7
2 8
4 9
5 3
6 4
7 5
8 6
9 7
5 6
6 7
7 8
5 9
*/
/*
13 5
4 5
3 3
4 3
5 3
6 4
7 6
8 6
9 7
3 6
2 7
5 8
3 9
4 9
*/
/*
15 3
0 0
1 0
2 0
3 0
4 0
5 0
0 2
2 2
4 2
1 4
3 4
5 4
0 6
2 6
4 6

*/
/*
5 4
0 4
1 5
2 2
2 4
1 3
*/
/*
39 10
0 8
1 8
2 8
4 8
6 8
7 8
0 6
1 6
2 6
4 6
6 6
7 6
8 6
0 4
1 4
2 4
4 4
6 4
7 4
8 4
0 2
1 2
2 2
4 2
6 2
7 2
8 2
5 1
0 0
0 0
1 0
2 0
3 0
4 0
6 0
7 0
8 0
3 6
8 8
*/
/*
9 10
0 8
1 8
2 8
3 0
4 8
5 1
6 8
7 8
8 8
*/
/*
24 10
0 7
2 7
5 7
4 6
6 6
7 5
0 4
3 4
4 4
5 4
6 4
2 3
9 3
4 2
8 2
1 1
5 1
6 1
7 1
0 0
2 0
3 0
4 0
9 7
*/
/*
4 4
0 2
1 3
2 1
1 1
*/
/*
56 2
0 0
0 2
0 4
0 6
0 8
0 10
1 0
1 3
1 5
1 7
1 9
1 11
2 1
2 4
2 6
2 8
2 10
3 0
3 2
3 5
3 7
3 9
3 11
4 1
4 3
4 6
4 8
4 10
5 0
5 2
5 4
5 7
5 9
5 11
6 1
6 3
6 5
6 8
6 10
7 0
7 2
7 4
7 6
7 9
7 11
8 1
8 3
8 5
8 7
8 10
9 0
9 2
9 4
9 6
9 10
9 8
*/
