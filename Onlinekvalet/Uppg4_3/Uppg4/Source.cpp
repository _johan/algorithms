#include <iostream>
#include <vector>
#include <map>
#include <utility>
#include <set>
#include <climits>
#include <algorithm>
typedef long long ll;

struct Node{
    friend bool operator< (const Node &lhs, const Node &rhs){
        if(lhs.val == rhs.val){
            if(lhs.x == rhs.x){
                return lhs.y < rhs.y;
            }
            return lhs.x < rhs.x;
        }
        return lhs.val < rhs.val;
    }

    Node(){
        val = 0;
        x = 0;
        y = 0;
        uppercoord = LONG_MAX;
        loweredgeval = 0;
        loweredgecoord = LONG_MAX;
    }
    Node(ll _val, ll _x, ll _y, ll _index){
        val = _val;
        x = _x;
        y = _y;
        index = _index;
        uppercoord = LONG_MAX;
        loweredgecoord = LONG_MAX;
    }

    ll val;
    ll x;
    ll y;
    ll index;
    ll uppercoord;
    ll loweredgecoord = LONG_MAX;
    ll loweredgeval = 0;
    ll loweredgeindex = 0;
    std::vector<ll> e;
    bool interesting = true;
    void removeEdge(ll index){
        for(ll i = 0; i < e.size(); i++){
            if(e[i] == index){
                e.erase(e.begin()+i);
                return;
            }
        }
    }
};

inline bool verifyInBounds(ll x, ll y, ll upperbound, ll rightbound){
    //upperbound+=2;
    //rightbound++;
    if(x >= 0 && x < rightbound && y >= 0 && y < upperbound){
        return true;
    }
    return false;
}


bool compare(const std::pair<ll,ll> &lhs, const std::pair<ll,ll> &rhs){
    if(lhs.first == rhs.first){
        return lhs.second < rhs.second;
    }
    return lhs.first < rhs.first;
}
//Can I pass vis as a reference?
/*
ll Do(std::vector<Node> n, std::vector<bool> vis, ll pos, ll numvis, const ll end){
    if(numvis == n.size() || pos == end){
        //Done!
        return n[end].val;
    }
    vis[pos] = true;

    ll min = LONG_MAX;
    for(ll i: n[pos].e){
        if(!vis[i] && n[i].val > std::abs(n[i].y - n[pos].y)){
            ll orgval = n[i].val;
            n[i].val = std::max(n[pos].val, std::abs(n[i].y - n[pos].y));
            min = std::min(min, Do(n, vis, i, numvis++, end));
            n[i].val = orgval;
        }
    }
    vis[pos] = false;

    return min;
}
*/
int main(){
    std::cout.sync_with_stdio(false);
    ll N, K;
    std::cin >> N >> K;
    std::vector<std::pair<ll,ll>>  houses(N);
    //std::map<ll,std::vector<ll>> _house_map;
    std::pair<ll,ll> start, end;
    //ll upperbound = 0, rightbound = 0;
    for(ll i = 0; i < N; i++){
        std::pair<ll,ll> coord;
        std::cin >> coord.first >> coord.second;
        if(i == 0){
            start = coord;
        }else if(i == N-1){
            end = coord;
        }
        houses[i] = coord;
        //_house_map[coord.first].push_back(coord.second);
        //upperbound = std::max(upperbound, coord.second+2);
        //rightbound = std::max(rightbound, coord.first+2);
        //_house_init[i] = coord;
    }
    if(N == 1 && K != 0){
        std::cout << 0 << std::endl;
        return 0;
    }
    if(N == 1 && K == 0){
        std::cout << "NATT" << std::endl;
        return 0;
    }
    std::sort(houses.begin(), houses.end(), compare);

    ll lastcoord = houses[0].first, sequencestart = 0;
    bool startinsequence = false, endinsequence = false;
    for(ll i = 0; i < N; i++){
        if(houses[i].first - lastcoord > 1){ //Doesn't work
            //We have a gap
            if(startinsequence && endinsequence){
                //This is the sequence we want
                houses.erase(houses.begin()+i, houses.end());
                break;
            }
            if((startinsequence && !endinsequence) || (!startinsequence && endinsequence)){
                std::cout << "NATT" << std::endl; //They can't reach each other
                return 0;
            }
            houses.erase(houses.begin() + sequencestart, houses.begin() + i);
            i = 0;
            sequencestart = i;
            lastcoord = houses[i].first;
        }else{
            lastcoord = houses[i].first;
        }
        if(houses[i].first == start.first && houses[i].second == start.second){
            startinsequence = true;
        }
        if(houses[i].first == end.first && houses[i].second == end.second){
            endinsequence = true;
        }
    }
    if((startinsequence && !endinsequence) || (!startinsequence && endinsequence)){
        std::cout << "NATT" << std::endl; //They can't reach each other. It's impossible
        return 0;
    }
    //We now have a vector, now to combine it to mult vector
    //std::vector<std::vector<ll>> gb(houses.back().first - houses.front().first+1);
    std::set<Node> q;
    //std::vector<std::map<ll, ll>> dist(houses.back().first - houses.front().first+1);
    std::vector<Node> n(houses.size());
    //std::vector<ll> dist(houses.size());
    //All coords get shiftet to the left with houses.back().first
    ll shift = houses.front().first;
    end.first-= shift;
    start.first-= shift;
    ll startindex = 0, laststart = 0, endindex = 0;
    lastcoord = houses[0].first - shift;
    for(ll i = 0; i < houses.size(); i++){
        //ll orgx = houses[i].first;
        houses[i].first -= shift;
        n[i] = Node(LONG_MAX, houses[i].first, houses[i].second, i);
        if(n[i].x == start.first && n[i].y == start.second){
            n[i].val = 0;
        }
        if(i < houses.size()-1 && houses[i+1].first -shift == houses[i].first){
            n[i].uppercoord = houses[i+1].second;
        }
        if(lastcoord == houses[i].first){
            lastcoord = houses[i].first;
        }else{
            laststart = startindex;
            endindex = i;
            startindex = i;
            lastcoord = houses[i].first;
        }

        //dist[houses[i].first][houses[i].second] = (LONG_MAX);
        for(ll x = laststart; x < endindex; x++){
            //Add in edges
            //Nodes are sorted in y-order, right?
            if(n[x].y+1 >= n[i].y){ //This is a loweredge
                //This could be the lower edge
                if((n[x].loweredgecoord < n[i].y || n[x].loweredgecoord == LONG_MAX) && n[i].y < n[x].uppercoord-1){
                    //This is the new loweredge
                    if(n[x].loweredgecoord != LONG_MAX){
                        //This node already had a loweredge, remove the previous one
                        n[n[x].loweredgeindex].removeEdge(n[x].index);
                    }
                    n[x].loweredgecoord = n[i].y;
                    n[x].loweredgeindex = i;
                    n[x].loweredgeval = std::abs(n[x].y - n[i].y);
                    n[i].e.push_back(x);

                }else if(n[x].y+1 == n[i].y){ //Is this valid if they are on the same ycoord? I don't think s0
                    n[x].loweredgecoord = LONG_MAX; //n[x].y == n[i].y
                }
            }else if(n[i].y < n[x].uppercoord-1){
                //This is a normal edge for both
                n[x].e.push_back(i);
                n[i].e.push_back(x);
            }
        }

    }
    for(ll i = 0; i < N; i++){
     /*   if(n[i].loweredgecoord != LONG_MAX){
            n[i].e.push_back(n[i].loweredgeindex);
        }*/

        q.insert(n[i]);
    }
    //Graphs been built
    //printarr(gb);
    //Preprocess where the edges are

    //ll time = Do(n, std::vector<bool>(n.size(), false), _begin, 0, _end);
    //std::cout << time << std::endl;
    //return 0;
    /*
    Om dess enda edge är målnoden, så är den noden ointressant,
    Om den bara har en edge och inte är startnod är den också ointressant
*/
    /*
    for(ll i = 0; i < N; i++){
        if(n[i].e.size() == 1){
            if(n[i].x != start.first && n[i].y != start.second){
                //Remove this node
                n[i].interesting = false; //To not mess up the edges
            }
        }
    }*/
    ll time = LONG_MAX;
    std::set<Node>::iterator it;

    while(!q.empty()){
        Node cur = *q.begin();
        q.erase(q.begin());

        if(cur.x == end.first && cur.y == end.second){
            time = std::min(cur.val, time);
            break;
        }

        for(ll i = 0; i < cur.e.size(); i++){
            it = q.find(n[cur.e[i]]);
            if(it != q.end()){
                Node _n = *it;
                if(_n.interesting && _n.val > std::max(std::abs(_n.y - cur.y), cur.val)){
                    //We want to change it
                    q.erase(it);
                    _n.val = std::max(std::abs(_n.y - cur.y), cur.val);
                    n[_n.index] = _n;
                    q.insert(_n);
                }
            }
        }

        if(cur.loweredgecoord != LONG_MAX){
            //This has a lower edge
            it = q.find(n[cur.loweredgeindex]);
            if(it != q.end()){
                Node _n = *it;
                if(_n.val > std::max(cur.loweredgeval, cur.val)){
                    //We want to change it
                    q.erase(it);
                    _n.val = std::max(cur.loweredgeval, cur.val);
                    n[_n.index] = _n;
                    q.insert(_n);
                }
            }

        }
        //do all egdees. YES
    }
    if(time >= K){
        std::cout << "NATT" << std::endl;
        return 0;
    }
    std::cout << time << std::endl;
}

/*
7 10
0 1
1 1
2 1
3 1
4 1
7 1
5 1
*/
/*
3 1000000000000
100000 100000000000
100001 0
100000 999999999999


999999999999
*/
/*
12 1000000
3 4
5 5
6 3
5 5
5 8
2 3
1 4
0 5
1 6
4 6
3 7
2 7
*/
/*
10 6
4 3
3 1
4 0
5 0
6 0
7 0
6 5
5 4
4 5
3 5

5
*/
/*
15 5
0 0
1 1
2 2
3 3
4 4
5 5
6 6
7 7
0 3
1 4
2 5
3 6
4 7
5 8
8 8

2
*/
/*
5 1000000000000
3 1000000
4 999999
4 0
4 1000000000
5 1000

KOLLA SVARET
*/
/*
3 10000
0 2
0 4
1 3
*/
/*
12 100
3 4
3 7
2 3
2 7
4 4
5 5
5 3
5 8
1 4
1 6
0 5
4 6

 * /
23 100000000000
0 0
0 2
1 1
0 2
2 2
1 3
0 4
2 4
1 5
0 6
2 6
4 0
6 0
5 1
4 2
6 2
5 3
4 4
6 4
5 5
4 6
3 1
6 6
*/
/*
15 3
4 5
3 3
4 3
3 7
2 8
4 9
5 3
6 4
7 5
8 6
9 7
5 6
6 7
7 8
5 9
*/
/*
13 5
4 5
3 3
4 3
5 3
6 4
7 6
8 6
9 7
3 6
2 7
5 8
3 9
4 9
*/
/*
15 3
0 0
1 0
2 0
3 0
4 0
5 0
0 2
2 2
4 2
1 4
3 4
5 4
0 6
2 6
4 6

*/
/*
5 4
0 4
1 5
2 2
2 4
1 3
*/
/*
39 10
0 8
1 8
2 8
4 8
6 8
7 8
0 6
1 6
2 6
4 6
6 6
7 6
8 6
0 4
1 4
2 4
4 4
6 4
7 4
8 4
0 2
1 2
2 2
4 2
6 2
7 2
8 2
5 1
0 0
0 0
1 0
2 0
3 0
4 0
6 0
7 0
8 0
3 6
8 8
*/
/*
9 10
0 8
1 8
2 8
3 0
4 8
5 1
6 8
7 8
8 8
*/
/*
24 10
0 7
2 7
5 7
4 6
6 6
7 5
0 4
3 4
4 4
5 4
6 4
2 3
9 3
4 2
8 2
1 1
5 1
6 1
7 1
0 0
2 0
3 0
4 0
9 7
*/
/*
4 4
0 2
1 3
2 1
1 1
*/
/*
56 2
0 0
0 2
0 4
0 6
0 8
0 10
1 0
1 3
1 5
1 7
1 9
1 11
2 1
2 4
2 6
2 8
2 10
3 0
3 2
3 5
3 7
3 9
3 11
4 1
4 3
4 6
4 8
4 10
5 0
5 2
5 4
5 7
5 9
5 11
6 1
6 3
6 5
6 8
6 10
7 0
7 2
7 4
7 6
7 9
7 11
8 1
8 3
8 5
8 7
8 10
9 0
9 2
9 4
9 6
9 10
9 8
*/
