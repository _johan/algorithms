#include <iostream>
#include <vector>
#include <utility>
#include <deque>
#include <algorithm>

int main(){
    std::cout.sync_with_stdio(false);
    int N, P;
    std::cin >> N;
    std::string s;
    std::vector<std::vector<std::pair<std::string, std::string>>> args(N);
    std::vector<std::string> _args(N);
    std::cin >> s;
    bool done = false;
    for(int i = 0; i < N && !done; i++){
        if(s.compare("accept") == 0){
            _args[i] = "accept";
        }else if(s.compare("log") == 0){
            _args[i] = "log";
        }else if(s.compare("drop") == 0){//Drop
            _args[i] = "drop";
        }else{
            //This is an integer
            //i.e pair
            P = std::stoi(s);
            break;
        }
        while(std::cin >> s){
            if(s.compare("accept") == 0 || s.compare("log") == 0 || s.compare("drop") == 0){
                break;
            }
            int delim = -1;
            for(int o = 0; o < s.size(); o++){ //Använd find ist
                if(s[o] == '='){
                    //Delimiter
                    delim = o;
                    break;
                }
            }
            if(delim == -1){
                P = std::stoi(s);
                done = true;
                break;
            }
            std::string lhs = s.substr(delim+1, s.size() - delim - 1), rhs = s.substr(0, delim);
            args[i].push_back(std::make_pair(lhs, rhs));
        }
    }
    std::vector<std::pair<std::string, std::string>> packets(P);
    std::deque<std::string> _num;
    //This works
    for(int i = 0; i < P; i++){
        std::string s;
        std::cin >> s;
        int pos = s.find(':');
        packets[i] = std::make_pair(s.substr(0, pos), s.substr(pos+1, s.size() - pos-1));
    }
    //for(int i = 0; i < P; i++){
    //    std::cout << packets[i].first << ", " << packets[i].second << std::endl;
    //}

    for(int i = 0; i < P; i++){
        int thlatest = -1;
        _num.push_front(packets[i].first);
        for(int c = 0; c < N; c++){
            bool meetsreq = true;
            for(int l = 0; l < args[c].size(); l++){
                if(args[c][l].second.compare("port") == 0){
                    //This checks the port
                    if(args[c][l].first.compare(packets[i].second) != 0){
                        meetsreq = false;
                        break;
                    }
                }else if(args[c][l].second.compare("ip") == 0){
                    if(args[c][l].first.compare(packets[i].first) != 0){
                        meetsreq = false;
                        break;
                    }
                }else{
                    int req = std::stoi(args[c][l].first), cur = 0, upperbound = std::min(1000, (int)_num.size());
                    if(thlatest == -1){
                        thlatest = 0;
                        for(int _i = 0; _i < upperbound; _i++){
                            if(_num[_i].compare(packets[i].first) == 0){
                                cur++;
                                thlatest++;
                            }
                        }
                    }
                    if(thlatest < req){
                        meetsreq = false;
                        break;
                    }

                    //The limit thing
                }
            }
            if(meetsreq){
                if(_args[c].compare("accept") == 0){
                    std::cout << "accept " << i+1 << "\n";
                    break;
                }else if(_args[c].compare("log") == 0){
                    std::cout << "log " << i+1 << "\n";
                }else{
                    std::cout << "drop " << i+1 << "\n";
                    break;
                }
            }
        }
    }
}
