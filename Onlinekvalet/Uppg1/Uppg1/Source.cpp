#include <iostream>
#include <vector>
#include <string>

int main(){
    std::string name;
    std::cin >> name;
    int N, namnsdagpos;
    std::cin >> N;
    std::vector<std::string> names(N);
    for(int i = 0; i < N; i++){
        std::cin >> names[i];
        if(names[i].compare(name) == 0){
            namnsdagpos = i;
        }
    }
    for(int i = 0; i < namnsdagpos; i++){
        if(names[i].size() != name.size()){
            continue;
        }
        int same = 0;
        for(int c = 0; c < name.size(); c++){
            if(name[c] == names[i][c]){
                same++;
            }
        }
        if(same == name.size() - 1){
            //This is possible
            std::cout << i+1 << std::endl;
            return 0;
        }
    }
    std::cout << namnsdagpos+1 << std::endl;
    return 0;
}
