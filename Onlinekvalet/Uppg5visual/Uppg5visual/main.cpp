#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <utility>
#include <sstream>
std::pair<int,int> Turn(std::pair<int,int> cur, int t){
    if(t == 0){
        if(cur.first == 1){
            cur = std::make_pair(0, -1);
        }else if(cur.first == -1){
            cur = std::make_pair(0, 1);
        }else if(cur.second == 1){
            cur = std::make_pair(1, 0);
        }else{
            cur = std::make_pair(-1, 0);
        }
    }else if(t == 1){
        if(cur.first == 1){
            cur = std::make_pair(0, 1);
        }else if(cur.first == -1){
            cur = std::make_pair(0, -1);
        }else if(cur.second == 1){
            cur = std::make_pair(-1, 0);
        }else{
            cur = std::make_pair(1, 0);
        }

    }
    return cur;
}
int H,W;

bool verify(std::pair<int,int> pos){
    if(pos.first >= 0 && pos.first < W && pos.second >= 0 && pos.second < H){
        return true;
    }
    return false;
}

//void printArr(std::vector<std::vector<char>> &gb)
int main()
{
    std::vector<std::pair<int, std::string>> labels;
    std::pair<int,std::string> main;
    std::ifstream in("robot_diagonal.in");
    if(!in.is_open()){
        std::cout << "Didn't open" << std::endl;
    }
    //robot_diagonal.ans
    std::ifstream _in("robot_diagonal.ans");
    if(!_in.is_open()){
        std::cout << "Didn't open" << std::endl;
    }

    std::streambuf *cinbuf = std::cin.rdbuf();
    std::cin.rdbuf(_in.rdbuf());
    std::vector<std::string> commands;
    std::string s;
    in >> s;
    in >> H >> W;
    std::pair<int,int> rot;
    std::pair<int,int> pos;
    std::vector<std::vector<char>> gb(H, std::vector<char>(W, '.'));
    for(int y = 0; y < H; y++){
        std::string s;
        in >> s;
        for(int x = 0; x < W; x++){
            if(s[x] == '^'){
                rot = std::make_pair(0, -1);
                pos = std::make_pair(x,y);
            }else if(s[x] == '>'){
                rot = std::make_pair(1, 0);
                pos = std::make_pair(x,y);
            }else if(s[x] == '<'){
                rot = std::make_pair(-1, 0);
                pos = std::make_pair(x,y);
            }else if(s[x] == '#'){
                gb[y][x] = '#';
            }
        }
    }
    int ln = 0;
    while(std::getline(std::cin, s)){
        commands.push_back(s);
        for(char c : s){
            if(c == ':'){
                s.resize(s.size()-1);
                labels.push_back(std::make_pair(ln, s));
                if(s.compare("main:") == 0){
                    main = std::make_pair(ln, "main");
                    s.resize(s.size()-1);
                    main = std::make_pair(ln, s);
                }
            }
        }
        ln++;
    }

    for(int i = main.first; i < commands.size(); i++){
       // gb[pos.second][pos.first] = 'X';
        std::cout << pos.first << ", " << pos.second << std::endl;
        std::stringstream ss(commands[i]);
        std::string comm1;
        std::string m;
        ss >> comm1;
        int forrange = 0;
        std::pair<int,int> npos = pos;
        npos.first += rot.first;
        npos.second+=rot.second;

        if(comm1.compare("for") == 0){
            ss >> forrange;

        }else if(comm1.compare("left") == 0){
            rot = Turn(rot, 0);
        }else if(comm1.compare("right") == 0){
            rot = Turn(rot, 1);
        }else if(comm1.compare("forward") == 0){
            if(!verify(npos)){
                std::cout << "BLOCKED " << npos.first << ", " << npos.second << std::endl;
                continue;
            }
            if(gb[npos.second][npos.first] == '#'){
                continue;
            }
            pos.first += rot.first;
            pos.second+=rot.second;
        }else if(comm1.compare("gotoblocked") == 0){
            if(verify(npos) && gb[npos.second][npos.first] == '.'){
                continue;
            }
            std::cout << "Blocked " << npos.first << ", " << npos.second << std::endl;
            ss >> m;
            for(std::pair<int, std::string> p : labels){
                if(p.second.compare(m) == 0){
                    i = p.first;
                }
            }
        }else if(comm1.compare("call") == 0){
            ss >> m;
            for(std::pair<int, std::string> p : labels){
                if(p.second.compare(m) == 0){
                    i = p.first;
                }
            }
        }
    }

    return 0;
}

