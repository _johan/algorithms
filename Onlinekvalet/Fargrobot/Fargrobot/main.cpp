#include <iostream>
#include <vector>
typedef unsigned int uint;
int main()
{
    uint N;
    std::cin >> N;
    std::string s, out;
    std::cin >> s;
    uint pos = -1, t = 0;
    bool r = false, b = false, g = false;
    while(N > 0){
        N--;
        pos++;
        while(t < 2){
            if(pos >= s.size()){
                std::cout << out << std::endl;
                return 0;
            }
            if(s[pos] == 'R' && !r){
                r = true;
                t++;
            }else if(s[pos] == 'B' && !b){
                b = true;
                t++;
            }else if(s[pos] == 'G' && !g){
                g = true;
                t++;
            }
            if(t == 2){
                //Find next pos
                char l = 'R';
                if(!b){
                    l = 'B';
                }else if(!g){
                    l = 'G';
                }
                for(; s[pos] != l; pos++);
            }else
            pos++;
        }
        if(!r){
            out.push_back('R');
        }else if(!b){
            out.push_back('B');
        }else if(!g){
            out.push_back('G');
        }
        r = false; b = false; g = false; t = 0;
    }
    std::cout << out << std::endl;
}

/*
Kan ha tre for loopar inuti while loopen. En för varje bokstavstyp, sedan tar man den bokstav som kommer längst
*/

