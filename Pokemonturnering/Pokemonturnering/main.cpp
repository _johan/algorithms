#include <iostream>
#include <vector>
#include <set>
#include <utility>
#include <algorithm>
#include <climits>
#include <map>
#include <unordered_map>
#include <list>
typedef std::pair<int,int> pii;
typedef std::vector<pii> vpii;
typedef std::vector<int> vi;
typedef std::vector<vi> vvi;
typedef std::vector<bool> vb;
//typedef std::vector<std::set<int>> vsi;

using namespace std;


struct Match{
    Match(){}
    Match(int l, int r, int i) : l(l), r(r), index(i){}
    int l, r, index;

    friend bool operator < (const Match &lhs, const Match &rhs){
        if(lhs.l == rhs.l){
            if(lhs.r == rhs.r){
                return lhs.index < rhs.index;
            }
            return lhs.r < rhs.r;
        }
        return lhs.l < rhs.l;
    }
};
typedef std::vector<std::set<Match>> vsi;


void play(vsi &adj, vb &played, vi &money, int &ret, int index){

    for(set<Match>::reverse_iterator it = adj[index].rbegin(); it != adj[index].rend(); ++it){
        if(it->r < index){
            //If we win we play
            played[it->index] = true;
            ret += money[it->r]/2;
            money[it->l] -= ret;
            play(adj, played, money, ret, it->r);
        }
    }
}

int main()
{
    int N, M;
    cin >> N >> M;
    vsi adj(N);
    vi money(N, 100);
    //vb connected(N, false);
    vb played(M, false);
    //withoutconnect.reserve(N);
    for(int i = 0; i < M; ++i){
        int lhs, rhs;
        cin >> lhs >> rhs;
        --lhs; --rhs;        
        adj[lhs].insert(Match(lhs, rhs, i));
        adj[rhs].insert(Match(lhs, rhs, i));
    }
    int end = 100;
    set<Match>::reverse_iterator it = adj[0].rbegin();
    for(it; it != adj[0].rend(); ++it){
        //Starts with highest index
        //Play all this matches
        int ret = 0;
        play(adj, played, money, ret, it->r);
        money[it->r] += ret;
        end += money[it->r] / 2;
        //This has to be a recursive function
    }
    //Antingen har den en dirket anslutning till 0 eller också får den förlora till dom andra.
    //Om någon har en direkt anslutning till 0 ska dom inte spela en match som är mot någon annan som ska spela mot 0.
    cout << end << endl;
    return 0;
}

