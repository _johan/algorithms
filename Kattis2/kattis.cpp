#include "kattis.h"
#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <cmath>
#include <set>
#define all(v) v.begin(), v.end()
#define INF 10000000
using namespace std;
typedef pair<double, double> pdd;
typedef vector<pdd> vpdd;
typedef vector<int> vi;
/*
Är den konvex borde det väl alltid fungera med 1 vakt? Eller max 2. I mitten
*/
struct Pack{
    int index;
    double x;
    bool isEnd;
    Pack(){}
    Pack(int i, double x, bool e) : index(i), x(x), isEnd(e){}

    //bool operator()
    friend bool operator<(const Pack &lhs, const Pack &rhs){
        if(lhs.x == rhs.x){
            if(lhs.isEnd == rhs.isEnd){
                return lhs.index < rhs.index;
            }
            return lhs.isEnd < rhs.isEnd;
        }
        return lhs.x < rhs.x;
    }
};


int spec(vpdd &coords, double H, int N, vi &inDanger){
    if(inDanger.size() == 1)
        return 1;
    double lastK = INF, middleX = 0;
    int middle = -1;
    for(int i = 0; i < N-1; ++i){
        double k = (coords[i].second - coords[i+1].second) / (coords[i].first - coords[i+1].first);
        if(k == 0 || (lastK > 0 && k < 0)){
            middle = i;
            if(k == 0){
                middleX = (coords[i].first + coords[i+1].first) / 2;
            }else{
                middleX = coords[i].first;
            }
            break;
        }
    }
    int idS = inDanger[0], idS2 = inDanger[1], idE = inDanger[inDanger.size()-1], idE2 = inDanger[inDanger.size()-2];
    double startK = (coords[idS].second - coords[idS2].second) / (coords[idS].first - coords[idS2].first);
    double endK = (coords[idE].second - coords[idE2].second) / (coords[idE].first - coords[idE2].first);
    if((H - coords[idS].second) / (middleX - coords[idS].first) >= startK && (H - coords[idE].second) / (middleX - coords[idE].first) <= endK){
        return 1;
    }
    return 2;
}

int kattis(int N, int H, int X[], int Y[], int Z[]) {
    double h = H;
    vpdd coords(N);
    vi inDanger;
    inDanger.reserve(N);
    for(int i = 0; i < N; ++i){
        coords[i] = pdd(X[i], Y[i]);
        if(Z[i] == 1)
            inDanger.push_back(i);
    }
    if(N > 1000)
        return spec(coords, h, N, inDanger);
    set<Pack> segments;
    for(int i : inDanger){
        double position = -1, max = 0;
        for(int j = 0; j < i; ++j){
            double k = (coords[i].second - coords[j].second) / (coords[i].first - coords[j].first);
            if(k < max){
                double m = coords[i].second - k * coords[i].first;
                position = (h - m) / k;
                max = k;
            }
        }
        //if(position >= 0){
            segments.insert(Pack(i, position, false));
        //}
            position = INF;
            max = 0;
        for(int j = i+1; j < N; ++j){
            double k = (coords[i].second - coords[j].second) / (coords[i].first - coords[j].first);
            if(k > max){
                double m = coords[i].second - k * coords[i].first;
                position = (h - m) / k;
                max = k;
            }
        }
        if(position >= 0){
            segments.insert(Pack(i, position, true));
        }
    }

    long long guards = 0;
    set<int> taken, add;
    for(Pack p : segments){
        if(taken.find(p.index) == taken.end()){
            if(!p.isEnd){
                add.insert(p.index);
            }else{
                taken.insert(add.begin(), add.end());
                ++guards;
                //std::cout << "Added a guard at: " << p.x << std::endl;
            }
        }
    }

    return guards;
}
/*
5 12 
0 0 1 
5 0 1 
10 0 1 
12 10 1 
15 10 1 


11 106
0 0 1 
10 0 1 
20 0 1 
30 0 1 
49 0 1
50 100 1 
69 0 1
70 105 1 
71 0 1
72 105 1
73 0 1
*/
/*
4 8
0 0 1
2 2 1
4 4 1
6 6 1

1

6 6
0 0 0
2 2 0
4 4 1
6 4 1
8 2 0
10 0 0

1

7 8
0 0 0
1 4 0
2 6 1
4 7 1
5 6 1
6 4 0
8 0 0
*/
