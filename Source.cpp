#include <iostream>
#include <string>
#include <vector>
#include <queue>

#define WALKABLE '.'
#define ROBOT 'X'
#define WALL '#'


typedef unsigned int uint;
typedef long long ll;

struct Vec2{
	Vec2(){}
	Vec2(uint __x, uint __y){
		x = __x;
		y = __y;
	}
	Vec2(const Vec2 &v){
		x = v.x;
		y = v.y;
	}

	uint x;
	uint y;
};

int main(){
	uint w, h;
	std::cin >> h >> w;
	std::vector<std::vector<char>> gb(h, std::vector<char>(w));
	std::vector<std::vector<bool>> vis(w, std::vector<bool>(h, false));
	for (uint i = 0; i < h; i++){
		std::string s;
		std::cin >> s;
		for (uint x = 0; x < w; x++){
			gb[i][x] = s[x];
		}
	}

	Vec2 start(0, 0);
	vis[start.x][start.y] = true;
	std::queue<Vec2> q;
	q.push(start);
	while (!q.empty()){
		Vec2 cur = q.front();
		q.pop();

		if (gb[cur.y][cur.x] == ROBOT){
			std::cout << "Death to humans" << std::endl;
			return 0;
		}
		if (cur.x + 1 < w && gb[cur.y][cur.x + 1] == WALKABLE && !vis[cur.x+1][cur.y]){
			q.push(Vec2(cur.x + 1, cur.y));
		}
		if (cur.x - 1 >= 0 && gb[cur.y][cur.x - 1] == WALKABLE && !vis[cur.x - 1][cur.y]){
			q.push(Vec2(cur.x - 1, cur.y));
		}
		if (cur.y + 1 < h && gb[cur.y + 1][cur.x] == WALKABLE && !vis[cur.x][cur.y+1]){
			q.push(Vec2(cur.x, cur.y+1));
		}
		if (cur.y - 1 >= 0 && gb[cur.y - 1][cur.x] == WALKABLE && !vis[cur.x][cur.y-1]){
			q.push(Vec2(cur.x, cur.y-1));
		}
	}

	std::cout << "We are safe" << std::endl;
	int asd;
	std::cin >> asd;
}