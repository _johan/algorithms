#include <iostream>
#include <fstream>
#include <vector>
#include <utility>
#include <algorithm>

struct DrinkMilk{
	DrinkMilk(){}
	DrinkMilk(int p, int m, int t){
		this->p = p;
		this->m = m;
		this->t = t;
	}
	int p;
	int m;
	int t;
};
int main(){
	std::ifstream ifs("badmilk.in");
	std::cin.rdbuf(ifs.rdbuf());
	std::ofstream ofs("badmilk.out");
	std::cout.rdbuf(ofs.rdbuf());
	int N, M, D, S;
	std::cin >> N >> M >> D >> S;
	//std::vector<DrinkMilk> drinks(D);
	std::vector<std::vector<int>> drankmilk(N, std::vector<int>(M, -1));
	std::vector<int> sick(N, -2);
	//std::vector<std::pair<int, int>> sick(S);
	std::vector<bool> safemilk(M);
	for (int i = 0; i < D; i++){
		int p, m, t;
		std::cin >> p >> m >> t;
		drankmilk[p - 1][m - 1] = t;
		//drinks[i] = DrinkMilk(p, m, t);
	}
	for (int i = 0; i < S; i++){
		int p, t;
		std::cin >> p >> t;
		sick[p - 1] = t;
		//sick[i] = std::make_pair(p, t);
	}
	std::vector<bool> drankofthismilk(N);
	for (int i = 0; i < M; i++){
		std::fill(drankofthismilk.begin(), drankofthismilk.end(), false);
		for (int c = 0; c < N; c++){
			if (drankmilk[c][i] < sick[c] && drankmilk[c][i] != -1){
				drankofthismilk[c] = true;
			}
		}
		for (int c = 0; c < N; c++){
			if (sick[c] != -2 && drankofthismilk[c]){
				//Potentially bad
			}
			else if(!drankofthismilk[c] && sick[c] != -2){
				//This can't be bad
				safemilk[i] = true;
			}
		}
	}
	int mx = 0;
	for (int i = 0; i < M; i++){
		if (!safemilk[i]){
			//Look for everyone who drank of this milk
			int tot = 0;
			for (int c = 0; c < N; c++){
				if (drankmilk[i][c] != -1){
					tot++;
				}
			}
			mx = std::max(tot, mx);
		}
	}
	std::cout << mx << std::endl;

}