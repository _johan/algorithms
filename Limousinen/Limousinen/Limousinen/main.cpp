#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

int main(){
	int N, T;
	cin >> N >> T;
	vector<int> dist(N);
	for (int i = 0; i < N; ++i){
		int x, y;
		cin >> x >> y;
		dist[i] = (abs(x) + abs(y)) * 2;
	}
	sort(dist.begin(), dist.end());
	int cur = 0;
	for (int i = 0; i < N; ++i){
		if (T >= dist[i]){
			T -= dist[i];
			++cur;
		}
		else{
			break;
		}
	}
	cout << cur << endl;
#ifdef WIN32
	system("PAUSE");
#endif
}