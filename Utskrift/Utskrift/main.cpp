#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <climits>
using namespace std;
typedef long long ll;
ll N, M;
vector<ll> rows; //This is a vector of the amount of words in a row, max it can possibly be is one word per row
vector<string> sent;
//position, numwordssleft (sent.size()-position)
vector<ll> dp;
ll calcBadness(ll len){
    return (ll)pow(abs(N - len), 2);
}
ll Do(ll position, ll line, ll offset){
    if (dp[position] != -1){
        return dp[position];
	}
	if (position == sent.size() ||line == sent.size()){
        return offset; //What return val?
	}
    if(position + 1 == sent.size()){
        dp[position] = calcBadness(sent[position].size());
        return calcBadness(sent[position].size());
    }
//max((ll)pow(abs(N - curlen), 2), offset)
    ll minBadness = INT_MAX, sz = 0, breakWhere = 0;
    ll whitespaces = 0;
    for(int i = position; i < sent.size(); ++i){
        if(dp[i+1] == -1){
            Do(i+1, line, offset);
        }
        sz += sent[i].size();
        /*if(position == 0){
            cout << "0/Hello" << endl;
        }*/
        ll temp = max(calcBadness(sz + whitespaces),  dp[i+1]);
        whitespaces++;
        if(temp < minBadness){
            minBadness = temp;
            breakWhere = i;
        }
    }

    /*
    ll badness = min(Do(position + 1, line, curlen + sent[position].size(), offset), (ll)1000000000);
    offset = max((ll)pow(abs(N - curlen), 2), offset);
    badness = min(Do(position + 1, line + 1, sent[position].size(), max((ll)pow(abs(N - curlen - sent[position].size()), 2), offset)), badness);
    *///badness = max((ll)pow(abs(N - curlen), 3), badness);

   /* if (line == 0){
		cout << "Hej" << endl;
    }*/
    rows[breakWhere] = 1;
    dp[position] = minBadness;
    return minBadness;

}
int main(){
	cin >> N >> M;
	rows = vector<ll>(M);
	sent = vector<string>(M);
    //dp = vector<vector<ll>>(M+1, vector<ll>(M+ 1, -1));
    dp = vector<ll>(M+1, -1);
	for (ll i = 0; i < M; ++i){
		cin >> sent[i];
	}
    dp.back() = 0;
    ll mn = Do(0, 0, 0);
    cout << mn << "letters: " << sqrt(mn) << endl << endl;
    for(int i = 0; i < M; ++i){
        cout << sent[i] << " ";
        if(rows[i] == 1){
            cout << "\n";
        }
    }
    cout << endl;
#ifdef WIN32
	system("PAUSE");
#endif
}
/*
 *
5
3
aaa
cbd
bbcd
*/
/*
using System;
using System.Collections.Generic;

namespace WordWrap
{
	class Program
	{
		static Dictionary<int, int> DP;
		static int[] solution;

		static int badness(int[] words, int i, int j,int lineWidth)
		{
			int actualLen = 0;
			for (int x = i; x <= j; x++)
				actualLen += words[x];

			if (actualLen > lineWidth) return int.MaxValue;
			return (lineWidth - actualLen) * (lineWidth - actualLen);
		}

		static void solveWordWrap(int[] words, int n, int lineWidth, int index)
		{

			if (index > n-1) return;

			if (index == n - 1) //return the badness of the word as in if that word takes up the entire line
			{
				DP[index] = (lineWidth-words[n-1])*(lineWidth-words[n-1]);
				return;
			}

		int min = Int32.MaxValue;
		int breakWhere=0;

		for (int i = index ; i < n; i++)
		{
			if (!DP.ContainsKey(i+1)) solveWordWrap(words, n, lineWidth, i+1);
			int temp = badness(words, index, i,lineWidth);
			temp = (temp == int.MaxValue)? int.MaxValue : temp + DP[i+1];
			if (temp < min)
			{
				breakWhere = i;
				min = temp;
			}
		}
		DP[index] = min;
		solution[breakWhere] = 1;
	}

	static void Main(string[] args)
	{
		int[] l = {3, 2, 2, 5};
		solution = new int[4];
		for (int i = 0; i < 4; i++)
			solution[i] = 0;
		int M = 6;
		DP = new Dictionary<int, int>();
		solveWordWrap (l, 4, M,0);
		Console.WriteLine(DP[0]);
		for (int i = 0; i < 4; i++)
			Console.WriteLine(solution[i]);
		Console.Read();
	}
}
}
*/
