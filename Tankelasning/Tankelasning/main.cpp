#include <iostream>

using namespace std;
int main(){
	int N;
	cin >> N;
	double sum = 10;
	double xsum = 10;

	for (int i = 0; i < N; ++i){
		char op, num;
		cin >> op >> num;
		int n;
		if (num == 'x'){
			n = 10;
		}
		else{
			n = num - '0';
		}
		if (op == '-'){
			sum -= n;
			if (num == 'x'){
				xsum -= 10;
			}
		}
		else if (op == '+'){
			sum += n;
			if (num == 'x'){
				xsum += 10;
			}
		}
		else if (op == '*'){
			sum *= n;
			/* Endast vid multiplikation med x*/
			if (num != 'x'){
				xsum *= n;
			}
			else{
				xsum = sum;
			}
		}
		else{
			sum /= n;
			xsum /=n;
		}
	}
	if (xsum != 0){
		cout << "nej" << endl;
	}else
		cout << sum  << endl;
#ifdef WIN32
	system("PAUSE");
#endif
}