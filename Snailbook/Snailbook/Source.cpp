#include <iostream>
#include <vector>
#include <set>

struct DisjointNode{
	DisjointNode(){}
	DisjointNode(DisjointNode* parent, int index, int rank){
		this->parent = parent;
		this->index = index;
		this->rank = rank;
	}

	DisjointNode* parent;
	int index;
	int rank;

};

/*
This class holds indexes to another container (probably vector)
That means that all sorting must be done before doing anything with this class
Otherwise indexing will be broken
*/
class DisjointSet{
	int numelements;
	DisjointNode *dn;
public:
	DisjointSet(int numelements){
		dn = new DisjointNode[numelements];
		this->numelements = numelements;
		for (int i = 0; i < numelements; i++){
			dn[i] = DisjointNode(&dn[i], i, 0);
		}
	}
	//Takes an index
	int Find(int x){
		//Returns the root of the value being found
		//Or if it didn't find any value
		DisjointNode *t = &dn[x];
		if (t->parent != t){
			return Find(t->parent->index);
		}
		return t->parent->index;
	}

	void Union(int x, int y){
		DisjointNode * xRoot = &dn[Find(x)];
		//DisjointNode * yRoot = &dn[Find(y)];
		DisjointNode * yRoot = &dn[y];
		if (xRoot == yRoot ||xRoot->parent == yRoot ||xRoot->parent == yRoot->parent || yRoot->parent == xRoot->parent){
			return;
		}
		yRoot->parent = xRoot;
		return;
		if (xRoot->rank < yRoot->rank){
			xRoot->parent = yRoot;
		}
		else if (xRoot->rank > yRoot->rank){
			yRoot->parent = xRoot;
		}
		else{
			yRoot->parent = xRoot;
			++xRoot->rank;
		}
	}
	DisjointNode* GetArray(){
		return dn;
	}
};

int main(){
	int N;
	std::cin >> N;
	std::vector<std::vector<int>> adj(N);
	for (int i = 0; i < N; i++){
		int K;
		std::cin >> K;
		adj[i].resize(K);
		for (int k = 0; k < K; k++){
			std::cin >> adj[i][k];
		}
	}
	DisjointSet disjointSet(N);
	for (int y = 0; y < N; y++){
		for (int x = 0; x < adj[y].size(); x++){
			disjointSet.Union(y, adj[y][x] - 1);
		}
	}
	std::set<int> inset;
	for (int i = 0; i < N; i++){
		inset.insert(disjointSet.Find(i));
		//inset.insert(disjointSet.GetArray()[i].parent->index);
	}
	std::cout << inset.size() << std::endl;
}

/*
14
1 6       1  0
0         2  1
0         3  2
0         4  3 
1 8       5  4
2 7 11    6  5
1 1       7  6
3 2 3 4   8  7
2 8 10    9  8
1 9       10 9
0         11 10
2 11 13   12 11
0         13 12
1 12      14 13
*/

//1 6 7 11
//5 8 2 3 4 9 10
//12 11 13 14
