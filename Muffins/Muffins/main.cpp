#include <iostream>


int main()
{
    int N, A = 0, B = 0;
    bool Bturn = false;
    std::cin >> N;
    while(N > 0){
        int h1 = N/2, h2 = N - h1;
        int add = h2;
        if(h1 > h2){
            add = h1;
        }
        if(Bturn){
            B += add;
            Bturn = false;
        }else{
            A += add;
            Bturn = true;
        }
        N -= add;
    }
    std::cout <<B <<" " << A << std::endl;
    return 0;
}

