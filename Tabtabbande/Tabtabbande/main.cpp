#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

int main(){
    int N, M;
    std::cin >> N >> M;
    std::vector<int> goals(M);
    for(int i = 0; i < M; i++){
        std::cin >> goals[i];
    }
    int dist = 0, current = 1;
    for(int i = 0; i < M; i++){
        int ndist = 0;
        if(current > goals[i]){
            //Wrap around
            ndist = std::min(N - current + goals[i], current - goals[i]);
        }else{
            ndist = std::min(current + N - goals[i], goals[i] - current);
        }
        dist += ndist;
        current = goals[i];
    }
    std::cout << dist << std::endl;
}
