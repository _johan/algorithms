#include <iostream>
#include <vector>
#include <queue>
#include <utility>
using namespace std;
typedef pair<int,int> pii;
const int U = 0, N = 1, V = 2, H = 3;

struct Node{
    Node(){}
    Node(pii p, pii pr, int g) : pos(p), prev(pr), grav(g){}
    pii pos;
    pii prev;
    int grav;
};

int main()
{
    int H, W, N;
    cin >> W >> H >> N;
    vector<vector<vector<bool>>> walls(H+1, vector<vector<bool>>(W+1, vector<bool>(4, false)));
    vector<vector<pair<bool, bool>>> visited(H+1, vector<pair<bool, bool>>(W+1));
    for(int i = 0; i < N; ++i){
        int x1, y1, x2, y2;
        cin >> x1 >> y1 >> x2 >> y2;
        while(x1 != x2){
            if(y1 > 0){
                walls[y1-1][x1][U] = true;
            }
            walls[y1][x1][N] = true;
        }
        while(y1 != y2){
            if(x1 > 0){
                walls[y1][x1-1][H] = true;
            }
            walls[y1][x1][V] = true;
        }
    }
    vector<vector<pii>> prev(H+1, vector<pii>(W+1));
    pii goal = pii(H,W);
    pii start = pii(0,0);
    queue<Node> q;
    q.push(Node(start, start, 1));
    while(!q.empty()){
        Node cur = q.front();
        q.pop();
        //prev
    }
}

