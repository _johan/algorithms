#include <iostream>
#include <vector>
#include <algorithm>
#include <queue>
#include <list>
#include <map>
#include <set>
struct DisjointNode{
    DisjointNode(){}
    DisjointNode(DisjointNode* parent, int index, int rank){
        this->parent = parent;
        this->index = index;
        this->rank = rank;
    }

    DisjointNode* parent;
    int index;
    int rank;

};

/*
This class holds indexes to another container (probably vector)
That means that all sorting must be done before doing anything with this class
Otherwise indexing will be broken
*/
class DisjointSet{
    int numelements;
    DisjointNode *dn;
public:

    DisjointSet(int numelements){
        dn = new DisjointNode[numelements];
        this->numelements = numelements;
        for(int i = 0; i < numelements; i++){
            dn[i] = DisjointNode(&dn[i], i, 0);
        }
    }
    //Takes an index
    int Find(int x){
        //Returns the root of the value being found
        //Or if it didn't find any value
        DisjointNode *t = &dn[x];
        if(t->parent != t){
            Find(t->parent->index);
        }
        return t->parent->index;
    }

    void Union(int x, int y){
        DisjointNode * xRoot = &dn[Find(x)];
        DisjointNode * yRoot = &dn[Find(y)];
        if(xRoot == yRoot){
            return;
        }
        if(xRoot->rank < yRoot->rank){
            xRoot->parent = yRoot;
        }else if(xRoot->rank > yRoot->rank){
            yRoot->parent = xRoot;
        }else{
            yRoot->parent = xRoot;
            ++xRoot->rank;
        }
    }
};
const int greater = 0;
const int equal = 1;
const int greaterequal = 2;

struct Relation{
    Relation(){}
    Relation(int _lhs, int _rhs, int _rel) : lhs(_lhs), rhs(_rhs), rel(_rel){}
    int lhs;
    int rhs;
    int rel;
};
bool comp(const std::vector<Relation> &lhs, const std::vector<Relation> &rhs){
    return lhs.size() < rhs.size();
}

/*
Bara kolla om det är en "Strongly Connected Component" !?

EASY!
*/

bool isCyclic(std::vector<std::vector<Relation>> &outgoing, std::vector<bool> &visited, std::vector<bool> &recstack, int pos, DisjointSet &ds){
   //pos = ds.Find(pos);
    if(!visited[pos]){
        visited[pos] = true;
        recstack[pos] = true;
        for(int i = 0; i < outgoing[pos].size(); i++){
            if(!visited[ds.Find(outgoing[pos][i].lhs)] && isCyclic(outgoing, visited, recstack, ds.Find(outgoing[pos][i].lhs), ds) && outgoing[pos][i].rel != equal){
                return true;
            }else if(recstack[ds.Find(outgoing[pos][i].lhs)] && outgoing[pos][i].rel != equal){
                return true;
            }
        }
    }
    recstack[pos] = false;
    return false;
}

int main()
{
    //Fast iostream
    int N, M, R;
    std::cin >> N >> M >> R;
    //nodes.reserve(N);
    std::vector<Relation> relations(R);
    std::vector<std::vector<Relation>> incomingedges(N);
    std::vector<std::vector<Relation>> outgoingedges(N);
    std::vector<int> realsize(N, 0);
    std::vector<int> numequal(N, 0);
    DisjointSet ds(N);

    for(int i = 0; i < R; i++){
        int l,r, rl;
        std::string rel;
        std::cin >> l >> rel >> r;
        if(rel.compare("<") == 0){
            rl = greater;
        }else if(rel.compare("=") == 0){
            rl = equal;
        }else{
            rl = greaterequal;
        }
        --l;
        --r;
        if(rl == equal){
            ds.Union(l,r);
            ++numequal[l];
            ++numequal[r];
        }else if(rl == greater){
            realsize[l]++;
        }
        incomingedges[l].push_back(Relation(l,r,rl));
        outgoingedges[r].push_back(Relation(l,r,rl));
        relations[i] = Relation(l,r,rl);
    }
    //Borde kunna merga dom om rel är = (?)
    //Använd disjoint set
    std::vector<int> numincoming(N, 0);
    for(int i = 0; i < N; i++){
        int it = ds.Find(i);
        if(it != i){
            outgoingedges[it].reserve(outgoingedges[i].size() + outgoingedges[it].size());
            outgoingedges[it].insert(outgoingedges[it].end(), outgoingedges[i].begin(), outgoingedges[i].end());
            incomingedges[it].reserve(incomingedges[i].size() + incomingedges[it].size());
            incomingedges[it].insert(incomingedges[it].end(), incomingedges[i].begin(), incomingedges[i].end());
        }
        numequal[it] = std::max(numequal[it], numequal[i]);

        numincoming[it] = outgoingedges[it].size() - numequal[it]; //Måste ta bort de som är lika med
    }

    std::queue<int> q;
    for(int i = 0; i < N; i++){
        if(numincoming[ds.Find(i)] == 0){
            q.push(ds.Find(i));
        }
    }
    //Om någonting går i en cirkel vill vi couta -1
    std::vector<bool> recstack(N, false);
    std::vector<bool> visited(N, false);
    for(int i = 0; i < N; i++){
        if(isCyclic(outgoingedges, visited, recstack, ds.Find(i), ds)){
            std::cout << -1 << std::endl;
            return 0;
        }
    }

    std::vector<int> val(N, 0);
    //Måste sortera topologiskt bak-och-fram (dvs börja där DEG(outgoing) är 0)
    while(!q.empty()){
        int it = ds.Find(q.front());
        q.pop();
        for(int i = 0; i < incomingedges[it].size(); i++){
            int _it = ds.Find(incomingedges[it][i].rhs);
            if(numincoming[_it] <= 0){
                continue;
            }
            while(val[it] >= val[_it] && it != _it){ //They are merged...
                ++val[_it];
            }
            --numincoming[_it];
            if(numincoming[_it] <= 0 && it != _it){
                q.push(_it);
            }
        }
    }
    int maxgrade = 0;
    for(int i = 0; i < N; i++){
        maxgrade = std::max(val[ds.Find(i)]+1, maxgrade);
    }
    if(maxgrade > M){
        std::cout << -1 << std::endl;
        return 0;
    }
    for(int i = 0; i < N; i++){
        std::cout << val[ds.Find(i)]+1 << " ";
    }
    std::cout << std::endl;
    return 0;
}

/*
4 10 4
1 = 3
2 < 1
4 < 3
2 = 3
*/
