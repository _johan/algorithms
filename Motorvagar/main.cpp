#include <iostream>
#include <vector>
#include <string>
#include <tuple>
#include <set>
#include <map>
#include <iterator>
#include <algorithm>
typedef long long ll;
typedef unsigned int uint;
//lennart@park.se


struct Edge{
	Edge(){}
	Edge(const std::string &lhs, const std::string &rhs, ll val, char t, int i){
		this->lhs = lhs;
		this->rhs = rhs;
		this->val = val;
		this->t = t;
		index = i;
	}
	std::string lhs;
	std::string rhs;
	ll val;
	char t;
	int index;
};


struct Node{
	Node(){}
	Node(const std::string &name, uint N){
		this->name = name;
		edges.reserve(N);
	}
	std::string name;
	std::vector<Edge> edges;
};

std::map<std::string, Node> n;

bool edgeSort(const Edge &lhs, const Edge &rhs){
	if (rhs.val == lhs.val){
		//This is when we need to sort by the nodes DEG()
		return std::max(n[lhs.lhs].edges.size(), n[lhs.rhs].edges.size()) > std::max(n[rhs.lhs].edges.size(), n[rhs.rhs].edges.size());
	}
	return lhs.val < rhs.val;
}


class comparator{
public:
    bool operator() (const Edge &lhs, const Edge &rhs){
        if(lhs.val == rhs.val){
			//if (lhs.lhs == rhs.lhs || lhs.rhs == rhs.rhs){
			//	return lhs.lhs < rhs.rhs;
			//}
            return lhs.index < rhs.index;
        }
		return lhs.val < rhs.val;
	}
};


 void addToSet(const Node &_n, std::set<Edge, comparator> &c){
	 int q;
	 //c.insert(_n.edges.begin(), _n.edges.end());

    //for(std::set<std::tuple<std::string,std::string,ll, char>, comparator>::iterator it = e.begin(); it != e.end(); ++it){
        /*if(std::get<0>(*it) == std::get<0>(t)){
            //Add to set
            //std::cout << "inserting to current" << std::endl;
            c.insert(*it);
        }else if(std::get<0>(*it) == std::get<1>(t)){
            //Add to set
            //std::cout << "inserting to current" << std::endl;
            c.insert(*it);
        }else if(std::get<1>(*it) == std::get<0>(t)){
            //Add to setstd::cout << "inserting to current" << std::endl;
            //std::cout << "inserting to current" << std::endl;
            c.insert(*it);
        }else if(std::get<1>(*it) == std::get<1>(t)){
            //Add to set
            //std::cout << "inserting to current" << std::endl;
            c.insert(*it);
        }*/

    //}
}
void Do(std::vector<std::tuple<std::string, std::string, ll, char>> &e, std::vector<bool> vis){
    return;
}

int main()
{
    ll N, E;
    std::cin >> N >> E;

    //std::set<std::tuple<std::string,std::string,ll, char>, comparator> e;
    std::set<Edge, comparator> newtree;
    std::set<Edge, comparator> current;
    std::vector<Edge> e(E);
    ll eEnd = 0;
    std::vector<std::string> names(N);
    for(ll i = 0; i < N; i++){
        std::cin >> names[i];
		n[names[i]] = Node(names[i], N);
    }
    std::set<std::string> inset;

    for(ll i = 0; i < E; i++){
        std::string lhs, rhs;
        ll val, lhsindex, rhsindex;
        char t;
        std::cin >> lhs >> rhs >> val >> t;
        /*for(ll o = 0; o < N; o++){
            if(names[o] == lhs){
                lhsindex = o;
            }else if(names[o] == rhs){
                rhsindex = o;
            }
        }*/
        if(t == 'R'){
            //e.insert(std::make_tuple(lhs, rhs, val, t));
			e[i] = Edge(lhs, rhs, val, t, i);//std::make_tuple(lhs, rhs, val, t);
			n[lhs].edges.push_back(e[i]);
			n[rhs].edges.push_back(e[i]);
        }
        else{
            val = 0;
			e[i] = Edge(lhs, rhs, val, t, i);
			n[lhs].edges.push_back(e[i]);
			n[rhs].edges.push_back(e[i]);
            //e.insert(std::make_tuple(lhs, rhs, val, t));
            //inset.insert(lhs);
            //inset.insert(rhs);
            //eEnd++;
        }
    }
	std::sort(e.begin(), e.end(), edgeSort);
	//for (std::pair<std::string, Node> p : n){
	//	current.insert(p.second);
	//}

	Node lhs = n[e.front().lhs], rhs = n[e.front().rhs];
	newtree.insert(e.front());

	e.erase(e.begin());
	n.erase(lhs.name);
	n.erase(rhs.name);
	//addToSet(lhs, current);
	current.insert(lhs.edges.begin(), lhs.edges.end());
	current.insert(rhs.edges.begin(), rhs.edges.end());
	//addToSet(rhs, current);
//#ifdef WIN32
//	system("PAUSE");
//#endif
	while (n.size() != 0){
		for (std::set<Edge, comparator>::iterator it = current.begin(); it != current.end();){
			if (n.find(it->lhs) == n.end() && n.find(it->rhs) == n.end()){ //B�da tv� �r tagna finns
				it = current.erase(it);
			}
			else{
				Edge __e = *it;
				it = current.erase(it);
				newtree.insert(__e);
				current.insert(n[__e.lhs].edges.begin(), n[__e.lhs].edges.end());
				current.insert(n[__e.rhs].edges.begin(), n[__e.rhs].edges.end());
				n.erase(__e.lhs);
				n.erase(__e.rhs);
				break;
			}
		}
	}
	ll sum = 0;
	for (Edge __e : newtree){
		sum += __e.val;
	}
	std::cout << sum << std::endl;
#ifdef WIN32
	system("PAUSE");
#endif
    /*
    for(std::set<std::tuple<std::string, std::string, ll, char>>::iterator it = e.begin(); it != e.end(); ++it){
        std::cout << std::get<0>(*it) << " is connected to " << std::get<1>(*it) << " with val " << std::get<2>(*it) << std::endl;
    }*/
/*
    if(inset.size() == 0){
        std::tuple<std::string, std::string, ll, char> c = *e.begin();
        std::cout << "clhs: " << std::get<0>(c) << ", crhs: " << std::get<1>(c) << " val: " << std::get<2>(c) << std::endl;
        inset.insert(std::get<0>(c));
        inset.insert(std::get<1>(c));
        newtree.insert(c);
        addToSet(e, current, c);
    }
*/

/*
    while(inset.size() != N){
        if(e.size() == 0 || current.size() == 0){
            break;
        }
        for(std::set<std::tuple<std::string, std::string, ll, char>, comparator>::iterator it = current.begin(); it != current.end(); ++it){
            if(inset.find(std::get<0>(*it)) != inset.end() && inset.find(std::get<1>(*it)) != inset.end()){
                current.erase(it);
            }else{
                //Add node to current
                std::cout << std::get<0>(*it) << " is connected with " << std::get<1>(*it) << " lenght: " << std::get<2>(*it) << std::endl;
                inset.insert(std::get<0>(*it));
                inset.insert(std::get<1>(*it));
                newtree.insert(*it);
                addToSet(e, current, *it);
                current.erase(it);
                break;
            }
        }
    }*/
    /*
    while(inset.size() != N){
        //std::set<std::tuple<std::string, std::string, ll, char>>::iterator it = e.begin();
        if(e.size() == 0){
            break;
        }
        for(std::set<std::tuple<std::string, std::string, ll, char>>::iterator it = e.begin(); it != e.end(); ++it){
            std::tuple<std::string, std::string, ll, char> cur = *it;
            //std::cout << std::get<1>(cur) << std::endl;
            if(inset.find(std::get<1>(cur)) != inset.end() && inset.find(std::get<0>(cur)) != inset.end()){
                //This node exists in our set. We don't need this edge anymore

                //
                e.erase(it);
            }else{
                //This node is not currently in our new set of nodes
                //Add this edge and node to our set
                std::cout << "curlhs: " << std::get<0>(cur) << ", rhs: " << std::get<1>(cur) << std::endl;
                inset.insert(std::get<1>(cur));
                inset.insert(std::get<0>(cur));
                newtree.insert(cur);
                e.erase(it);
                break;

            }
        }
    }
*/
	/*
    ll sum = 0;
    for(std::set<std::tuple<std::string, std::string, ll, char>>::iterator it = newtree.begin(); it != newtree.end(); ++it){
        sum += std::get<2>(*it);

    }
    std::cout << sum << std::endl;*/
    return 0;
}
/*
6 9
A B C D E F
A B 20 R
A C 20 R
A D 20 R
A E 20 R
A F 20 R
B C 20 R
C D 20 R
D E 20 R
E F 20 R
*/
