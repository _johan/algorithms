#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>

bool compare(const std::pair<int, int> &lhs, const std::pair<int, int> &rhs){
	if (lhs.first < rhs.first){
		return true;
	}
	return false;
}

int main(){
	int num_visitors, different_appartements;
	std::cin >> num_visitors >> different_appartements;
	std::vector<std::pair<int, int>> appartements(different_appartements);

	for (int i = 0; i < different_appartements; i++){
		int cost, num_appartements;
		std::cin >> cost >> num_appartements;
		appartements[i] = std::make_pair(cost, num_appartements);
	}

	std::sort(appartements.begin(), appartements.end(), compare);
	
	int total_cost = 0;
	while (num_visitors > 0){
		if (appartements.front().second <= 0){
			appartements.erase(appartements.begin());
		}
		num_visitors--;
		total_cost += appartements.front().first;
		appartements.front().second--;
	}
	std::cout << total_cost << std::endl;


}