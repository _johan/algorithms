#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>

using namespace std;

/*
 *
 * Working segmented set where you can only update one and one
 * Why would I need a pointer to the parent?
 */
struct Segmented_Node{
    Segmented_Node *left;
    Segmented_Node *right;
    Segmented_Node *parent;
    int leftBound, rightBound;
    int value;
    Segmented_Node(){}
    Segmented_Node(Segmented_Node *l, Segmented_Node *r, Segmented_Node *p, int lb, int rb, int val) : left(l), right(r),
        parent(p), leftBound(lb), rightBound(rb), value(val){}
};

struct Segmented_Set{
    Segmented_Node* root;
public:
    Segmented_Set(vector<int> values){
        //Create all leaf nodes
        vector<Segmented_Node*> nodes;
        nodes.reserve(values.size());
        for(int i = 0; i < values.size(); ++i){
            nodes.push_back(new Segmented_Node(NULL, NULL, NULL, i, i, values[i]));
        }
        vector<Segmented_Node*> newNodes;
        newNodes.reserve(values.size());
        while(nodes.size() != 2){
            for(unsigned int i = 0; i < nodes.size(); i += 2){
                //Kommer bara fungera på odm som är delbara med 2
                newNodes.push_back(new Segmented_Node(nodes[i], nodes[i+1], NULL, min(nodes[i]->leftBound, nodes[i+1]->leftBound),
                        max(nodes[i]->rightBound, nodes[i+1]->rightBound), nodes[i]->value + nodes[i+1]->value));
                nodes[i]->parent = newNodes.back();
                nodes[i+1]->parent = newNodes.back();
            }
            nodes = newNodes;
            newNodes.clear();
        }
        root = new Segmented_Node(nodes[0], nodes[1], NULL, 0, nodes[1]->rightBound, nodes[0]->value + nodes[1]->value);
        nodes[0]->parent = root;
        nodes[1]->parent = root;
    }

    int get(int leftBound, int rightBound){
        return getRec(leftBound, rightBound, root);
    }

private:
    int getRec(int leftBound, int rightBound, Segmented_Node *cur){
        //Är hela innanför?
        if(cur->leftBound >= leftBound && cur->rightBound <= rightBound){
            return cur->value;
        }
        //Finns det något till vänster?
        int sum = 0;
        if(cur->right->leftBound > leftBound){
            sum += getRec(leftBound, rightBound, cur->left);
        }
        if(cur->left->rightBound < rightBound){
            sum += getRec(leftBound, rightBound, cur->right);
        }
        return sum;
        //Error need pen and papper
    }
};

int main()
{
    /*
    8 7 2 3 4 5 9 8
     15  5   9   17
       20      26
           46


*/
    vector<int> in = {8,7,2,3,4,5,9,8};
    Segmented_Set sSet(in);
    cout << sSet.get(0, 7) << endl; // 46
    cout << sSet.get(1,7) << endl; // 38
    cout << sSet.get(3, 7) << endl; // 29
    cout << sSet.get(4, 7) << endl; // 26
}

