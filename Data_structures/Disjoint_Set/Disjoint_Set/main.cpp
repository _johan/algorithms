struct DisjointNode{
    DisjointNode(){}
    DisjointNode(DisjointNode* parent, int index, int rank){
        this->parent = parent;
        this->index = index;
        this->rank = rank;
    }

    DisjointNode* parent;
    int index;
    int rank;

};

/*
This class holds indexes to another container (probably vector)
That means that all sorting must be done before doing anything with this class
Otherwise indexing will be broken
*/
class DisjointSet{
    int numelements;
    DisjointNode *dn;
public:
    DisjointSet(int numelements){
        dn = new DisjointNode[numelements];
        this->numelements = numelements;
        for(int i = 0; i < numelements; i++){
            dn[i] = DisjointNode(&dn[i], i, 0);
        }
    }
    //Takes an index
    int Find(int x){
        //Returns the root of the value being found
        //Or if it didn't find any value
        DisjointNode *t = &dn[x];
        if(t->parent != t){
            Find(t->parent->index);
        }
        return t->parent->index;
    }

    void Union(int x, int y){
        DisjointNode * xRoot = &dn[Find(x)];
        DisjointNode * yRoot = &dn[Find(y)];
        if(xRoot == yRoot){
            return;
        }
        if(xRoot->rank < yRoot->rank){
            xRoot->parent = yRoot;
        }else if(xRoot->rank > yRoot->rank){
            yRoot->parent = xRoot;
        }else{
            yRoot->parent = xRoot;
            ++xRoot->rank;
        }
    }
};


/*
Alternative implementation
use a vector<pair<int,int>> where the index is the "objects" index and first is the parent index second is rank
*/
/*
int main()
{
    DisjointSet ds(10);
    std::cout << ds.Find(9) << std::endl;
    ds.Union(1,2);
    std::cout << ds.Find(1) << ", " << ds.Find(2) << std::endl;
    ds.Union(3,1);
    std::cout << ds.Find(1) << ", " << ds.Find(2) << ", " << ds.Find(3) << std::endl;

    return 0;
}
*/
