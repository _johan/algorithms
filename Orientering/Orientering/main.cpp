#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <utility>
typedef std::pair<std::pair<int, int>, std::pair<int, int>> ppii;
typedef std::pair<int, int> pii;
struct Packet{
	Packet(){}
	Packet(pii pos, pii dir, int nummissed){
		this->pos = pos;
		this->dir = dir;
		this->missed = nummissed;
	}
	pii pos;
	pii dir;
	int missed = 0;
};
inline std::pair<int, int> Add(std::pair<int, int> lhs, std::pair<int, int> rhs){
	lhs.first += rhs.first;
	lhs.second += rhs.second;
	return lhs;
}
int main(){
	int H, B, Y, X;
	std::cin >> H >> B >> Y >> X;
	X--;
	Y--;
	std::vector < std::string > gb(H);
	std::vector<std::vector<bool>> missed(H, std::vector<bool>(B, false));
	for (int i = 0; i < H; i++){
		std::cin >> gb[i];
	}//Pos, Dir
	std::queue<Packet> q;
	q.push(Packet(std::make_pair(0,0), std::make_pair(1,0), 0));
	while (q.size() > 0){
		Packet cur = q.front();
		q.pop();
		if (cur.pos.first == X && cur.pos.second == Y){
			//We've reached the goal
			std::cout << cur.missed << std::endl;
#ifdef WIN32
			system("PAUSE");
#endif
			return 0;
		}
		if (cur.pos.first >= 0 && cur.pos.first < B && cur.pos.second >= 0 && cur.pos.second < H){

		}
		else{
			continue;
		}
		int missed = 0;
		if (gb[cur.pos.second][cur.pos.first] != '.'){
			missed = 1;
			if (gb[cur.pos.second][cur.pos.first] == '^'){
				if (cur.dir == std::make_pair(0, -1)){
					missed = 0;
				}
				else
					q.push(Packet(Add(cur.pos, std::make_pair(0, -1)), std::make_pair(0, -1), cur.missed));
			}
			else if (gb[cur.pos.second][cur.pos.first] == '>'){
				if (cur.dir == std::make_pair(1, 0)){
					missed = 0;
				}
				else
				q.push(Packet(Add(cur.pos, std::make_pair(1,0)), std::make_pair(1, 0), cur.missed));
			}
			else if (gb[cur.pos.second][cur.pos.first] == '<'){
				if (cur.dir == std::make_pair(-1, 0)){
					missed = 0;
				}
				else
					q.push(Packet(Add(cur.pos, std::make_pair(-1, 0)), std::make_pair(-1, 0), cur.missed));
			}
			else{
				if (cur.dir == std::make_pair(0, 1)){
					missed = 0;
				}
				else
					q.push(Packet(Add(cur.pos, std::make_pair(0, 1)), std::make_pair(0, 1), cur.missed));

			}
		}
		q.push(Packet(Add(cur.pos, cur.dir), cur.dir, cur.missed+missed));
	}
#ifdef WIN32
	system("PAUSE");
#endif

}