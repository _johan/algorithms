#include <iostream>
#include <string>
#include <set>
#include <map>
#include <vector>


int main()
{
    int N, K;
    std::cin >> N >> K;
    std::string in;
    std::cin >> in;
    std::map<char, std::set<char>> win;
    std::map<char, std::set<char>> nowin;
    std::map<char, int> schoolK;
    //std::map<char, std::vector<int>> numwins; /*Merge to the same map with a pair?*/
    for(int i = 0; i < in.size(); i++){
        if(win.find(in[i]) == win.end()){
            win[in[i]] = std::set<char>();
            win[in[i]].insert(in[i]);
            nowin[in[i]] = std::set<char>();
            schoolK[in[i]] = 1;
        }else{
            schoolK[in[i]]++;
        }
    }
    for(std::map<char,int>::iterator it = schoolK.begin(); it != schoolK.end(); ++it){
        if(it->second < K){
            win.erase(it->first);
            nowin.erase(it->first);
            //schoolK.erase(it);
        }
    }
    std::string winner("Ingen");
    for(std::map<char, std::set<char>>::iterator it = win.begin(); it != win.end(); ++it){
        if(nowin[it->first].size() > 0){
            continue;
        }else if(win[it->first].size() == win.size()){
            winner = it->first;
            //We found our winner
        }
		//std::cout << it->first << std::endl;
        for(std::map<char, std::set<char>>::iterator _it = win.begin(); _it != win.end(); ++_it){
            if(it == _it){
                continue;
            }
            //Do shit
            /*
                I.e: Traverse the string
*/
            int lhsscore = 0, rhsscore = 0;
			int numlhs = 0, numrhs = 0;
            int scoreiterator = 1;
            for(int i = 0; i < in.size(); i++){
                if(in[i] == it->first){
					if (numlhs < K)
						lhsscore+=scoreiterator;
                    ++scoreiterator;
					numlhs++;
                }else if(in[i] == _it->first){
					if (numrhs < K)
						rhsscore+=scoreiterator;
                    ++scoreiterator;
					numrhs++;
                }
            }
            if(lhsscore < rhsscore){
                //std::cout << it->first << " won over " << _it->first << std::endl;
                win[it->first].insert(_it->first);
                nowin[_it->first].insert(it->first);
            }else if(lhsscore > rhsscore){
                //std::cout << _it->first << " won over " << it->first << std::endl;

                nowin[it->first].insert(_it->first);
                win[_it->first].insert(it->first);
                //This branch is useless to keep on checking
                break;
            }else{
                nowin[it->first].insert(_it->first);
                nowin[_it->first].insert(it->first);
                //std::cout << _it->first << " tied with " << it->first << std::endl;

            }
        }
        if(win[it->first].size() == win.size()){
            winner = it->first;
            //We found our winner
        }
    }
    std::cout << winner << std::endl;
#ifdef WIN32
	system("PAUSE");
#endif
	return 0;
}

