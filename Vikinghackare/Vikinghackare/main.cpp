#include <iostream>
#include <string>
#include <vector>
#include <utility>
typedef unsigned int uint;

int main()
{
    uint N;
    std::cin >> N;
    std::vector<std::pair<char, std::string>> c(N);
    for(uint i = 0; i < N; i++){
        char k;
        std::string s;
        std::cin >> k >> s;
        c[i] = std::make_pair(k, s);
    }
    std::string comp, out;
    std::cin >> comp;
    for(uint pos = 0; pos < comp.size(); pos+=4){
        bool correct = false;
        for(uint i = 0; i < N; i++){
            std::string subs = comp.substr(pos, 4);
            if(comp.substr(pos, 4).compare(c[i].second) == 0){
                //Found it
                correct = true;
                out.append(1, c[i].first);
                //pos+= 3;
                break;
            }
        }
        if(!correct){
            out.append("?");
        }
    }
    std::cout << out << std::endl;
    return 0;
}

