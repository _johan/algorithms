#include <iostream>

int main(){
	int trgt;
	std::cin >> trgt;
	trgt *= 1000;
	if (trgt == 1){
		std::cout << 1 << std::endl;
		return 0;
	}
	int last = 1, cur = 1;
	int num = 2, sum = 2;
	while (true){
		if (sum > trgt){
			std::cout << num-1 << std::endl;
			return 0;
		}
		int nlast = cur;
		cur += last;
		last = nlast;
		sum += cur;
		num++;
	}
}