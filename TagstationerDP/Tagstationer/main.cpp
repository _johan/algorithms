#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <climits>
#include <map>
#include <unordered_map>
#include <list>
typedef long long ll;
typedef std::pair<int,int> pii;
typedef std::vector<pii> vpii;
typedef std::vector<int> vi;
typedef std::vector<vi> vvi;
typedef std::vector<bool> vb;
using namespace std;


bool compSecond(const pair<pair<ll,ll>, ll> &lhs, const pair<pair<ll,ll>, ll> &rhs){
    if(lhs.first.second == rhs.first.second){
        if(lhs.first.first == rhs.first.first){
            return lhs.second < rhs.second;
        }
        return lhs.first.first < rhs.first.first;
    }
    return lhs.first.second < rhs.first.second;
}

bool compFirst(const pair<pair<ll,ll>, ll> &lhs, const pair<pair<ll,ll>, ll> &rhs){
    if(lhs.first.first == rhs.first.first){
        if(lhs.first.first == rhs.first.first){
            return lhs.second < rhs.second;
        }
        return lhs.first.second < rhs.first.second;
    }
    return lhs.first.first < rhs.first.first;

}

int main(){
    int N;
    cin >> N;
    vector<pair<pair<ll,ll>, ll>> firststations;
    firststations.reserve(N);
    vector<pair<pair<ll,ll>, ll>> endstations;
    endstations.reserve(N);

    vi path;
    path.reserve(N);
    for(int i = 0; i < N; ++i){
        ll l, r;
        cin >> l >> r;
        if(l > r){
            firststations.push_back(make_pair(make_pair(l,r), i));
        }else{
            endstations.push_back(make_pair(make_pair(l,r), i));
        }
    }
    sort(firststations.begin(), firststations.end(), compSecond);
    sort(endstations.begin(), endstations.end(), compFirst);
    reverse(endstations.begin(), endstations.end());

    if(endstations.back().first.first != 0 || firststations.front().first.second != 0){
        cout << "NEJ" << endl;
        return 0;
    }
    ll ontrain = 0;
    for(unsigned int i = 0; i < firststations.size(); ++i){
        if(ontrain >= firststations[i].first.second){
            ontrain += firststations[i].first.first - firststations[i].first.second;
            path.push_back(firststations[i].second);
        }else{
            cout << "NEJ" << endl;
            return 0;
        }
    }
    for(unsigned int i = 0; i < endstations.size(); ++i){
        if(ontrain >= endstations[i].first.second){
            ontrain += endstations[i].first.first - endstations[i].first.second;
            path.push_back(endstations[i].second);
        }else{
            cout << "NEJ" << endl;
            return 0;
        }
    }
    /*
    if(path.size() != N){
        cout << "NEJ" << endl;
        return 0;
    }*/
    if(ontrain != 0){
        //Something weird happened
        cout << "NEJ" << endl;
        return 0;
    }
    cout << "JA" << endl;
    for(int i = 0; i < N; ++i){
        cout << path[i]+1 << " ";
    }
    cout << endl;

}
/*
 *
 *
4
0 10
4 0
8 2
2 2
*/
