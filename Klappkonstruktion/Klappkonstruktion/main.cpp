#include <iostream>
#include <algorithm>
int main(){
	int N, Sm, M, St;
	std::cin >> N >> Sm >> M >> St;
	int available = N, availablein1 = 0, avaiablein2 = 0, availablein3 = 0, dayspassed = 0;
	while (Sm > 0 || M > 0 || St > 0 || available != N){
		available += availablein1;
		availablein1 = avaiablein2;
		avaiablein2 = availablein3;
		++dayspassed;
		availablein3 = 0;
		if (St > 0){
			//
			while (available > 2 && St > 0){
				availablein3 += 3;
				available -= 3;
				--St;
			}
		}
		if (M > 0){
			while (available > 1 && M > 0){
				avaiablein2 += 2;
				available -= 2;
				--M;
			}
		}
		if (Sm > 0){
			int mn = std::min(Sm, available);
			Sm -= mn;
			available -= mn;
			availablein1 += mn;
		}
	}
	std::cout << dayspassed-1 << std::endl;
#ifdef WIN32
	system("PAUSE");
#endif
}