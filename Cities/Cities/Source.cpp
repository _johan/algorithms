#include "cities.h"
#include <vector>
#include <utility>
#include <iostream>
#include <algorithm>
#include <queue>

typedef long long ll;
using namespace std;
typedef vector<bool> vb;
typedef vector<ll> vi;
typedef vector<vi> vvi;
typedef pair<ll, ll> pii;
typedef vector<pii> vpii;

vvi nodes;

ll DFS(ll cur, ll len, const ll K, vb &visited, vpii branch, bool branched){
	/*
	//Do bfs instead
	queue<pii> q;
	//index, depth
	q.push(pii(cur, 0));
	ll ret = 0;
	while (!q.empty()){
		pii p = q.front();
		q.pop();
		visited[p.first] = true;
		if (p.second == K ){
			++ret;
			continue;
		}
		for (int i = 0; i < nodes[p.first].size(); ++i){
			if (!visited[nodes[p.first][i]]){
				q.push(pii(nodes[p.first][i], p.second + 1));
			}
		}
	}
	return ret;*/
	bool n = false;
	if (nodes[cur].size() > 2){
		n = true;
	}
	branched = max(n, branched);

	visited[cur] = true;
	/*
	if (len > K){
		return 0;
	}
	if (len == K){
		return 1;
	}
	else if (len == K){
		return 0;
	}*/
	ll add = 0;
	if (len - branch.front().second == K){
		add = 1;
	}
	if (branch.size() == 0){
		return add;
	}
	
	for (unsigned int i = 0; i < nodes[cur].size(); ++i){
		if (!visited[nodes[cur][i]]){
			if (!branched){
				branch.push_back(pii(nodes[cur][i], len+1));
			}
			add += DFS(nodes[cur][i], len + 1, K, visited, branch, branched);
		}
	}
	return add;
	
}
long long paths(int N, int K, int F[], int T[]) {
	nodes = vvi(N);
	for (int i = 0; i < N-1; ++i){
		nodes[F[i]].push_back(T[i]);
		nodes[T[i]].push_back(F[i]);
	}
	//vb gotpoints(N, false);//Could skip this and just split the result in 2
	vb visited(N, false);
	ll tot = 0;
	for (int i = 0; i < N; ++i){
		fill(visited.begin(), visited.end(), false);
		tot += DFS(i, 0, K, visited);
	}
	return tot/2;
}


/*
6 2 
1 2 3 3 5 
0 0 0 4 4
*/