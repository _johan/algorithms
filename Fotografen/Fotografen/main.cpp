#include <iostream>
#include <string>
#include <vector>
#include <climits>
#include <algorithm>
#include <utility>

using namespace std;

const char UP = 'U';
const char DOWN = 'N';
const char RIGHT = 'H';
const char LEFT = 'V';

inline void add(int &l, int &r){
	l += r;
	if (l >= 4){
		l -= 4;
	}
	else if (l < 0){
		l += 4;
	}
}

int main(){
	int N, Nrot;
	std::string rot;
	std::cin >> N >> Nrot >> rot;
	vector<int> val(N, 0);
	vector<int> rdiff(N, 0);
	int c_rot = 0;
	for (int i = 0; i < N; ++i)
	{
		char c = rot[i];
		if (c == UP){
			val[i] = 0;
		}
		else if (c == RIGHT){
			val[i] = 1;
		}
		else if (c == DOWN){
			val[i] = 2;
		}
		else{
			val[i] = 3;
		}
	}
	int res = 0;
	for (int i = 0; i < N; i++){
		add(val[i], c_rot);
		if (val[i] % 4 != 0){
			if (i + Nrot <= N){
				//Rotate
				int diff = 4 - val[i];
				res += diff;
				add(val[i], diff);
				add(c_rot, diff);
				rdiff[i + Nrot - 1] = -diff;
				/*for (int j = i; j < i + Nrot; ++j){
					add(val[j], diff);
				}*/
			}
			else{
				res = -1;
				break;
			}
		}
		add(c_rot, rdiff[i]);

	}
	for (int i = 0; i < N; i++){
		if (val[i] != 0){
			res = -1;
			break;
		}
	}
	/*

	for (int i = 0; i < N; ++i){
		add(val[i], c_rot);
		if (val[i] % 4 != 0){
			int diff = 4 - val[i];
			res += diff;
			add(c_rot, diff);

			if (i + Nrot <= N){
				rdiff[i + Nrot - 1] = -diff;
			}
			else{
				res = -1;
				break;
			}
		}
		add(c_rot, rdiff[i]);
	}*/
	cout << res << endl;
#ifdef WIN32
	system("PAUSE");
#endif
}