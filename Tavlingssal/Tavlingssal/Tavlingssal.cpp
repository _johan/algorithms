#include <iostream>
#include <cmath>
#include <climits>

#define ll long long

int main(){
	ll N;
	std::cin >> N;
	ll sqrtN = (ll)std::sqrt((double)N) + 1;

	ll least_size = LONG_MAX;
	for (ll horz_size = 1; horz_size < sqrtN; horz_size++){
		ll left = N;
		ll vert_size = (N / horz_size);
		left -= vert_size * horz_size;
		while (left > 0){
			vert_size++;
			left -= horz_size;
		}
		ll size = (horz_size + 1)*(vert_size + 1);
		if (size < least_size){
			least_size = size;
		}
	}
	std::cout << least_size << std::endl;

}
