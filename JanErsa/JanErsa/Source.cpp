#include <iostream>
#include <vector>
#include <map>
#include <climits>

typedef unsigned int uint;
typedef long long ll;

struct Node;
struct Edge{
	Edge(){}
	Edge(Node* s, Node* g, ll l){
		start = s;
		goal = g;
		len = l;
	}

	void set(Node* s, Node* g, ll l){
		start = s;
		goal = g;
		len = l;
	}

	Node* start;
	Node* goal;
	ll len;
};

struct Node{
	ll val = LONG_MAX;
	//std::vector<Edge> edges;
};

int main(){
	uint nNodes, nEdges;
	std::cin >> nNodes >> nEdges;
	//Node* n = new Node[nNodes];
	std::vector<Node> n(nNodes);
	//Edge* e = new Edge[nEdges];
	std::vector<Edge> e(nEdges);
	for (uint i = 0; i < nEdges; i++){
		uint sn, gn, len;
		std::cin >> sn >> gn >> len;
		sn--;
		gn--;
		e[i].set(&n[sn], &n[gn], len);
		//n[sn].edges.push_back(Edge(&n[gn], len));
		//n[gn].edges.push_back(Edge(&n[sn], len));
	}
	std::map<Node*, Node*> nodes;
	std::map<Node*, std::map<Node*, ll>> dist;
	for (uint i = 0; i < nNodes; i++){
		for (uint j = 0; j < nNodes; j++){
			dist[&n[i]][&n[j]] = LONG_MAX;
		}
	}

	for (uint i = 0; i < nNodes; i++){
		dist[&n[i]][&n[i]] = 0;
	}
	for (uint i = 0; i < nEdges; i++){
		dist[e[i].start][e[i].goal] = e[i].len;
		dist[e[i].goal][e[i].start] = e[i].len;
	}

	for (uint k = 0; k < nNodes; k++){
		for (uint i = 0; i < nNodes; i++){
			for (uint j = 0; j < nNodes; j++){
				if (dist[&n[i]][&n[j]] > dist[&n[i]][&n[k]] + dist[&n[k]][&n[j]]){
					dist[&n[i]][&n[j]] = dist[&n[i]][&n[k]] + dist[&n[k]][&n[j]];
					dist[&n[i]][&n[j]] = dist[&n[i]][&n[k]] + dist[&n[k]][&n[j]];
				}
			}
		}
	}
	int n1, n2, len = 0;
	for (uint i = 0; i < nNodes; i++){
		for (uint j = 0; j < nNodes; j++){
			//std::cout << "Dist between node: " << i << " and node: " << j << " is " << dist[&n[i]][&n[j]] << std::endl;
			int nlen = dist[&n[i]][&n[j]];
			if (nlen > len){
				len = nlen;
				n1 = i;
				n2 = j;
			}

		}
	}
	std::cout << n1+1 << " " << n2+1 << " " << len * 100 << std::endl;
	int q;
	std::cin >> q;
	//delete[] n;
	//delete[] e;

}