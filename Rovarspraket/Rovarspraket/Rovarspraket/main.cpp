#include <iostream>
#include <string>

using namespace std;
bool isConsonant(char c){
	if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y'){
		return false;
	}
	return true;
}
int main(){
	string org, in;
	cin >> org >> in;
	int orgpos = 0, inpos = 0;
	while (orgpos < org.size() && inpos < in.size() && org[orgpos] == in[inpos]){
		if (isConsonant(org[orgpos])){
			//Check that current in "in" is the same as org[orgpos]. If it is, check that in[inpos+2] is same as org[orgpos] and in[inpos+1] is an o
			int a = 1, b = 1;
			while (orgpos + 2 < org.size() && org[orgpos + 1] == 'o' && org[orgpos + 2] == org[orgpos]){
				orgpos += 2;
				++a;
			}
			while (inpos + 2 < in.size() && in[inpos + 1] == 'o' && in[inpos + 2] == in[inpos]){
				inpos += 2;
				++b;
			}
			if (a > b || a*2 < b){
				break;
			}
		}
		++inpos;
		++orgpos;
	}
	if (inpos == in.size() && orgpos == org.size()){
		cout << "ja" << endl;
	}
	else{
		cout << "nej" << endl;
	}
#ifdef WIN32
	system("PAUSE");
#endif
}
/*
ojojojoj
ojojoj
*/