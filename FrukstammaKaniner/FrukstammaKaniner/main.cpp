#include <iostream>
#include <vector>
typedef unsigned long long ll;


int main()
{

    int x, y;
    std::cin >> x >> y;
    std::vector<ll> added(11, 0); //Vector with how many were added
    added[0] = 1;
    for(int i = 0; i < 11; i++){
        //0 is begining
        for(int c = 0; c < i; c++){
            if(i - c >= x){
                //Add new ahres
                added[i] += y * added[c];
            }
        }
    }
    int sum = 1;
    for(int i = 1; i < 11; i++){
        sum += added[i];
        std::cout << sum << " ";
    }
    return 0;
}

