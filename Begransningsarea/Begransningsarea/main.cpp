#include <iostream>
#include <vector>
#include <algorithm>

int main(){
	int H, B, t = 0;
	std::cin >> H >> B;
	std::vector<std::vector<int>> gb(H+2, std::vector<int>(B+2, 0));
	for (int y = 0; y < H; y++){
		for (int x = 0; x < B; x++){
			std::cin >> gb[y+1][x+1];
			if (gb[y + 1][x + 1] != 0){
				t++;
			}
		}
	}
	int totArea = t*2;
	for (int y = 0; y < gb.size(); y++){
		for (int x = 0; x < gb[y].size(); x++){
			int val = gb[y][x];
			if (y + 1 < gb.size()){
				totArea += std::max(0, gb[y + 1][x] - val);
			}
			if (y - 1 >= 0){
				totArea += std::max(0, gb[y - 1][x] - val);
			}
			if (x + 1 < gb[y].size()){
				totArea += std::max(0, gb[y][x+1] - val);
			}
			if (x - 1 >= 0){
				totArea += std::max(0, gb[y][x-1] - val);
			}
		}
	}
	std::cout << totArea << std::endl;
	//system("PAUSE");
}