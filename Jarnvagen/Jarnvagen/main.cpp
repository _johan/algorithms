#include <iostream>
#include <vector>
#include <utility>
#include <climits>

int maxKm = 0;
std::vector<int> bestTimeTable;
//currentPassagers borde kunna vara en referens
void Do(const std::vector<int> &dist, std::vector<std::vector<std::pair<int,std::pair<int,int>>>> &currentPassagers/*first = came from index, second = first = max time, second = km travelled*/, int currentTime, std::vector<int> timeTable, int currentStation, int km){
	if (currentStation > dist.size()){
		//Framme, kolla om vi kan byta ut
		if (timeTable.back() == 0){
			return;
		}
		if (km > maxKm||(km == maxKm && timeTable.back() < bestTimeTable.back())){
			maxKm = km;
			bestTimeTable = timeTable;
		}
		return;
	}
	int addTime = 0, addkm = 0;
	//Passera...
	if (currentStation != dist.size()){
		addTime += dist[currentStation] / 2;
	}

	Do(dist, currentPassagers, currentTime + addTime, timeTable, currentStation + 1, km);
	currentTime++;
	timeTable[currentStation] = currentTime;
	addTime++;
	for (int i = 0; i < currentPassagers[currentStation].size(); i++){
		bool validorigin = false;
		if (timeTable[currentPassagers[currentStation][i].first] != 0 || currentPassagers[currentStation][i].first == 0)
			validorigin = true;
		if (currentPassagers[currentStation][i].second.first >= currentTime - timeTable[currentPassagers[currentStation][i].first] && validorigin){
			addkm += currentPassagers[currentStation][i].second.second;
		}
	}
	Do(dist, currentPassagers, currentTime + addTime, timeTable, currentStation + 1, km + addkm);

	//Man kan stanna p� en station och man kan passera den
	//Om man stannar kan man plocka upp en av de som v�ntar d�r eller flera (Ladda p� alla och kolla sedan om de kommer fram i tid)
	//Man kan inte �ka bak�t
}
int main(){
	int N, P;
	std::cin >> N >> P;
	bestTimeTable = std::vector<int>(N, INT_MAX);
	std::vector<int> stationDistance(N - 1);
	for (int i = 0; i < N - 1; i++){
		std::cin >> stationDistance[i];
	}
	int origin, dest, maxtime;
	std::vector<std::vector<std::pair<int, std::pair<int,int>>>> passagers(N);
	for (int i = 0; i < P; i++){
		std::cin >> origin >> dest >> maxtime;
		origin--;
		dest--;
		int km = 0;
		for (int c = origin; c < dest; c++){
			km += stationDistance[c];
		}
		passagers[dest].push_back(std::make_pair(origin, std::make_pair(maxtime, km)));
	}
	Do(stationDistance, passagers, 1, std::vector<int>(N, 0), 0, 0);
	std::cout << maxKm << std::endl;
	bestTimeTable[0] = 0;
	for (int i = 0; i < N; i++){
		if (bestTimeTable[i] == 0 && i != 0)
			continue;
		std::cout << i + 1 << " " << bestTimeTable[i] << std::endl;
	}
#ifdef WIN32
	system("PAUSE");
#endif
}