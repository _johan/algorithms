#define _USE_MATH_DEFINES
#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>
#include <cmath>
#include <math.h>
#include <limits>
#include <utility>

#define DOUBLE_MAX 2000000000
#define DEG_RAD  M_PI / 180
double Do(std::vector<double> &e, int n){
    if(n > 4){
        return 0;
    }
    //for()
    return 0;
}

int main()
{
    std::cout.precision(std::numeric_limits<double>::max_digits10);
    int N;
    double minA, radius;
	std::cout << "Waiting for input.." << std::endl;
    std::cin >> N >> minA >> radius;
	std::cout << "N: " << N << ", minA: " << minA << ", radius: " << radius << std::endl;
    //std::cout << std::endl;
    std::vector<double> e(N);
	std::cout << "Start reading" << std::endl;
    for(int i = 0; i < N; i++){
        std::cin >> e[i];
    }
	std::cout << "Done reading" << std::endl;
    double lowestDiff = DOUBLE_MAX;

    std::sort(e.begin(), e.end());
	std::vector<std::pair<double, double>> coord(N);
	std::vector<std::vector<double>> edgeLenght(N, std::vector<double>(N, 0));
	//std::vector<std::vector<std::vector<double>>> areas(N, std::vector<std::vector<double>>(N, std::vector<double>(N, 0)));
	for (int i = 0; i < N; i++){
		coord[i] = std::make_pair((double)std::cos(e[i] * DEG_RAD), (double)std::sin(e[i] * DEG_RAD));
	}
	/*
	for (int i = 0; i < N; i++){
		for (int c = 0; c < N; c++){
			if (edgeLenght[i][c] != 0){
				double el = 
				edgeLenght[i][c] = ;
			}
		}
	}*/
//Inga intersects
    int areassize = (N*(N - 1)*(N - 2) / 6 + 1);
    std::vector<double> areas;
    areas.reserve(areassize);
	std::cout << "areassize: " << (N*(N - 1)*(N - 2) / 6 + 1) << std::endl;
    int start = N-2;
    int sum = 0;
    while(start > 0){
        sum += start;
        start--;
    }
    std::cout << "sum: " << sum << std::endl;
    std::vector<std::vector<double>> _areas(N, std::vector<double>(N));
    int iterations = 0;
	for(int i = 0; i < N; i++){
        for(int o = i+1; o < N; o++){
            if(o == i)
                continue;
            for(int p = o+1; p < N; p++){
                if(p == i || p == o)
                    continue;
				double xi = coord[i].first, yi = coord[i].second, xo = coord[o].first, yo = coord[o].second;
				double xp = coord[p].first, yp = coord[p].second;//, xk = coord[k].first, yk = coord[k].second
				//double xi = std::cos(e[i] * DEG_RAD) * radius, yi = std::sin(e[i] * DEG_RAD)* radius, xo = std::cos(e[o] * DEG_RAD) * radius, yo = std::sin(e[o] * DEG_RAD)* radius;
				//double xp = std::cos(e[p] * DEG_RAD)* radius, yp = std::sin(e[p] * DEG_RAD)* radius, xk = std::cos(e[k] * DEG_RAD)* radius, yk = std::sin(e[k] * DEG_RAD)* radius; //Räkna ut detta innan och sätt det i en annan array(Efter att e blivit sorterad)

				double iolen = std::sqrt((xi - xo)*(xi - xo) + (yi - yo)*(yi - yo)), oplen = std::sqrt((xi - xp)*(xi-xp) + (yi-yp)*(yi-yp)), iplen = std::sqrt((xi - xp)*(xi-xp) + (yi-yp)*(yi-yp));
				//double a1 = areas[i][o][p];

				double A = std::sin(std::acos(((iolen*iolen - oplen*oplen - iplen*iplen) / (2 * oplen*iolen)))) * iolen * oplen / 2;
				areas.push_back(A);
                iterations++;
            }
        }
    }
    std::cout << "iterations: " << iterations << std::endl;
    return 0;
	//double lowestDiff = DOUBLE_MAX;
    for (int i = 0; i < areassize; i++){
        for (int c = i+1; c < areassize; c++){
			double A = areas[i] + areas[c];
			std::cout << A << ", area i: " << areas[i] << ", areas c: "<< areas[c] << std::endl;
			if (A >= minA && A - minA < lowestDiff){
				lowestDiff = A - minA;
			}
		}
	}
	std::cout << "Done" << std::endl;
    if(lowestDiff == DOUBLE_MAX){
        std::cout << -1 << std::endl;
    }else
      std::cout << std::fixed << lowestDiff + minA << std::endl;
#ifdef WIN32
	system("PAUSE");
#endif
	//int q;
	//std::cin >> q;
    return 0;
}

// 2^0.5^2

/*
200 1 1
1 2 3 4 5 6 7 8 9 10 11 12 13 14 15
16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
31 32 33 34 35 36 37 38 39 40 41 42 43 44 45
46 47 48 49 50 51 52 53 54 55 56 57 58 59 60
61 62 63 64 65 66 67 68 69 70 71 72 73 74 75
76 77 78 79 80 81 82 83 84 85 86 87 88 89 90
91 92 93 94 95 96 97 98 99 100 101 102 103 105
106 107 108 109 110 111 112 113 114 115 116 117 118 119 120
121 122 123 124 125 126 127 128 129 130 131 132 133 134 135
136 137 138 139 140 141 142 143 144 145 146 147 148 149 150
151 152 153 154 155 156 157 158 159 160 161 162 163 164 165
166 167 168 169 170 171 172 173 174 175 176 177 178 179 180
181 182 183 184 185 186 187 188 189 190 191 192 193 194 195
196 197 198 199 200 201
*

/*
Gör en spn där mattegrej
Om båda noderna finns gör en BFS och kolla om man kan nå den andra från den första, om man inte kan det, lägg till edgen
*/
