#include <iostream>
#include <vector>
#include <algorithm>
#include <climits>

using namespace std;

vector<int> primes;
int N;


bool availableNumber(int num, vector<bool> &vb){
	if (!vb[num % 10]){
		vb[num % 10] = true;
	}
	else{
		return false;
	}
	if (!vb[(num/10) % 10]){
		vb[(num / 10) % 10] = true;
	}
	else{
		vb[num % 10] = false;
		return false;
	}
	if (!vb[(num/100) % 10]){
		vb[(num / 100) % 10] = true;
	}
	else{
		vb[num % 10] = false;
		vb[(num / 10) % 10] = false;
		return false;
	}
	return true;
}

void rmNumber(int num, vector<bool> &vb){
	vb[num % 10] = false;
	vb[(num / 10) % 10] = false;
	vb[(num / 100) % 10] = false;
}
int main(){
	primes.reserve(1000);
	primes.push_back(2);
	for (int i = 3; i < 1000; ++i){
		bool avail = true;
		for (int d = 0; d < primes.size(); ++d){
			if (i % primes[d] == 0){
				avail = false;
			}
		}
		if (avail){
			primes.push_back(i);
		}
	}
	N = primes.size();
	int high = 0, low = INT_MAX, num = 0;

	//V�lj 3 tal
	vector<bool> used(10, false);
	for (int i = 0; i < N; ++i){
		fill(used.begin(), used.end(), false);
		if (!availableNumber(primes[i], used)){
			continue;
		}
		for (int j = i+1; j < N; ++j){
			if (!availableNumber(primes[j], used)){
				continue;
			}
			for (int h = j+1; h < N; ++h){
				if (!availableNumber(primes[h], used)){
					continue;
				}
				bool valid = true;
				for (int l = 1; l < 10; ++l){
					if (!used[l]){
						valid = false;
						break;
					}
				}
				if (!valid){
					rmNumber(primes[h], used);
					continue;
				}
				int sum = primes[i] + primes[j] + primes[h];
				//cout << "adding: " << primes[i] << ", " << primes[j] << ", " << primes[h] << endl;
				high = max(high, sum);
				low = min(low, sum);
				++num;
				rmNumber(primes[h], used);
			}
			rmNumber(primes[j], used);
		}
	}
	cout << num << endl <<low << endl <<high << endl;
#ifdef WIN32
	system("PAUSE");
#endif
}