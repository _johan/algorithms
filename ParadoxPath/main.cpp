#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <utility>
#include <algorithm>
#define Node(x,y) std::make_pair(x,y)
#define NtoI(mapWidth, x, y) (y * mapWidth + x)
typedef std::pair<int,int> Node;
int FindPath(const int nStartX, const int nStartY,
             const int nTargetX, const int nTargetY,
             const unsigned char* pMap, const int nMapWidth, const int nMapHeight,
             int* pOutBuffer, const int nOutBufferSize){
    Node start = Node(nStartX, nStartY);
    Node end = Node(nTargetX, nTargetY);
    //std::vector<std::vector<int>> gb(nMapWidth, std::vector<int>(nMapHeight));
    std::map<Node, int> dist;
    std::map<Node, Node> prev;
    std::set<std::pair<int,Node>> q; //dist,prev, x,y
    q.insert(std::make_pair(0, start));
    dist[start] = 0;
    prev[start] = start;
    while(!q.empty()){
        std::pair<int,Node> _cur = *q.begin();
        Node cur = _cur.second;
        if(cur == end){
            //We've reached the end...
            break;
        }
        q.erase(q.begin());
        int curDist = dist[cur];
        int index = NtoI(nMapWidth, cur.first-1, cur.second);

        if(cur.first - 1 >= 0){
            if((dist.find(Node(cur.first-1, cur.second)) == dist.end() || dist.find(Node(cur.first-1, cur.second))->second > curDist+1) && index < nMapWidth * nMapHeight && pMap[index] == 1){
                //Replace it

                if(dist.find(Node(cur.first-1, cur.second)) == dist.end() || q.find(std::make_pair(dist.find(Node(cur.first-1, cur.second))->second, Node(cur.first-1, cur.second))) != q.end())
                    q.erase(std::make_pair(dist.find(Node(cur.first-1, cur.second))->second, Node(cur.first-1, cur.second)));
                dist[Node(cur.first-1, cur.second)] = curDist+1;
                prev[Node(cur.first-1, cur.second)] = cur;
                q.insert(std::make_pair(curDist+1,Node(cur.first-1, cur.second)));
            }
        }
        index = NtoI(nMapWidth, cur.first, cur.second-1);

        if(cur.second - 1 >= 0){
            if((dist.find(Node(cur.first, cur.second-1)) == dist.end() || dist.find(Node(cur.first, cur.second-1))->second > curDist+1) && index < nMapWidth * nMapHeight && pMap[index] == 1){
                //Replace it

                if(dist.find(Node(cur.first, cur.second-1)) == dist.end() || q.find(std::make_pair(dist.find(Node(cur.first, cur.second-1))->second, Node(cur.first, cur.second-1))) != q.end())
                    q.erase(std::make_pair(dist.find(Node(cur.first, cur.second-1))->second, Node(cur.first, cur.second-1)));

                dist[Node(cur.first, cur.second-1)] = curDist+1;
                prev[Node(cur.first, cur.second-1)] = cur;
                q.insert(std::make_pair(curDist+1,Node(cur.first, cur.second-1)));
            }
        }
        index = NtoI(nMapWidth, cur.first, cur.second+1);

        if((dist.find(Node(cur.first, cur.second+1)) == dist.end() || dist.find(Node(cur.first, cur.second+1))->second > curDist+1) && index < nMapWidth * nMapHeight && pMap[index] == 1){
            //Replace it

            if(dist.find(Node(cur.first, cur.second+1)) == dist.end() || q.find(std::make_pair(dist.find(Node(cur.first, cur.second+1))->second, Node(cur.first, cur.second+1))) != q.end())
                q.erase(std::make_pair(dist.find(Node(cur.first, cur.second+1))->second, Node(cur.first, cur.second+1)));

            dist[Node(cur.first, cur.second+1)] = curDist+1;
            prev[Node(cur.first, cur.second+1)] = cur;
            q.insert(std::make_pair(curDist+1,Node(cur.first, cur.second+1)));
        }
        index = NtoI(nMapWidth, cur.first+1, cur.second);

        if((dist.find(Node(cur.first+1, cur.second)) == dist.end() || dist.find(Node(cur.first+1, cur.second))->second > curDist+1) && index < nMapWidth * nMapHeight && pMap[index] == 1){
            //Replace it

            if(dist.find(Node(cur.first+1, cur.second)) == dist.end() || q.find(std::make_pair(dist.find(Node(cur.first+1, cur.second))->second, Node(cur.first+1, cur.second))) != q.end())
                q.erase(std::make_pair(dist.find(Node(cur.first+1, cur.second))->second, Node(cur.first+1, cur.second)));

            dist[Node(cur.first+1, cur.second)] = curDist+1;
            prev[Node(cur.first+1, cur.second)] = cur;
            q.insert(std::make_pair(curDist+1,Node(cur.first+1, cur.second)));
        }
    }

    if(prev.find(end) == prev.end()){
        return -1;
    }
    int ret = dist[end];
    if(ret > nOutBufferSize){
        return ret;
    }
    std::vector<int> path;
    path.reserve(ret);
    Node c = end, n;
    int _index = ret-1;
    while((n = prev[c]) != c){
        //int index = (nMapHeight / c.second) * c.first;
        //index -= c.first;//NtoI(nMapWidth, c.first, c.second)
        pOutBuffer[_index] = NtoI(nMapWidth, c.first, c.second);
        //path.push_back();
        _index--;
        c = n;
    }
    //std::reverse(path.begin(), path.end());
    //std::reverse(&pOutBuffer[0], &pOutBuffer[ret-1]);
    //pOutBuffer = &path[0];
    return ret;
}



int main()
{
    unsigned char pMap[] = {1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1};
    int pOutBuffer[12];
    int ret = FindPath(0, 0, 1, 2, pMap, 4, 3, pOutBuffer, 12);
    std::cout << ret << "\n";
    for(int i = 0; i < ret; i++){
        std::cout << pOutBuffer[i] << std::endl;
    }
    return 0;
}

