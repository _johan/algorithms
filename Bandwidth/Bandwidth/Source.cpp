#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <climits>
#include <cmath>
#include <algorithm>
#include <utility>
/*
Floyd Warshall �r f�r l�ngsam
Kan jag bara g�ra en BFS fr�n nod 0 till alla andra och ta den minsta
*/
typedef long long ll;
typedef unsigned int uint;


struct Node;
struct Edge{
	Edge(){}
	Edge(Node* n, uint __b){
		endNode = n;
		bandwidth = __b;
	}
	Node* endNode;
	int bandwidth;
};

struct Node{
	std::vector<Edge> edges;
	int val;
	uint index;

};
std::vector<Node> n;

class comp{
	bool reverse;
public:
	comp(const bool& revparam = false)
	{
		reverse = revparam;
	}
	bool operator ()(const std::pair<int, int>& lhs, const std::pair<int, int>& rhs){
		if (lhs.second == rhs.second){
			return lhs.first < rhs.first;
		}
		if (lhs.second > rhs.second){
			return true;
		}
		return false;
	}

};

bool compare(const int& lhs, const int& rhs){
	if (n[lhs].val > n[rhs].val){
		return true;
	}
	return false;
}
/*
void changeval(std::deque<int> &q, int index, int newval){
	n.erase(n.begin() + index);
	for (uint i = 0; i < q.size(); i++){
		if (n[q[i]].val < newval){
			q.insert(q.begin() + i, newval);
			return;
		}
	}
	//Here nothing matched
	q.push_back(newval); //Put it in the back
	return;
	
}*/
//Kan vara problem med undirected graph (Edges fram o tillbaka)
int main(){
	uint nComps, nEdges;
	std::cin >> nComps >> nEdges;
	n.resize(nComps);
	for (uint i = 0; i < nComps; i++){
		n[i].index = i;
		n[i].val = 0;
	}
	for (uint i = 0; i < nEdges; i++){
		uint c1, c2, bandwidth;
		std::cin >> c1 >> c2 >> bandwidth;
		n[c1].edges.push_back(Edge(&n[c2], bandwidth));
		n[c2].edges.push_back(Edge(&n[c1], bandwidth));
	}
	std::set<std::pair<int, int>, comp> q;
	std::vector<bool> vis(nComps, false);
	for (uint i = 0; i < nComps; i++){
		q.insert(std::make_pair(i, 0));
		//q[i].first = i;
		//q[i].second = 0;
	}
	vis[0] = true;
	uint numVis = 1;
	while (numVis < nComps){
		numVis++;
		Node curNode = n[q.begin()->first];
		vis[curNode.index] = true;
		q.erase(q.begin());
		for (Edge e : curNode.edges){
			if (!vis[e.endNode->index] && n[e.endNode->index].val < e.bandwidth){
				q.erase(std::make_pair(n[e.endNode->index].index, n[e.endNode->index].val));
				n[e.endNode->index].val = e.bandwidth;
				q.insert(std::make_pair(n[e.endNode->index].index, n[e.endNode->index].val));
			}
		}
	}

	//Det �r den minsta osm inte �r 0
	std::cout << std::endl;
	uint min = INT_MAX;
	for (uint i = 1; i < nComps; i++){
		if (min > n[i].val){
			min = n[i].val;
		}
		//std::cout << n[i].val << " ";
	}
	//std::cout << std::endl;
	//std::cout << std::endl;
	/*for (uint v : bandwidth){
		if (v < min){
			min = v;
		}
		std::cout << v << " ";
	}*/
	//std::cout << std::endl;
	std::cout << min << std::endl;
#ifdef WIN32
	system("PAUSE");
#endif



}