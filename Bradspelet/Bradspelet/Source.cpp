#include <iostream>
#include <vector>
#include <string>
using namespace std;

char A = 'A', B = 'B';
inline char alternatePlayer(char cur){
	if (cur == A){
		return B;
	}
	return A;
}
inline int alternateTurn(int turn){
	if (turn == 0 ){
		return 1;
	}
	return 0;
}
vector<vector<vector<int>>> dp;
bool favorableOutcome(int turn, int w, int h){
	bool _reverse = false;
	if (turn == 0){
		_reverse = true;
	}
	if (_reverse){
		return dp[turn][h][w] < 0;
	}
	return dp[turn][h][w] > 0;

}
int Play(int turn, int w1, int h1, int w2, int h2){
	if (dp[turn][h1][w1] != 0 && dp[turn][h2][w2]){
		if (favorableOutcome(turn, w1, h1)){
			return dp[turn][h1][w2];
		}
		return dp[turn][h2][w2];
	}
	if (dp[turn][h1][w1] != 0 && favorableOutcome(turn, w1, h1)){
		return dp[turn][h1][w1];
	}
	if (dp[turn][h2][w2] != 0 && favorableOutcome(turn, w2, h2)){
		return dp[turn][h2][w2];
	}
	if (w1 == 1 && h1 == 1 && w2 == 1 && h2 == 1){
		if (turn == 0){
			return -1;
		}
		return 1;
	}
	int best;
	if (turn == 0){
		best = -1000000;
	}
	else{
		best = 1000000;
	}
	for (int i = 1; i < w1; ++i){
		int val = Play(alternateTurn(turn), i, h1, w1 - i, h1);
		if (turn == 0 && val > best){
			best = val;
		}
		else if (turn == 1 && val < best){
			best = val;
		}
	}
	for (int i = 1; i < w2; ++i){
		int val = Play(alternateTurn(turn), i, h2, w2 - i, h2);
		if (turn == 0 && val > best){
			best = val;
		}
		else if (turn == 1 && val < best){
			best = val;
		}
	}
	for (int i = 1; i < h1; ++i){
		int val = Play(alternateTurn(turn), w1, i, w1, h1-i);
		if (turn == 0 && val > best){
			best = val;
		}
		else if (turn == 1 && val < best){
			best = val;
		}
	}
	for (int i = 1; i < h2; ++i){
		int val = Play(alternateTurn(turn), w2, i, w2, h2 - i);
		if (turn == 0 && val > best){
			best = val;
		}
		else if (turn == 1 && val < best){
			best = val;
		}
	}
	dp[turn][h1][h2] = best;
	dp[turn][h2][w2] = best;
	return best;

}
int main(){
	int H, W;
	cin >> H >> W;
	dp = vector<vector<vector<int>>>(2, vector<vector<int>>(H+1, vector<int>(W+1, 0)));
	//B�sta �r att alttid ta den som �r j�mn
	char curplayer = A;
	int b = Play(0, W, H, W, H);
	if (b > 0){
		//A wins
		cout << 'A' << endl;
	}
	else{
		cout << 'B' << endl;
	}
	/*
	while (H != 1 || W != 1){
		if (H % 2 == 0){
			//Dela H
			H /= 2;
		}
		else if (W % 2 == 0){
			//Dela W
			W /= 2;
		}
		else if (H == 1){
			//Split W
			W /= 2;
		}
		else if (W == 1){
			H /= 2;
		}
		else{
			//Just split W
			W /= 2;
		}
		curplayer = alternatePlayer(curplayer);
	}*/
	//cout << curplayer << endl;
#ifdef WIN32
	system("PAUSE");
#endif
}