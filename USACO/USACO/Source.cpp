#include <iostream>
#include <vector>
#include <fstream>
#include <utility>
#include <algorithm>

int main(){
	std::ifstream ifs("speeding.in");
	std::cin.rdbuf(ifs.rdbuf());
	std::ofstream ofs("speeding.out");
	std::cout.rdbuf(ofs.rdbuf());
	int N, M; 
	std::cin >> N >> M;
	std::vector < std::pair<int, int>> road(N);
	std::vector<std::pair<int, int>> bess(M);
	for (int i = 0; i < N; i++){
		int lhs, rhs;
		std::cin >> lhs >> rhs;
		road[i] = std::make_pair(lhs, rhs);
	}
	for (int i = 0; i < M; i++){
		int lhs, rhs;
		std::cin >> lhs >> rhs;
		bess[i] = std::make_pair(lhs, rhs);
	}
	int mx = 0;
	while (bess.size() > 0 && road.size() > 0){
		if (road.front().first > bess.front().first){
			road.front().first -= bess.front().first;
			mx = std::max(bess.front().second - road.front().second, mx);
			bess.erase(bess.begin());
		}
		else if (road.front().first < bess.front().first){
			bess.front().first -= road.front().first;
			mx = std::max(bess.front().second - road.front().second , mx);
			road.erase(road.begin());
		}
		else{
			//They are equal
			mx = std::max(bess.front().second - road.front().second, mx);
			road.erase(road.begin());
			bess.erase(bess.begin());

		}
	}
	std::cout << mx << std::endl;
}