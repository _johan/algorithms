#include <iostream>
#include <vector>
#include <string>
#include <map>
using namespace std;
char P1 = 'o', P2 = 'x', E = '.';
vector<string> _gb(3, string(3, '.'));

inline int toIndex(pair<int, int> p){
	return p.first + p.second * 3;
}
inline pair<int, int> toVec2(int index){
	int layer = index / 3;
	return make_pair(index - layer * 3, layer);
}

bool won(vector<string> &gb, char lf){
	//Scan horizontally
	for (int i = 0; i < 3; ++i){
		int numoccurenses = 0;
		for (int j = 0; j < 3; ++j){
			if (gb[i][j] == lf){
				++numoccurenses;
			}
		}
		if (numoccurenses == 3){
			return true;
		}
	}
	//Scan vertically
	for (int j = 0; j < 3; ++j){
		int numoccurenses = 0;
		for (int i = 0; i < 3; ++i){
			if (gb[i][j] == lf){
				++numoccurenses;
			}
		}
		if (numoccurenses == 3){
			return true;
		}
	}
	//Scan diagonals
	int occurenses = 0;
	for (int i = 0; i < 3; ++i){
		if (gb[i][i] == lf){
			++occurenses;
		}
	}
	if (occurenses == 3){
		return true;
	}

	occurenses = 0;
	for (int i = 2; i >= 0; --i){
		if (gb[2-i][i] == lf){
			++occurenses;
		}
	}
	if (occurenses == 3){
		return true;
	}
	return false;
}

char who_won(vector<string> &gb){
	if (won(gb, P1)){
		return P1;
	}
	if (won(gb, P2)){
		return P2;
	}
	return E;
}

bool game_over(vector<string> &gb){
	bool full = true;
	for (int i = 0; i < 3; ++i){
		for (int j = 0; j < 3; ++j){
			if (gb[i][j] == '.'){
				full = false;
			}
		}
	}
	if (full){
		return true;
	}
	if (won(gb, P1)){
		return true;
	}
	if (won(gb, P2)){
		return true;
	}
	return false;
}
inline char alternatePlayer(char cur){
	if (cur == P1){
		return P2;
	}
	return P1;
}
inline int alternateturn(int cur){
	if (cur == 0){
		return 1;
	}
	return 0;
}

//int best = -100000000000;
std::map<std::vector<string>, pair<int, int>> bmap;
std::map<std::vector<string>, int> bimap;

int Play(int turn,char player, vector<string> &gb){
	std::map<std::vector<string>, int>::iterator mip = bimap.find(gb);
	if (mip != bimap.end()){
		return mip->second;
	}
	if (game_over(gb)){
		char w = who_won(gb);
		if (w == P1){
			return -1;
		}
		else if (w == P2){
			return 1;
		}
		else{
			return 0;
		}
	}
	int best;
	pair<int, int> bmove;
	if (player == P1){
		best = 1000000000;
	}
	else{
		best = -1000000000;
	}
	for (int i = 0; i < 9; ++i){
		pair<int, int> m = toVec2(i);
		if (gb[m.second][m.first] == '.'){
			gb[m.second][m.first] = player;
			int val = Play(alternateturn(turn), alternatePlayer(player), gb);
			if (player == P1 && val < best){
				best = val;
				bmove = m;
			}
			if (player == P2 && val > best){
				best = val;
				bmove = m;
			}
			gb[m.second][m.first] = '.';
		}
	}
	if (player == P2){
		bimap[gb] = best;
		bmap[gb] = bmove;
	}
	return best;
}

void getGb(vector<string> &gb){
	for (int i = 0; i < 3; ++i){
		cin >> gb[i];
	}
}

void putGb(vector<string> &gb){
	for (int i = 0; i < 3; ++i){
		for (int j = 0; j < 3; ++j){
			cout << gb[i][j];
		}
		cout << endl;
	}
}
int main(){
	string start;
	cin >> start;
	if (start.compare("first") == 0){
		//Vi g�r v�rt drag f�rst
		/*
		Play(0, P2, _gb);
		pair<int, int> bmove = bmap[_gb];
		_gb[bmove.second][bmove.first] = 'x';
		putGb(_gb);
		Basic startegier s�ger att vi ska s�tta den i ett h�rn
		*/
		_gb[0][2] = P2;
		putGb(_gb);
	}
	while (!game_over(_gb)){
		getGb(_gb);
		if (game_over(_gb)){
			return 0;
		}
		Play(0, P2, _gb);
		pair<int, int> bmove = bmap[_gb];
		_gb[bmove.second][bmove.first] = 'x';
		putGb(_gb);
	}
}