#include <iostream>
#include <vector>
#include <set>
struct DisjointNode{
	DisjointNode(){}
	DisjointNode(DisjointNode* parent, int index, int rank){
		this->parent = parent;
		this->index = index;
		this->rank = rank;
	}

	DisjointNode* parent;
	int index;
	int rank;

};

/*
This class holds indexes to another container (probably vector)
That means that all sorting must be done before doing anything with this class
Otherwise indexing will be broken
*/
class DisjointSet{
	int numelements;
	DisjointNode *dn;
public:
	DisjointSet(int numelements){
		dn = new DisjointNode[numelements];
		this->numelements = numelements;
		for (int i = 0; i < numelements; i++){
			dn[i] = DisjointNode(&dn[i], i, 0);
		}
	}
	DisjointSet(){}
	//Takes an index
	int Find(int x){
		//Returns the root of the value being found
		//Or if it didn't find any value
		DisjointNode *t = &dn[x];
		if (t->parent != t){
			Find(t->parent->index);
		}
		return t->parent->index;
	}

	void Union(int x, int y){
		DisjointNode * xRoot = &dn[Find(x)];
		DisjointNode * yRoot = &dn[Find(y)];
		if (xRoot == yRoot){
			return;
		}
		if (xRoot->rank < yRoot->rank){
			xRoot->parent = yRoot;
		}
		else if (xRoot->rank > yRoot->rank){
			yRoot->parent = xRoot;
		}
		else{
			yRoot->parent = xRoot;
			++xRoot->rank;
		}
	}
	DisjointNode* getNode(){ return dn; }
};





const int NUMTHROWS = 20;
struct Trip{
	Trip() : x(0), y(0), z(0){}
	Trip(int _x, int _y, int _z) : x(_x), y(_y), z(_z){}
	int x;
	int y;
	int z;
	void sub(){
		x--;
		y--;
		z--;
	}
};

//Maybe I should have a disjoint set struct to see which I can't have
void Do(const std::vector<std::vector<bool>> &tarn, std::vector<std::set<int>> &tarns, std::vector<DisjointSet> ds, bool &done, int curTarn){
	if (done){
		return;
	}
	if (curTarn >= tarns.size()){
		return;
	}
	if (tarns[curTarn].size() == 4){
		Do(tarn, tarns, ds, done, curTarn + 1);
		return;
	}
	if (tarns[0].size() == 4 && tarns[1].size() == 4 && tarns[2].size() == 4){
		done = true;
		return;
	}
	for (int i = 0; i < 12; i++){
		std::set<int>::iterator it = tarns[curTarn].begin();

		if (it == tarns[curTarn].end() || ds[curTarn].Find(i) != ds[curTarn].Find(*it)){
			//This is possible   !tarn[curTarn][i] &&
			DisjointSet lds = ds[curTarn];
			DisjointSet nds = ds[curTarn];
			tarns[curTarn].insert(i);
			nds.Union(curTarn, i);
			ds[curTarn] = nds;
			Do(tarn, tarns, ds, done, curTarn);
			if (done)
				return;
			ds[curTarn] = lds;
			tarns[curTarn].erase(i);
		}
	}
}

int main(){
	std::vector < Trip> throws(NUMTHROWS);
	for (int i = 0; i < NUMTHROWS; i++){
		std::cin >> throws[i].x >> throws[i].y >> throws[i].z;
		throws[i].sub();
	}

	std::vector<std::vector<bool>> tarn(12, std::vector<bool>(12, false)); //If true, these two numbers can't be on the same tarn 
	for (int i = 0; i < NUMTHROWS; i++){
		tarn[throws[i].x][throws[i].y] = true;
		tarn[throws[i].x][throws[i].z] = true;

		tarn[throws[i].y][throws[i].x] = true;
		tarn[throws[i].y][throws[i].z] = true;

		tarn[throws[i].z][throws[i].y] = true;
		tarn[throws[i].z][throws[i].x] = true;
	}

	DisjointSet ds(12);
	for (int i = 0; i < tarn.size(); i++){
		for (int c = 0; c < tarn[i].size(); c++){
			if (tarn[i][c])
				ds.Union(c, i);
		}
	}
	std::vector<std::vector<int>> tarningar(3);
	std::set<int> roots;
	for (int i = 0; i < 12; i++){
		roots.insert(ds.Find(i));
	}
	std::vector<std::set<int>> tarns(3);
	//std::vector<DisjointSet> ds(3, DisjointSet(12));
	bool done = false;
	Do(tarn, tarns, std::vector<DisjointSet>(3,DisjointSet(12)), done, 0);
	for (int i = 0; i < tarns.size(); i++){
		for (int c : tarns[i]){
			std::cout << c << " ";
		}
		std::cout << std::endl;
	}
	system("PAUSE");
}