#include <iostream>
#include <vector>
#include <string>
#include <utility>
//N�r man kommer till en INPUT, l�s in talet med cin d�

int main(){
	int N;
	std::cin >> N;
	std::vector<std::pair<std::string, std::pair<int, int>>> commands(N);
	for (int i = 0; i < N; i++){
		std::cin >> commands[i].first;
		if (commands[i].first.compare("INPUT") == 0 || commands[i].first.compare("OUTPUT") == 0){
			//Only read first
			std::cin >> commands[i].second.first;
		}
		else if (commands[i].first.compare("HALT") != 0){
			//Read both ints
			std::cin >> commands[i].second.first >> commands[i].second.second;
		}
		else{
			//This is a halt
		}
	}
	std::vector<int> m_register(256, 0);
	for (int i = 0; i < N; i++){
		//std::cout << "i is: " << i << std::endl;
		if (commands[i].first.compare("CONST") == 0){
			m_register[commands[i].second.second] = commands[i].second.first;

		}
		else if (commands[i].first.compare("ADD") == 0){
			m_register[commands[i].second.second] += m_register[commands[i].second.first];
		}
		else if (commands[i].first.compare("SUB") == 0){
			m_register[commands[i].second.second] -= m_register[commands[i].second.first];
		}
		else if (commands[i].first.compare("JNZ") == 0){
			if (m_register[commands[i].second.first] != 0){
				i = commands[i].second.second-1;
			}
		}
		else if (commands[i].first.compare("INPUT") == 0){
			std::cin >> m_register[commands[i].second.first];
		}
		else if (commands[i].first.compare("OUTPUT") == 0){
			std::cout << m_register[commands[i].second.first] << std::endl;
		}
		else if (commands[i].first.compare("HALT") == 0){
#ifdef WIN32
			system("PAUSE");
#endif
			return 0;
		}
	}
#ifdef WIN32
	system("PAUSE");
#endif
}