#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <list>
#include <climits>
#include <set>
#include <iterator>
#include <tuple>
typedef long long ll;

bool comp(const std::pair<ll, std::pair<ll, ll>> &lhs, const std::pair<ll, std::pair<ll, ll>> &rhs){
    if (lhs.second.second == 0 && rhs.second.second == 0){
        return lhs.second.first < rhs.second.first;
    }
    if (lhs.second.second == 0){
        return true;
    }
    else if (rhs.second.second == 0){
        return false;
    }
    if (lhs.second.first > rhs.second.first){
        return true;
    }
    return false;
}

class setcomp{
public:
    bool operator() (const std::tuple<ll, ll,ll,ll> &lhs, const std::tuple<ll, ll,ll,ll> &rhs){
        if (std::get<2>(lhs) == 0 && std::get<2>(rhs) == 0){
            return std::get<1>(lhs) < std::get<1>(rhs);
        }
        if (std::get<2>(lhs) == 0){
            return true;
        }
        else if (std::get<2>(rhs) == 0){
            return false;
        }
        if (std::get<1>(lhs) > std::get<1>(lhs)){
            return true;
        }
        return false;
    }
};

void DFS(std::vector<ll> path, ll on, std::vector<std::pair<ll,ll>> &stat, std::vector<ll> c){
    if(c.size() == 0){
        std::cout << "JA" << std::endl;
        for(ll i : path){
            std::cout << (i+1) << " ";
        }
        std::cout << std::endl;
#ifdef WIN32
        system("PAUSE");
#endif
        exit(0);
    }
    for(ll i = 0; i < c.size(); i++){
        if(on < stat[c[i]].second){
            continue;
        }
        ll it = c[i];
        path.push_back(c[i]);
        c.erase(c.begin()+i);
        DFS(path, on + stat[it].first - stat[it].second, stat, c);
        c.push_back(it);
    }
}
//tuple: orginalindex, på, av, val
void BFS(std::vector<std::tuple<ll,ll,ll ,ll>> &stat, std::set<std::tuple<ll,ll,ll,ll>> &q){
//Prova två for loopar
    std::vector<std::vector<ll>> dist(stat.size(), std::vector<ll>(stat.size(), INT_MAX));
    for(ll y = 0; y < stat.size(); y++){
        for(ll x = 0; x < stat.size(); x++){
            if(x != y && std::get<2>(stat[x]) <= dist[y][x]){
                dist[y][x] += std::get<1>(stat[x]) - std::get<2>(stat[x]);
            }
        }
    }
    for(ll i : path){
        std::cout << i <<  " ";
    }
    std::cout << std::endl;
}

int main(){
    std::cin.sync_with_stdio(false);
    std::cin.tie(static_cast<std::ostream*>(0));
    ll N;
    std::cin >> N;
    std::vector<std::pair<ll,ll>> s(N);
    ll tot = 0;
    std::vector<ll> c(N);
    std::set<std::tuple<ll,ll,ll,ll>> q;
    std::vector<std::tuple<ll,ll,ll,ll>> st(N);
    for (ll i = 0; i < N; i++){
        ll in, out;
        std::cin >> in >> out;
        s[i] = std::make_pair(in, out);
        tot += in;
        c[i] = i;
        q.insert(std::make_tuple(i, in, out, (ll)0));
        st[i] = std::make_tuple(i, in, out, (ll)0);
    }
    std::vector<bool> vis(N, false);
    //BFS(q, s, vis, std::vector<ll>(), 0, 0, 0)
    //DFS(std::vector<ll>(), 0, s, c);//, tot
    BFS(st, q);
    //Alla med 0 avstigningar är möjliga starter

    std::cout << "NEJ" << std::endl;

#ifdef WIN32
    system("PAUSE");
#endif
}
