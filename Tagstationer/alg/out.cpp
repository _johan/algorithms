#include <iostream>
#include <vector>
#include <stdlib.h>

int main(){
	int n = 20000;
	int curbalance = 0;
	std::cout << n << "\n";
	std::cout << "10 0\n";
	for(int i = 1; i < n-1; i++){
		int in = rand() % 1000 + 0;
		int out = rand() % 1000 + 0;
		std::cout << in << " " << out << "\n";
		curbalance += in - out;
	}
	
	int IN_ = 0;
	int OUT_ = 0;
	if(curbalance < 0){
		OUT_= -curbalance;
	}else{
		IN_ = curbalance;
	}
	std::cout << IN_ << " " << OUT_ << "\n";
}