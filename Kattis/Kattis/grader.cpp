#include "kattis.h"
#include <iostream>
#include <stdio.h>
//#include <bits/stdc++.h>

using namespace std;

int main() {
	int N, H;
	int ignore = scanf("%d%d", &N, &H);
	int *X = new int[N], *Y = new int[N], *Z = new int[N];
	for (int i = 0; i < N; i++) {
		ignore = scanf("%d", X + i);
		ignore = scanf("%d", Y + i);
		ignore = scanf("%d", Z + i);
	}
	printf("%d\n", kattis(N,H,X,Y,Z));
#ifdef WIN32
	system("PAUSE");
#endif
}
