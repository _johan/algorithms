#include "kattis.h"
#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <cmath>
#include <set>
#define all(v) v.begin(), v.end()
#define INF 1000000000
using namespace std;
typedef pair<int, int> pii;
int kattis(int N, int H, int X[], int Y[], int Z[]) {
	vector<pii> coords(N);
	vector<pii> maxmin;
	maxmin.reserve(N);
	set<int> guards;
	for (int i = 0; i < N; ++i){
		coords[i] = pii(X[i], Y[i]);
	}

	vector<int> openings;
	vector<pii> endings;
	openings.reserve(N);
	endings.reserve(N);
	for (int i = 0; i < N; ++i){
		if (Z[i] != 1)
			continue;
		double max = 0.0f;
		int index = -1;
		pii t;
		for (int j = 0; j < i; ++j){
			if (coords[i].first == coords[j].first)
				continue;
			double k = (double)(coords[i].second - coords[j].second) / (double)(coords[i].first - coords[j].first);
			if (k < max){
				max = k;
				//double  m = coords[i].second - k * coords[i].first;
				index = ((double)H - (double)coords[i].second + (double)k * (double)coords[i].first) / k;
			}
		}
		if (index != -1)
			t.first = index;//pii(coords[index]);
		else
			t.first = -1;
		max = 0.0f;
		index = -1;
		for (int j = i + 1; j < N; ++j){
			if (coords[i].first == coords[j].first)
				continue;
			double k = (double)(coords[i].second - coords[j].second) / (double)(coords[i].first - coords[j].first);
			if (k > max){
				max = k;
				index = ((double)H - (double)coords[i].second + (double)k * (double)coords[i].first) / k;
			}
		}
		if (index != -1)
			t.second = index;//pii(coords[index]);
		else
			t.second = -1;
		maxmin.push_back(t);
		openings.push_back(t.first);
		endings.push_back(pii(t.second, i));
	}

	set<int> used;
	sort(all(endings));
	int numguards = 0;
	int openingit = 0, endingit = 0;
	while (endingit != openings.size()){ //Why not for loop?
		if (endings[endingit].first != -1 && used.find(endings[endingit].second) == used.end()){
			for (int i = 0; i < N; ++i){
				if (openings[i] <= endings[endingit].first){
					used.insert(i);
				}
			}
			++numguards;
			//cout << "Placed guard at: " << endings[endingit].first << endl;
		}
		++endingit;
	}
	if (used.size() != openings.size() && openings.back() != -1){
		++numguards;
	}
	return numguards;
}
/*
5 12 
0 0 1 
5 0 1 
10 0 1 
12 10 1 
15 10 1 


9 106 
0 0 1 
10 0 1 
20 0 1 
30 0 1 
40 0 1 
50 100 1 
60 0 1 
70 105 1 
80 0 1 
*/