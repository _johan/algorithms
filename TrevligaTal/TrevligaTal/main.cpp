#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <climits>
#include <utility>
#include <iomanip>
#include <fstream>

#define all(v) v.begin(), v.end()
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef pair<int,int> pii;
typedef vector<pii> vpii;
ll N, M = 0, sum = 0;
vi num;
//1023

//12
//102
//1023
//3
//123
//
ll Do(ll pos, ll sum, ll numnums){
    if(pos >= M){
        return 0;
    }
    if(sum == 0 && pos != -1){
        return 1; //Inledande nollor
    }
    ll ret = 0;
    if(sum % 3 == 0 && pos != -1){
       // cout << sum << endl;
        ++ret;
    }
    for(unsigned int i = pos+1; i < M; ++i){
        ret += Do(i, sum + (num[i] * (ll)pow(10, M - numnums)), numnums+1);
    }
    return ret;
}

int main()
{
    //100001
    string in;
    cin >> in;
    M = in.size();
    num = vi(M);
    bool onezero = false;
    for(unsigned int i = 0; i < in.size(); ++i){
        num[i] = in[i] - '0';
        sum += num[i];
        if(num[i] == 0 && !onezero){
            onezero = true;
        }
    }// + onezero
    cout << Do(-1, 0, 0) % 1000000000 << endl;
}

