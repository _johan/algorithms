#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <utility>
using namespace std;

int main(){
	int N, M;
	cin >> N;
	vector<pair<string, string>> over(N);
	for (int i = 0; i < N; ++i){
		cin >> over[i].first >> over[i].second;
	}
	cin >> M;
	vector<string> sent(M);
	for (int i = 0; i < M; ++i){
		cin >> sent[i];
	}
	for (int i = 0; i < M; ++i){
		string cureng;
		string curswe = sent[i];
		bool lookingforEng = true;
		for (int j = N - 1; j >= 0; --j){
			if (lookingforEng){
				if (over[j].first == curswe){
					cureng = over[j].second;
					lookingforEng = false;
				}
			}
			else{
				if (over[j].second == cureng){
					curswe = over[j].first;
					lookingforEng = true;
				}
			}
		}
	sent[i] = curswe;
	}
	for (int i = 0; i < M; ++i){
		cout << sent[i] << " ";
	}
	cout << endl;
#ifdef WIN32
	system("PAUSE");
#endif

}