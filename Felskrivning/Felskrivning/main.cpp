#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <climits>
#include <cmath>

typedef std::pair<int,int> pii;
typedef std::vector<pii> vpii;
typedef std::vector<int> vi;
typedef std::vector<vi> vvi;
typedef std::vector<bool> vb;
typedef std::vector<std::string> vs;
std::string in;
vs alt;
int N;

inline int fac(int N){
    if(N <= 0){
        return 1;
    }
    int sum = 1;
    for(int i = 1; i <= N; i++){
        sum *= i;
    }
    return sum;
}

int main()
{
    std::cin >> in >> N;
    alt = vs(N);
    int mxsize = 0;
    for(int  i = 0; i < N; i++){
        std::cin >> alt[i];
        mxsize = std::max((int)alt[i].size(), mxsize);
    }
    vi numcombs(N);
    vi numsame;
    numsame.reserve(mxsize);
    for(int i = 0; i < N; ++i){
        //Scan string for the number of occurenses of one of the chars
        numsame.resize(alt[i].size());
        for(int j = 0; j < alt[i].size(); j++){
            for(int c = 0; c < in.size(); c++){
                if(in[c] == alt[i][j]){
                    ++numsame[j];
                }
            }
        }
        int tot = 0;
        //for(int j = 0; j < numsame.size(); j++){
            int localtot = 0;
            //tot += fac(numsame[j]) / numsame[j];
            //tot += fac(numsame.size()) / (std::max(numsame[j], 1));
            //tot += fac(numsame[j]) / (fac(numsame.size()-1) * fac(numsame[j] - fac(numsame.size()-1)) * numsame[j]);
            for(int c = 1; c <= numsame[i]; ++c){
                localtot += std::pow(alt[i].size(), c);
                /*
abba
1
ia
*/
            }
            localtot /= std::max(1, numsame[i]);
            tot += localtot;
        //}
        --tot;
        numcombs[i] = tot;
        numsame.clear();
    }
    int tot = 1;
    for(int i = 0; i < numcombs.size(); i++){
        tot *= numcombs[i];
    }
    std::cout << tot << std::endl;
    return 0;
}

