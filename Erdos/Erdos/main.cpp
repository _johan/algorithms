#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <algorithm>
#include <string>
#include <utility>
#include <climits>
struct Comparator{
	bool operator () (const std::pair<std::string, int> &lhs, const std::pair<std::string, int> &rhs){
		if (lhs.second == rhs.second){
			return lhs.first < rhs.first;
		}
		return lhs.second < rhs.second;
	}
};
#define GETI(i) _names[i]
int main(){
	int N, P;
	std::cin >> N >> P;
	std::map<std::string, int> erdosnum;
	std::vector<std::vector<std::string>> name(P); //Publikationer
	std::map<std::string, std::set<std::string>> edges;
	std::set<std::string> names;
	std::vector<std::string> _names;
	_names.reserve(N);
	for (int i = 0; i < P; i++){
		int F;
		std::cin >> F;
		std::string p;
		for (int c = 0; c < F; c++){
			std::cin >> p;
			name[i].push_back(p);
			names.insert(p);
			erdosnum[p] = INT_MAX;
		}
		for (int c = 0; c < name[i].size(); c++){
			for (int j = 0; j < name[i].size(); j++){
				if (c != j){
					edges[name[i][j]].insert(name[i][c]);
				}
			}
		}
	}
	for (std::string s : names){
		_names.push_back(s);
	}
	//std::cout << "Done reading..." << std::endl;
	erdosnum["ERDOS"] = 0;
	/*
	Beware segfault due to unitiialized maps and stuff
	*/
	/*
	--Kan-- M�STE g�ra djikstras
	*/
	std::set<std::pair<std::string, int>, Comparator> q;
	std::set<std::string> visited;
	q.insert(std::make_pair("ERDOS", 0));
	visited.insert("ERDOS");
	while (!q.empty()){
		std::pair<std::string, int> first = *q.begin();
		q.erase(q.begin());
		visited.insert(first.first);
		for (std::string e : edges[first.first]){
			if (visited.find(e) == visited.end()){
				if (erdosnum[e] > first.second + 1){
					q.erase(std::make_pair(e, erdosnum[e]));
					erdosnum[e] = first.second + 1;
					q.insert(std::make_pair(e, erdosnum[e]));
				}
			}
		}
	}
	for (std::pair<std::string, int> p : erdosnum){
		std::cout << p.first << " " << p.second << std::endl;
	}
#ifdef WIN32
	system("PAUSE");
#endif
}