#include <iostream>
#include <vector>
#include <set>
#include <climits>
typedef long long ll;
const char in = 'i';
const char out = 'u';
const char locked = 'l';
const char unlocked = 'o';
struct Node;
struct Edge{
    Edge(){}
    Edge(Node* _lhs, Node* _rhs, int _dist, char _locked){
        lhs = _lhs;
        rhs = _rhs;
        dist = _dist;
        locked = _locked;
    }

    Node* lhs;
    Node* rhs;
    int dist;
    char locked;
};

struct Node{
    std::vector<Edge> e;
    char type;
    int val = INT_MAX;
    int index;
    friend bool operator <(const Node &lhs, const Node& rhs){
        if(lhs.val == rhs.val){
            return lhs.index < rhs.index;
        }
        return lhs.val < rhs.val;
    }
};

int main(){
    int N, S, K, M;
    std::cin >> N >> S >> K;
    if(N == 1){
        std::cout << 0 << std::endl;
    }
    std::vector<Node> n(N);
    for(int i = 0; i < N; i++){
        std::cin >> n[i].type;
        n[i].index = i;
        if(i == S){
            n[i].val = 0;
        }
    }
    std::cin >> M;
    for(int i = 0; i < M; i++){
        int lhs, rhs, dist;
        char t;
        std::cin >> lhs >> rhs >> dist >> t;
        n[lhs].e.push_back(Edge(&n[lhs], &n[rhs], dist, t));
        n[rhs].e.push_back(Edge(&n[rhs], &n[lhs], dist, t));
    }
    //Do a djikstras to the closest outside node first

    std::set<Node> q;
    for(int i = 0; i < N; i++){
        q.insert(n[i]);
    }
    int disttokey = 0, shortestout = INT_MAX;
    std::set<Node>::iterator it;
    while(!q.empty()){
        Node cur = *q.begin();
        if(cur.val == INT_MAX){
            //We can reach no more nodes, might aswell break
            break;
        }
        if(cur.index == K){
            //Key found
           // std::cout << "Distance to key: " << cur.val << std::endl;
            disttokey = cur.val;
        }
        if(cur.type == out){
            //We're outside
           // std::cout << "Found an outside node: " << cur.val << std::endl;
            if(cur.val < shortestout){
                shortestout = cur.val;
            }
        }
        for(Edge e : cur.e){
            it = q.find(*e.rhs);
            if(it != q.end()){
                Node _e = *it;
                if(_e.val > cur.val + e.dist && e.locked != locked){
                    _e.val = cur.val + e.dist;
                }
                q.erase(it);
                q.insert(_e);
                it = q.end();
            }
        }
        q.erase(cur);
    }

    for(int i = 0; i < N; i++){
        n[i].val = INT_MAX;
        if( i == K){
            n[i].val = disttokey;
        }
        q.insert(n[i]);
    }
    while(!q.empty()){
        Node cur = *q.begin();
        if(cur.type == out){
            //We're outside
           // std::cout << "Found an outside node: " << cur.val << std::endl;
            //std::cout << "Breaking.." << std::endl;
            if(cur.val < shortestout){
                shortestout = cur.val ;
            }
            break;
        }
        for(Edge e : cur.e){
            it = q.find(*e.rhs);
            if(it != q.end()){
                Node _e = *it;
                if(_e.val > cur.val + e.dist){
                    _e.val = cur.val + e.dist;
                }
                q.erase(it);
                q.insert(_e);
                it = q.end();
            }
        }
        q.erase(cur);
    }



    std::cout << shortestout << std::endl;
    //do a djikstras to the key, and then to the closest outside node. I can save the distance to the key in the previous djikstras algorithm

}
