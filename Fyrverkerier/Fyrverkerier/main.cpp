#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <climits>

typedef std::pair<int,int> pii;
typedef std::vector<pii> vpii;
typedef std::vector<int> vi;
typedef std::vector<vi> vvi;
typedef std::vector<bool> vb;



int N, M;
vpii rockets;
vvi dp;

int Do(int pos, vi &used, int stor, int price, int numused){
    if(pos >= N){
        return price;
    }
    if(dp[pos][stor] != -1){
        return dp[pos][stor];
    }
    int pc = INT_MAX;
    if(stor >= M && numused > 2 /*Else there will be one over 50%*/){
        pc = price;
    }else{
        for(int i = 0; i < N; i++){
            if(used[i] == 0 || (float)(numused) / (float)used[i] < 0.5){
                ++used[i];
                pc = std::min(pc, Do(i, used, stor + rockets[i].first, price + rockets[i].second, numused + 1));
                --used[i];
            }
        }
    }
    return dp[pos][stor] = pc;
}

int main()
{
    std::cin >> N >> M;
    rockets = vpii(N);
    int sum = 0;
    for(int i = 0; i < N; i++){
        std::cin >> rockets[i].first >> rockets[i].second;
        sum += rockets[i].first;
    }
    dp = vvi(N, vi(sum+1, -1));
    vi us(N, 0);
    std::cout << Do(0, us, 0, 0, 0) << std::endl;
}

