#include <iostream>
#include <vector>

int main(){
	int ramsalen, N;
	std::cin >> ramsalen >> N;
	std::vector<int> s;
	s.reserve(N);
	for (int i = 0; i < N; i++){
		s.push_back(i);
	}
	int lastit = 0;
	while (s.size() != 1){
		int it = ramsalen % s.size() + lastit - 1;
		if (it < 0){
			it = s.size() - 1;
		}
		if (it >= s.size()){
			it -= s.size();
		}
		//std::cout << "Out: " << s[it] << std::endl;
		s.erase(s.begin() + it);
		lastit = it;
		//lastit--;
		//if (lastit < 0){
		//	lastit = s.size() - 1;
		//}
	}
	std::cout << s.front()+1 << std::endl;
	system("PAUSE");
}