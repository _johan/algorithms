#include <iostream>
#include <sstream>
int main(){
    long N;
    std::cin >> N;
    std::string _N = static_cast<std::ostringstream*>( &(std::ostringstream() << N) )->str();
    long Nsum = 0;
    for(char c : _N){
        Nsum += c - '0';
    }
    //std::cout << Nsum << std::endl;
    //Betyder att talet är mellan
    //"Serierna" är 0-99, 100 - 999, 1000-9999, 10000-99999
    long correct = 0;
    for(int i = N+1; true; i++){
       std::string num = static_cast<std::ostringstream*>( &(std::ostringstream() << i) )->str();
       long sum = 0;
       for(char c : num){
           sum += c - '0';
       }
       if(sum == Nsum){
           correct = i;
           break;
       }
    }
    std::cout << correct << std::endl;
}
//Vi vill börja på så små tal som möjligt
