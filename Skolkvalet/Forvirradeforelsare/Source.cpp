#include <iostream>
#include <vector>

int main(){
    int N;
    std::cout << "Ant veckor?\n" << std::endl;
    std::cin >> N;
    std::vector<int> num(N);
    for(int i = 0; i < N; i++){
        std::cout << "Vecka " << i << "?\n" << std::endl;
        std::cin >> num[i];
    }
    int empty = 0, missed = 0, last = 0;
    last = num[0];
    for(int i : num){
        if(i != last){
            //Vals can change
            if(last > i){
                //Håller för många
                empty += last - i;
                last = i;
            }else{
                //Håller för få
                missed += i - last;
                last = i;
            }
        }
    }
    std::cout << "Tomma: " << empty << "\nMissade:" << missed << std::endl;
}
//4 1 3 2 4
//5 4 1 5 3 10
//2 10 10
