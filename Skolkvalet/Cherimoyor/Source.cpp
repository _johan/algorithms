#include <iostream>
#include <vector>
#include <algorithm>
inline int calcSum(const int c){
    int sum = 0, max = std::min(10, c);
    for(int i = 0; i < max; i++){
        sum += 10 - i;
    }
    return sum;
}
/*
int Do(std::vector<int> &c, int cur, int root2, int root1, int max, int day){
    if(day >= c.size()){
        return max;
    }
    int _max = std::min(10, cur), add = 0;
    for(int i = 0; i < _max; i++){
        int nroot1 = root1, nroot2 = root2, ncur = c[day];
        nroot1 = std::max(root1-i, 0);
        if(nroot1 == -1)
            nroot2 = std::max(root1 - i + root1, 0);
        else
            nroot2 = 0;
        if(nroot2 == -1){
            ncur = (c[day] - i + root1 + root2);
        }else
            ncur = c[day];
        //add = std::max(add, Do(c, cur + ncur - nroot1 - i /*All nroot1 are going to dissapear*///, ncur, nroot2, max + calcSum(i), day+1));
  /*  }
    return add;
}
*/
//root3 försvinner om 3 dagar, root2 om 2 dagar och root1 idag
int Do(const std::vector<int> &c, int cur, int root3, int root2, int root1, int max, int day){
    if(day >= c.size()){
        return max;
    }
    int _max= std::min(10, cur), add = 0;
    for(int i = 0; i <= _max; i++){
      //  if(day == 1){
      //      std::cout << "Hey" << std::endl;
      //  }
        int nroot3 = root3, nroot2 = root2, nroot1 = root1;
        nroot1 = (root1 - i);
        if(nroot1 < 0){
            nroot2 = root2 + nroot1;
            nroot1 = 0;
            if(nroot2 < 0){
                nroot3 = root3 + nroot2;
                nroot2 = 0;
            }
        }
        //std::cout << "Eating " << i << " fruits on day " << day << std::endl;
        add = std::max(Do(c, cur - nroot1 + c[day] - i, c[day], nroot3, nroot2, max + calcSum(i), day+1), add);
    }
    return add;
}

/*
int Do(const std::vector<int> &c, int N1, int N2, int N3, int max, int day){
    if(N1 == 0 && N2 == 0 && N3 == 0 && day != 0){
        return max;
    }

    int add = 0;
    //Always eat all N3's
    add += calcSum(N3);
    int _max = std::min(10, N2);
    for(int i = 0; i < _max ; i++){
        //N2--;
        add = std::max(add, Do(c, c[day], N1, N2-i, add+calcSum(i), day+1));
    }
    _max  = std::min(10, N1);
    for(int i = 0; i < _max ; i++){
        add = std::max(add, Do(c, c[day], N1-i, N2, add+calcSum(i), day+1));
    }
    _max  = std::min(10, c[day]);
    for(int i = 0; i < _max ; i++){
        add = std::max(add, Do(c, c[day]-i, N1, N2, add+calcSum(i), day+1));

    }
    add = std::max(add, Do(c, c[day], N1, N2, add, day+1));
    return add;
}
*/
int main(){
    int N;
    std::cin >> N;
    std::vector<int> c(N+3);
    for(int i = 0; i < N; i++){
        std::cin >> c[i];
    }
    std::cout << Do(c, c[0], c[0], 0, 0, 0, 1) << std::endl;

}
/*
3 18 0 2
*/
/*
8 3 0 1 2 0 0 3 6
*/
/*
2 30 30
*/
