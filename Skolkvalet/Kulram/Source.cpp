#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
typedef long long ll;
int main(){
    ll R, N;
    std::cin >> R;
    std::vector<std::pair<ll, ll>> v(R);
    std::vector<std::pair<ll,ll>> _v(R);
    for(ll i = 0; i < R; i++){
        ll lhs,rhs;
        std::cin >> lhs >> rhs;
        v[i] = std::make_pair(lhs, rhs);
        _v[i] = std::make_pair(0, lhs+rhs);
    }
    //_v = v;
    std::cin >> N;
    std::reverse(v.begin(), v.end());
    bool istrue = false;
    ll forzeroagain = 0;
    for(ll i = 0; !istrue; i++){
        for(ll o = 0; o < R; o++){
            if(_v[o].second > 0){
                _v[o].first++;
                _v[o].second--;
                for(ll c = o-1; c >= 0; c--){
                    _v[c].second = v[c].first;
                    _v[c].first = 0;
                }
                break;
            }else if(o == R-1){
                    //Move everything to the right
                for(ll x = 0; x < R; x++){
                    _v[x].second = v[x].first;
                    _v[x].first = 0;
                    forzeroagain = i;
                    istrue = true;
                    break;
                }
                //Kan man göra en som går från alla till höger och till max igen, och på så sätt minska det där N:et?
            }
        }

    }
    for(ll i = 0; i < N; i++){
        for(ll o = 0; o < R; o++){
            if(v[o].second > 0){
                v[o].first++;
                v[o].second--;
                for(ll c = o-1; c >= 0; c--){
                    v[c].second = v[c].first;
                    v[c].first = 0;
                }
                break;
            }else if(o == R-1){
                    //Move everything to the right
                for(ll x = 0; x < R; x++){
                    v[x].second = v[x].first;
                    v[x].first = 0;
                }
                //Kan man göra en som går från alla till höger och till max igen, och på så sätt minska det där N:et?
                //ll mult = (N - i) / forzeroagain;
                //i += mult * forzeroagain;
                break;
            }
        }
    }
    for(ll i = R-1; i >= 0; i--){
        std::cout << v[i].first << " " << v[i].second << std::endl;
    }
}
/*
4
0 4 0 2 1 2 0 1 6
*/
/*
4 2 2 2 0 2 1 1 0 85
*/
/*
4 1 1 0 2 2 0 1 1 37
*/
/*
10 4 5 7 2 8 0 6 3 3 5 4 4 1 8 0 9 7 1 2 6 9876543210
*/
