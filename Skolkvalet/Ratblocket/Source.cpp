#include <iostream>
#include <vector>
#include <utility>
/* if size is 1 at any place it's bigger*/
int alternate(int x){
    if(x == 0)
        return x;
    if(x == 1)
        return x;
    if(x == -1)
        return 2;
}

inline std::pair<int,int> getNewSize(int xSize, int ySize, int &xmove, int &ymove){
    if(xSize != 0 && ySize == 0){
        if(xmove != 0){
            //Moves in x
            if(xSize == -1){
                if(xmove == -1)
                     xmove--;
            }else{
                if(xmove == 1)
                  xmove++;
            }
            return std::make_pair(0,0);
        }else{
            //Moves in y
            return std::make_pair(ySize, xSize);
        }
    }else if(ySize != 0 && xSize == 0){
        if(ymove != 0){
            if(ySize == -1){
                if(ymove == -1)
                  ymove--;
            }else{
                if(ymove == 1)
                   ymove++;
            }
            return std::make_pair(0,0);
        }else{
            return std::make_pair(ySize, xSize);
        }
    }else{ //Båda == 0
        if(ymove != 0){
            if(ymove == -1){
                return std::make_pair(-1,0);
            }else{
                return std::make_pair(1,0);
            }
        }else{
            if(xmove == -1){
                return std::make_pair(0,-1);
            }else{
                return std::make_pair(0,1);
            }
        }
    }
}
#define INF 2000000
int calls = 0;
//vec[ypos][xpos][ysize][xsize]
int Do(std::vector<std::vector<char>> &gb, std::vector<std::vector<std::vector<std::vector<bool>>>> vis, std::pair<int,int> pos, int ysize, int xsize, const std::pair<int,int> &end, int dist, std::vector<std::pair<int,int>> path){
   // std::cout << "Called." << std::endl;
    calls++;
    if(pos.first+ysize >= gb.size()){
        return INF;
    }
    if(pos.second+xsize >= gb[pos.first].size()){
        return INF;
    }
    //if(gb[pos.first][pos.second] == '#' || gb[pos.first - ysize][pos.second+xsize] == '#'){
    //    return INF;
    //}
    if(end.first == pos.first && end.second == pos.second && ysize == 0 && xsize == 0){
        return dist;
    }
    std::cout << "Pos: " << pos.first << ", " << pos.second << ": size: " << ysize << ", " << xsize << std::endl;
    //Move right

    int add = INF;
    int xmove = 1, ymove = 0;
    vis[pos.first][pos.second][alternate(ysize)][alternate(xsize)] = true;
    std::pair<int,int> size = getNewSize(xsize, ysize, xmove, ymove);
    std::pair<int,int> newpos = std::make_pair(pos.first + ymove, pos.second + xmove);

    if(newpos.first >= 0 && newpos.first + size.first >= 0 && newpos.second >= 0 && newpos.second + size.second >= 0 && newpos.first < gb.size() && newpos.first + size.first < gb.size() && newpos.second < gb[newpos.first].size() && newpos.second + size.second < gb[newpos.first].size()){
        if(!vis[newpos.first][newpos.second][alternate(size.first)][alternate(size.second)] && gb[newpos.first][newpos.second] != '#' && gb[newpos.first + size.first][newpos.second + size.second] != '#'){
            path.push_back(size);
            add = std::min(add, Do(gb, vis, newpos, size.first, size.second, end, dist+1, path));
            path.pop_back();
        }
    }
    xmove = -1, ymove = 0;
    size = getNewSize(xsize, ysize, xmove, ymove);
    newpos = std::make_pair(pos.first + ymove, pos.second + xmove);
    if(newpos.first >= 0 && newpos.first + size.first >= 0 && newpos.second >= 0 && newpos.second + size.second >= 0 && newpos.first < gb.size() && newpos.first + size.first < gb.size() && newpos.second < gb.size() && newpos.second + size.second < gb[newpos.first].size()){
        if(!vis[newpos.first][newpos.second][alternate(size.first)][alternate(size.second)] && gb[newpos.first][newpos.second] != '#' && gb[newpos.first + size.first][newpos.second + size.second] != '#'){
            path.push_back(size);
            add = std::min(add, Do(gb, vis, newpos, size.first, size.second, end, dist+1, path));
            path.pop_back();
        }
    }

    xmove = 0, ymove = 1;
    size = getNewSize(xsize, ysize, xmove, ymove);
    newpos = std::make_pair(pos.first + ymove, pos.second + xmove);

    if(newpos.first >= 0 && newpos.first + size.first >= 0 && newpos.second >= 0 && newpos.second + size.second >= 0 && newpos.first < gb.size() && newpos.first + size.first < gb.size() && newpos.second < gb.size() && newpos.second + size.second < gb[newpos.first].size()){
        if(!vis[newpos.first][newpos.second][alternate(size.first)][alternate(size.second)] && gb[newpos.first][newpos.second] != '#' && gb[newpos.first + size.first][newpos.second + size.second] != '#'){
            path.push_back(size);
            add = std::min(add, Do(gb, vis, newpos, size.first, size.second, end, dist+1, path));
            path.pop_back();
        }
    }

    xmove = 0, ymove = -1;
    size = getNewSize(xsize, ysize, xmove, ymove);
    newpos = std::make_pair(pos.first + ymove, pos.second + xmove);

    if(newpos.first >= 0 && newpos.first + size.first >= 0 && newpos.second >= 0 && newpos.second + size.second >= 0 && newpos.first < gb.size() && newpos.first + size.first < gb.size() && newpos.second < gb.size() && newpos.second + size.second < gb[newpos.first].size()){
        if(!vis[newpos.first][newpos.second][alternate(size.first)][alternate(size.second)] && gb[newpos.first][newpos.second] != '#' && gb[newpos.first + size.first][newpos.second + size.second] != '#'){
            path.push_back(size);
            add = std::min(add, Do(gb, vis, newpos, size.first, size.second, end, dist+1, path));
            path.pop_back();
        }
    }
    return add;

}

int main(){
    int H, W;
    std::cin >> H >> W;
    std::vector<std::vector<char>> gb(H, std::vector<char>(W, '.'));
    std::pair<int,int> start, end;
    for(int i = 0; i < H; i++){
        std::string s;
        std::cin >> s;
        for(int x = 0; x < W; x++){
            if(s[x] == '#'){
                gb[i][x] = '#';
            }else if(s[x] == 'A'){
                start = std::make_pair(i,x);
            }else if(s[x] == 'B'){
                end = std::make_pair(i,x);
            }
        }
    }

    std::cout << Do(gb, std::vector<std::vector<std::vector<std::vector<bool>>>>(H, std::vector<std::vector<std::vector<bool>>>(W, std::vector<std::vector<bool>>(3, std::vector<bool>(3, false)))), start, 0,0, end, 0, std::vector<std::pair<int,int>>()) << std::endl;
}
/*
3 3
A..
...
..B
*/
/*
3 5
A#...
.....
...#B
*/
