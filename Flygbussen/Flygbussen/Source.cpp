#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <utility>
#include <iterator>
#include <algorithm>
#define INF 2000000000

typedef long long ll;
typedef std::pair<ll,ll> pll;
typedef std::set<std::pair<ll,ll>, std::less<std::pair<ll,ll>>> pqll;

ll Do(std::map<ll,ll> avg, ll waiting, ll curtime, ll mintime, bool busavailable, ll timetilbus, const ll defttbuss){
    if(avg.size() == 0 && waiting == 0){
       // std::cout << "Returning: " << mintime << ", from uppper" << std::endl;
        return mintime;
    }
   // std::cout << "timetilbus: " << timetilbus << ", mintime: " << mintime <<  std::endl;
    ll add = INF;
    //for(ll i = 0; i < avg.size(); i++){
        if(busavailable){
            if(avg.size() == 0){
                return std::min(add, Do(avg, 0, avg.begin()->first, mintime, false, defttbuss*2, defttbuss));

            }
            //There are two paths we can take, either we wait for the next people or we depart with this group and all the waiting ones
            std::pair<ll,ll> _this = *avg.begin();
            avg.erase(avg.begin());
            //pll begin;
            //if(avg.size() == 0){
            //    begin.first = _this.first;
            //}else{
            //    begin = *avg.begin();
            //}
            //ll timediff = begin.first - _this.first;
            ll timediff = _this.first - curtime;
            add = std::min(add, Do(avg, waiting+_this.second, _this.first, mintime + (waiting) * timediff, true, 0, defttbuss));
            //mintime += waiting * timediff;
            add = std::min(add, Do(avg, 0, _this.first, mintime + waiting * timediff/*The bus only comes after tthis has been reached*/, false, defttbuss*2, defttbuss));
           // avg.insert(avg.begin(), _this);
            avg[_this.first] = _this.second;
        }else{
            //Call the function again but with a higher mintime(?)
            pll begin = *avg.begin();
            if(avg.size() == 0){
                begin.first = timetilbus;
            }
            if(begin.first - curtime < timetilbus && avg.size() != 0){
                //Go to avg.front() time
// + _this.second
                ll timediff = (begin.first - curtime);
                avg.erase(avg.begin());
                add = Do(avg, waiting+begin.second, begin.first/*curtime+timediff*/, mintime + (waiting) * timediff, false/*Bus still not available*/, timetilbus - timediff, defttbuss);
            }else{
                //The closest time is the arrival of the bus
                ll timediff = timetilbus;
                add = Do(avg, waiting, curtime+timediff, mintime + waiting * timediff, true /*Bus is now available*/, 0, defttbuss);
            }
        }
    //}
       // std::cout << "Returning: " << add << ", from upper" << std::endl;
    return add;
}

int main(){
    ll N, time;
    std::cin >> N >> time;
    //std::map<ll> avg(N);
    std::map<ll,ll> avg;
    for(ll i = 0; i < N; i++){
        ll t;
        std::cin >> t;
        if(avg.find(t) != avg.end()){
            //This key exists
            avg[t]++;
        }else{
            avg[t] = 1;
        }
       // std::cin >> avg[i];
    }
    //std::sort(avg.begin(), avg.end());
    std::cout << Do(avg, 0, 0, 0, true, 0, time) << std::endl;
}
