#include <iostream>
#include <vector>
#include <utility>


std::vector<std::pair<int, int>> stations;
std::vector<int> m_path;

void comb(std::vector<bool> used, std::vector<int> path, int pos, int num_used, int people_on_train){
	if (num_used == stations.size()){
		for (int x : path){
			std::cout << x << " ";
		}
		std::cout << std::endl;
		exit;

	}
	if (pos >= stations.size() - 1){
		return;
	}
	//used[pos] = true;
	for (int i = 0; i < used.size(); i++){
		if ( stations[i].second <= people_on_train ){
			std::vector<int> new_path(path);
			new_path.push_back(i);

			std::vector<bool> new_used(used);
			new_used[i] = true;
			comb(new_used, new_path, pos + 1, num_used + 1, people_on_train + stations[i].first - stations[i].second);
		}
	}
}

int main()
{
	int num_stations;
	std::cin >> num_stations;
	stations.resize(num_stations);
	for (int i = 0; i < num_stations; i++){
		int entries, exits;
		std::cin >> entries >> exits;
		stations[i] = std::make_pair(entries, exits);
	}
	comb(std::vector<bool>(num_stations, false), std::vector<int>(0), 0, 0, 0);
	return 0;
}

