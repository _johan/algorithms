#include <iostream>
#include <vector>
#include <algorithm>
#include <utility>
#include <queue>
int numcombos = 0;


std::vector<std::vector<int>> dp;
int Generate(std::vector<int> &parties, int pos, int left, int currentmandates, const int req){
    if(pos < 0){
        return 0;
    }
    if(dp[pos][currentmandates] != -1){
        return dp[pos][currentmandates];
    }
    int ret;
    if(left + currentmandates < req){
        ret = 0;
    }else{
        ret = Generate(parties, pos - 1, left - parties[pos], currentmandates, req);
        //currentmandates += parties[pos];
        if(currentmandates + parties[pos] >= req){
            ++ret;
        }else{
            ret += Generate(parties, pos - 1, left - parties[pos], currentmandates + parties[pos], req);
        }
        //currentmandates -= parties[pos];
    }
    return dp[pos][currentmandates] = ret;
}


int main()
{
    int N, sum = 0;
    std::cin >> N;
    std::vector<int> parties(N);
    for(int i = 0; i < N; i++){
        std::cin >> parties[i];
        sum += parties[i];
    }
    dp = std::vector<std::vector<int>>(N, std::vector<int>(sum, -1));

    int req = sum / 2 + 1;
    std::sort(parties.begin(), parties.end());
    std::cout << Generate(parties, parties.size()-1, sum, 0, req) << std::endl;

}

