#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
typedef unsigned int uint;

inline int checkPoints(std::string &cur, const std::string &org, uint &bufferpos, uint pts){
    char last = ' ';
    uint numsame = 0, totnumsame = 0;
    bool redo = false;
    bool found = true;
    //std::vector<bool> vis(cur.size(), false);
    while(found){
        found = false;

        for(uint i = 0; i < cur.size(); i++){
            if(last == cur[i]){
                numsame++;
            }else if(last != cur[i] && numsame >= 3){
                //Add points. Change current and recall the function
                //numsame = 0;
                last = cur[i];

                //cur.erase(cur.begin() + i - numsame, cur.begin()+i);
                for(uint c = i - numsame; c < i; c++){
                    cur[c] = '1';
                }
                pts += 2 * numsame - 5;
                redo = true;
                totnumsame += numsame;
                numsame = 1;
                //i = 0;
            }else{
                last = cur[i];
                numsame = 1;
            }
        }
        if(numsame >= 3){
            cur.erase(cur.end() - numsame, cur.end());
            totnumsame += numsame;
            pts += 2 * numsame - 5;
            redo = true;
        }
        for(int i = cur.size(); i >= 0; i--){
            if(cur[i] == (char)'1'){
                cur.erase(cur.begin()+i);
            }
        }
        if(redo){
            //cur.append(buffer.begin(), buffer.begin()+totnumsame);
            //history.push_back((std::string)(cur + " -PTS"));
            //buffer.erase(buffer.begin(), buffer.begin()+totnumsame);
            uint bend = bufferpos + totnumsame;
            if(bend >= org.size()){
                cur.append(org.begin()+bufferpos, org.end());
                bufferpos = bend - org.size();
                cur.append(org.begin(), org.begin()+bufferpos);
            }else{
                cur.append(org.begin()+bufferpos, org.begin() + bend);
                bufferpos = bend;
            }
            found = true;
            redo = false;
            last = ' ';
            numsame = 0, totnumsame = 0;
        }
    }
	return pts;
}
uint maxpts = 0;
int Do(std::string cur, const std::string &org, uint bufferpos, uint pts, uint n, const uint &N){
    int ret = checkPoints(cur, org, bufferpos, 0);
	if (N <= n){
		return ret;
	}
	if (ret == 0 && n != 0){
		return ret; //0
	}
	int best = 0;
    for(uint pos = 0; pos < (cur.size()-1); pos++){
        std::swap(cur[pos+1], cur[pos]);
        best =  std::max(best, Do(cur, org, bufferpos, pts, n+1, N));
        std::swap(cur[pos+1], cur[pos]);
    }
	ret += best;
	return ret;
}
int n;
int r;
std::string in;
std::vector<char> org;

int main()
{
    uint N;
    std::cin >> N;
    std::string gb;
    std::cin >> gb;
/*	std::vector<char> _gb(gb.size());
	for (uint i = 0; i < gb.size(); i++){
		_gb[i] = gb[i];
	}
	org = _gb;
	in = gb;
	n = 0;
	r = 0;
	std::cout << "Carls:" << (_gb, 0, 0) << std::endl;
	*/
	std::cout << Do(gb, gb, 0, 0, 0, N) << std::endl;
#ifdef WIN32
	system("PAUSE");
#endif
    return 0;
}



/*
#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <sstream>
#include <vector>
#include <algorithm>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <map>
#include <tuple>
#include <bitset>
#include <queue>
#include <functional>
#include <array>
#include <list>
#include <forward_list>

using namespace std;

typedef set<int> si;
typedef pair<int, int> pii;
typedef pair<long long, long long> pll;
typedef long long ll;
#define fi first
#define se second

#define INF 2000000000

typedef vector<int> vi;
typedef vector<char> vc;
typedef vector<pair<int, int>> vpii;
#define fillvec(v, x) fill(v.begin(), v.end(), x)
#define printvec(v) for(int i = 0; i < v.size(); ++i){ cout << v[i] << " "; }
#define printvecn(v) for(int i = 0; i < v.size(); ++i){ cout << v[i] << "\n"; }
#define sortvec(v) sort(v.begin(),v.end());

#define rangein(x, a, b) for(int x = a; x <= b; ++x)
#define range(x, a, b) for(int x = a; x < b; ++x)
#define rerangein(x, a, b) for(int x = b; x >= a; --x)
#define rerange(x, a, b) for(int x = b - 1; x >= a; --x)

template<typename T>
T FromString(const std::string& s)
{
    T ret;
    std::istringstream i(s);
    i >> ret;
    return ret;
}

template<typename T>
std::string ToString(T t)
{
    std::stringstream s;
    s << t;
    return s.str();
}


int _Do(vc game, int drag, int orgPos)
{
    int ret = 0;


    //hitta sekvenser
    bool krossHappened = true;
    while (krossHappened)
    {
        krossHappened = false;

        char last = -1;
        char lastIndex = 0;
        range(i, 0, r)
        {
            if (game[i] != last || (i == r - 1 && game[i] == game[lastIndex] && i - lastIndex >= 2))
            {
                if (i == r - 1 && game[i] == game[lastIndex] && i - lastIndex >= 2)
                    i++;

                if (i - lastIndex >= 3)
                {
                    krossHappened = true;
                    int length = (i - lastIndex);
                    ret += 2 * length - 5;
                    int orgCount = 0;
                    for (int j = i - 1; j >= 0; --j) //flyttar efter spelplanen in i hålet
                    {
                        if (j - length < 0)
                        {
                            game[j] = org[orgPos];
                            orgPos--;
                            if (orgPos < 0)
                                orgPos += org.size();
                        }
                        else
                            game[j] = game[j - length];
                    }

                    last = 0;
                    lastIndex = i;
                    --i;
                    continue;
                }
                last = game[i];
                lastIndex = i;
            }
        }
    }

    if (ret == 0 && drag != 0)
        return 0;

    if (drag == n)
    {
        return ret;
    }

    //gör nya drag
    int best = 0;
    range(i, 0, r - 1)
    {
        swap(game[i], game[i + 1]);
        best = max(best, _Do(game, drag + 1, orgPos));
        swap(game[i], game[i + 1]);
    }
    ret += best;
    return ret;
}
/*
int main()
{
    cin >> n;
    cin >> in;
    r = in.size();

    range(i, 0, in.size())
        org.push_back(in[i]);

    reverse(org.begin(), org.end());

    cout << Do(org, 0, org.size() - 1);

    //system("pause");
    return 0;
}

*/
