#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <climits>

typedef std::pair<int,int> pii;
typedef std::vector<pii> vpii;
typedef std::vector<int> vi;
typedef std::vector<vi> vvi;
typedef std::vector<bool> vb;

int N, K;
vi times;
vvi dp;


int Do(int pos, int curtime, int busarrivalin, int numwaiting, int tottimewaited, int busavail){
    /*if(pos >= N){
        return tottimewaited;
    }
    if(busarrivalin > times[pos]){
        busavail = 0;
    }else{
        busavail = 1;
    }
    if(dp[pos][busavail] != -1){
        return dp[pos][busavail];
    }
    int lowest = INT_MAX;
    if(busarrivalin > times[pos]){
        //Move forth
        busavail = 0;
        lowest = Do(pos+1, times[pos], busarrivalin, numwaiting+1, tottimewaited + (times[pos] - curtime) * (numwaiting+1), 0);
    }else{
        busavail = 1;
        //Bus can leave times[pos]
        lowest = std::min(lowest, Do(pos+1, times[pos], times[pos] + 2*K, 0, tottimewaited + (times[pos]-curtime) * (numwaiting), 0));
        lowest = std::min(lowest, Do(pos+1, times[pos], -1, numwaiting+1, tottimewaited + (times[pos]-curtime) * (numwaiting), 1));
    }*/

    if(pos >= N){
        return tottimewaited;
    }
    if(dp[pos][numwaiting] != -1){
        return dp[pos][numwaiting];
    }
    int lowest = INT_MAX;
    if(busarrivalin > times[pos]){
        lowest = Do(pos+1, times[pos], busarrivalin, numwaiting+1, tottimewaited + ((busarrivalin - times[pos]) * (numwaiting+1)), 1);
    }else{
        lowest = std::min(lowest, Do(pos+1, times[pos], times[pos] + 2*K, 0, tottimewaited + (times[pos]-curtime) * (numwaiting), 1));
        lowest = std::min(lowest, Do(pos+1, times[pos], -1, numwaiting+1, tottimewaited + (times[pos]-curtime) * (numwaiting), 1));
    }
    return dp[pos][numwaiting] = lowest;

    /*
    int lowest = INT_MAX;
    if(busarrivalin > times[pos]){
        lowest = Do(pos+1, times[pos], busarrivalin, numwaiting+1, tottimewaited + (busarrivalin - times[pos]) * (numwaiting+1), 0);
    }else{
        lowest = std::min(lowest, Do(pos+1, times[pos], times[pos] + 2*K, 0, tottimewaited + (times[pos]-curtime) * (numwaiting), 0));
        lowest = std::min(lowest, Do(pos+1, times[pos], -1, numwaiting+1, tottimewaited + (times[pos]-curtime) * (numwaiting), 1));

    }*/
    return dp[pos][busavail] = lowest;

}


int main()
{
    std::cin >> N >> K;
    times = vi(N);
    dp = vvi(N, vi(N+1, -1));
    for(int i = 0; i < N; i++){
        std::cin >> times[i];
    }
    std::sort(times.begin(), times.end());
    std::cout << Do(0, times.front(), -1, 0, 0, 1) << std::endl;
    return 0;
}
/*
4 1
1
3
5
7
*/

