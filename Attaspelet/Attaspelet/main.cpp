#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <climits>
#include <queue>
struct Vec2{
	int x;
	int y;
	Vec2(int _x, int _y) : x(_x), y(_y){}
	Vec2(){}
	bool Equals(const Vec2 &rhs){
		if (rhs.x == x && rhs.y == y)
			return true;
		return false;
	}
};

struct St{
	std::vector<std::vector<int>> gb;
	Vec2 cleanPos;
	Vec2 cameFrom;
	int numDraws = 0;
	St(std::vector<std::vector<int>> _gb, Vec2 _cp, Vec2 _cf, int nd) : gb(_gb), cleanPos(_cp), cameFrom(_cf), numDraws(nd){}
	St(){}
};
std::set<std::vector<std::vector<int>>> vis;

int Do(std::vector<std::vector<int>> &gb, Vec2 cleanPos, int numDraws){
	if (numDraws > 31){
		return 32;
	}
	//vis.insert(gb);
	//Controll it
	bool done = true;
	std::queue<St> q;
	q.push(St(gb, cleanPos, Vec2(0, 0), 0));
	vis.insert(gb);
	while (q.size() > 0){
		St cur = q.front();
		if (cur.numDraws > 9){
			q.pop();
			continue;
		}
		q.pop();
		int sum = 1;
		for (int y = 0; y < 3; y++){
			for (int x = 0; x < 3; x++){
				if (y == 2 && x == 2){
					if (cur.gb[y][x] != 10){
						done = false;
						std::cout << "gb is: " << cur.gb[y][x] << ", should be: " << 10 << ", pos: { " << x << ", " << y << " }" << std::endl;
						break;
					}
				}
				else if (cur.gb[y][x] != sum){
					done = false;
					if (sum > 2)
						std::cout << "gb is: " << cur.gb[y][x] << ", should be: " << sum << ", pos: { " << x << ", " << y << " }"<<std::endl;
					break;
				}
				sum++;
			}
			if (!done)
				break;
		}
		if (done){
			return cur.numDraws;
		}
		if (cur.cleanPos.y - 1 >= 0){
			St n(cur.gb, Vec2(cur.cleanPos.x, cur.cleanPos.y - 1), cur.cleanPos, cur.numDraws+1);
			//std::vector<std::vector<int>> ngb = cur.gb;
			std::swap(n.gb[n.cleanPos.y][n.cleanPos.x], n.gb[cur.cleanPos.y][cur.cleanPos.x]);
			if (vis.find(n.gb) == vis.end()){
				vis.insert(n.gb);
				q.push(n);
			}
		}
		if (cur.cleanPos.y + 1 < gb.size()){
			St n(cur.gb, Vec2(cur.cleanPos.x, cur.cleanPos.y + 1), cur.cleanPos, cur.numDraws + 1);
			//std::vector<std::vector<int>> ngb = cur.gb;
			std::swap(n.gb[n.cleanPos.y][n.cleanPos.x], n.gb[cur.cleanPos.y][cur.cleanPos.x]);
			if (vis.find(n.gb) == vis.end()){
				vis.insert(n.gb);
				q.push(n);
			}
		}
		if (cur.cleanPos.x - 1 >= 0){
			St n(cur.gb, Vec2(cur.cleanPos.x - 1, cur.cleanPos.y), cur.cleanPos, cur.numDraws + 1);
			//std::vector<std::vector<int>> ngb = cur.gb;
			std::swap(n.gb[n.cleanPos.y][n.cleanPos.x], n.gb[cur.cleanPos.y][cur.cleanPos.x]);
			if (vis.find(n.gb) == vis.end()){
				vis.insert(n.gb);
				q.push(n);
			}
		}
		if (cur.cleanPos.x + 1 < gb[cur.cleanPos.y].size()){
			St n(cur.gb, Vec2(cur.cleanPos.x + 1, cur.cleanPos.y), cur.cleanPos, cur.numDraws + 1);
			//std::vector<std::vector<int>> ngb = cur.gb;
			std::swap(n.gb[n.cleanPos.y][n.cleanPos.x], n.gb[cur.cleanPos.y][cur.cleanPos.x]);
			if (vis.find(n.gb) == vis.end()){
				vis.insert(n.gb);
				q.push(n);
			}
		}
	}
	return -1;
}

struct State{
	std::vector<int> gb;
	int pos;
	int moves = 0;
	State(std::vector<int> _gb, int _pos, int _moves) : gb(_gb), pos(_pos), moves(_moves){}
	State(){}
};
const int SIZE = 9;

int main(){
	/*std::vector<std::vector<int>> gb(3, std::vector<int>(3));
	Vec2 cleanPos;
	for (int y = 0; y < 3; y++){
		for (int x = 0; x < 3; x++){
			int i;
			std::cin >> i;
			if (i == 0){
				cleanPos = Vec2(x, y);
				gb[y][x] = 10;
			}
			else{
				gb[y][x] = i;
			}
		}
	}*/
	std::vector<int> correct = { 1, 2, 3, 4, 5, 6, 7, 8, 0 };
	std::vector<int> gb(9);
	int cleanPos;
	for (int i = 0; i < SIZE; i++){
		std::cin >> gb[i];
		if (gb[i] == 0){
			cleanPos = i;
		}
	}
	//1 5 2 7 0 3 8 4 6
	//0 1 2 3 4 5 6 7 8
	/*
	0 1 2
	3 4 5
	6 7 8
	*/
	std::queue<State> q;
	std::set < std::vector<int>> visited;
	q.push(State(gb, cleanPos, 0));
	while (true){
		State cur = q.front();
		q.pop();
		if (cur.gb == correct){
			std::cout << cur.moves << std::endl;
#ifdef WIN32
			system("PAUSE");
#endif
			return 0;
		}
		if (cur.pos > 2){
			//Move upwards
			State n = cur;
			n.pos -= 3;
			n.moves++;
			std::swap(n.gb[cur.pos], n.gb[n.pos]);
			int size = visited.size();
			visited.insert(n.gb);
			if (size != visited.size()){
				//Add to queue
				q.push(n);
			}
		}
		if (cur.pos < 6){
			//Move down
			State n = cur;
			n.pos += 3;
			n.moves++;
			std::swap(n.gb[cur.pos], n.gb[n.pos]);
			int size = visited.size();
			visited.insert(n.gb);
			if (size != visited.size()){
				//Add to queue
				q.push(n);
			}
		}
		if (cur.pos % 3 > 0){
			//Move left
			State n = cur;
			n.pos--;
			n.moves++;
			std::swap(n.gb[cur.pos], n.gb[n.pos]);
			int size = visited.size();
			visited.insert(n.gb);
			if (size != visited.size()){
				//Add to queue
				q.push(n);
			}
		}
		if (cur.pos % 3 < 2){
			//Move right
			State n = cur;
			n.pos++;
			n.moves++;
			std::swap(n.gb[cur.pos], n.gb[n.pos]);
			int size = visited.size();
			visited.insert(n.gb);
			if (size != visited.size()){
				//Add to queue
				q.push(n);
			}
		}
	}
	//std::cout << Do(gb, cleanPos, 0) << std::endl;
#ifdef WIN32
	system("PAUSE");
#endif
}