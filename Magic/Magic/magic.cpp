#include "magic.h"

#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <map>
using namespace std;
vector<int> _R, _L;
vector<int> tricks;
vector<vector<int>> dp;
int Do(int turn, int magic, int score){
	if (turn >= _R.size()){
		return score;
	}
	
	//if (dp[turn][magic] != -1){
		//return dp[turn][magic];
	//}
	int lower = min(_L[turn], _R[turn]), upper = max(_L[turn], _R[turn]);
	int add = 0;
	if (!(lower <= 0 && upper >= 0)){
		add = Do(turn + 1, magic, score); //There is no other free "slot" here
	}
	else{
		add = max(add, Do(turn+1, magic, score + abs(_R[turn] + _L[turn])/2));
	}
	int spend = min(lower, magic);
	if (lower < 0){
		if (-lower > magic){
			spend = magic;
		}
		else{
			spend = lower;
		}
	}
	if (spend <= upper && spend >= lower){
		add = max(add, Do(turn + 1, magic - abs(spend), score + abs(spend - (_R[turn] + _L[turn]) / 2)));
	}
	spend = min(upper, magic);
	if (upper < 0){
		if (-upper > magic){
			spend = magic;
		}
		else{
			spend = upper;
		}
	}
	if (spend <= upper && spend >= lower){
		add = max(add, Do(turn + 1, magic - abs(spend), score + abs(spend - (_R[turn] + _L[turn]) / 2)));
	}
	//We either want to spend all we can here, (as much as possible or as little as possible)
	/*
	for (int i = lower; i <= upper; ++i){
		if (magic >= abs(i))
			add = max(add, Do(turn + 1, magic - abs(i), score + abs(i - (_L[turn] + _R[turn]) / 2)));
	}*/
	//dp[turn][magic] = add;
	return add;
}

int magic_score(int N, int K, int L[], int R[]) {
	tricks = vector<int>(N);
	_R = vector<int>(N);
	_L = vector<int>(N);
	dp = vector<vector<int>>(N, vector<int>(K + 1, -1));
	for (int i = 0; i < N; ++i){
		_R[i] = R[i];
		_L[i] = L[i];
	}
	int ret = Do(0, K, 0);
	for (int i = 0; i < N; i++)
		trick(0);
	return ret;
}
/*
2 5 
-2 5 
0 7
*/