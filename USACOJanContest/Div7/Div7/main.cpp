#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
typedef long long ll;


int main()
{
    std::ifstream ifs("div7.in");
    std::cin.rdbuf(ifs.rdbuf());
    std::ofstream ofs("div7.out");
    std::cout.rdbuf(ofs.rdbuf());

    int N;
    std::cin >> N;
    std::vector<ll> ids(N);
    ll totsum = 0;
    for(int i = 0; i < N; i++){
        std::cin >> ids[i];
        totsum += ids[i];
    }
    ll lrgsgroup = 0;
    for(int i = 0; i < N; i++){
        //Add numbers
        ll cursum = totsum;
        //ll groupsize = lastlrgsgroup, sum = sumtolargest, largestgroup = 0;
        //sumtolargest = std::max(sum-ids[i], (ll)0);
        /*if(lrgsgroup >= N - i){
            //This can't be a group
            continue;
        }*/
        for(int c = N-1; c > i + lrgsgroup; --c){
            if(cursum % 7 == 0){
                lrgsgroup = std::max((ll)c - i+1, lrgsgroup);
                break;
            }
            cursum -= ids[c];
            /*
            if(c > i && groupsize <= lrgsgroup){
                sumtolargest += ids[c];
            }
            sum += ids[c];
            ++groupsize;
            if(sum % 7 == 0){
                largestgroup = std::max(groupsize, largestgroup);
            }*/
        }
        totsum -= ids[i];
        //lrgsgroup = std::max(largestgroup, lrgsgroup);
    }
    std::cout << lrgsgroup << std::endl;
    return 0;
}

