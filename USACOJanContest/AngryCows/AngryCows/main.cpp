#include <iostream>
#include <fstream>
#include <vector>

int main()
{
    std::ifstream ifs("angry.in");
    std::cin.rdbuf(ifs.rdbuf());
    std::ofstream ofs("angry.out");
    std::cout.rdbuf(ofs.rdbuf());

    int N, K;
    std::cin >> N >> K;
    std::vector<int> dist(N);
    for(int i = 0; i < N; i++){
        std::cin >> dist[i];
    }
}

