#include <iostream>
#include <fstream>

#include <string>
#include <vector>
int main()
{
    std::ifstream ifs("gates.in");
    std::cin.rdbuf(ifs.rdbuf());
    std::ofstream ofs("gates.out");
    //std::cout.rdbuf(ofs.rdbuf());
    int N, toteast = 0, totwest = 0, totnorth = 0, totsouth = 0;
    std::string path;
    std::cin >> N >> path;
    int H = 0, W = 0;
    for(int i = 0; i < N; i++){
        if(path[i] == 'N' || path[i] == 'S'){
            ++H;
            if(path[i] == 'N'){
                ++totnorth;
            }else{
                ++totsouth;
            }
        }else if(path[i] == 'E' || path[i] == 'W'){
            ++W;
            if(path[i] == 'E'){
                ++toteast;
            }else{
                ++totwest;
            }
        }
    }
    std::vector<std::vector<bool>> occupied(W, std::vector<bool>(H, false));
    std::vector<std::vector<int>> edge; //This is every rooms neighbour
    int x = totwest, y = totsouth;
    for(int i = 0; i < N; i++){
        occupied[x][y] = true;
        if(path[i] == 'N'){
            ++y;
        }else if(path[i] == 'S'){
            --y;
        }else if(path[i] == 'E'){
            ++x;
        }else{
            --x;
        }
    }
    for(int y = 0; y < H; y++){
        for(int x = 0; x < W; x++){
            std::cout << occupied[x][y];
        }
        std::cout << std::endl;
    }
    return 0;
}

