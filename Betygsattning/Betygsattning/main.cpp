#include <iostream>
#include <vector>
typedef unsigned int uint;
int main()
{
    std::vector<uint> g(5);
    for(uint i = 0; i < 5; i++){
        std::cin >> g[i];
    }
    uint p;
    std::cin >> p;
    for(uint i = 0; i < 5; i++){
        if(p >= g[i]){
            std::cout << (char)('A' + (char)i) << std::endl;
            return 0;
        }
    }
    std::cout << "F" << std::endl;
    return 0;
}

