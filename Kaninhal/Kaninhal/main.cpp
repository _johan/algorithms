#include <iostream>
#include <vector>
#include <algorithm>

void emplacevec(std::vector<int> &vec, int k){
    //Allt bakom k ska framåt
    //Allt innan k ska bli reversed

    std::vector<int> cur(vec);
    //std::reverse(cur.begin(), cur.begin()+k);
    for(int i = k; i < vec.size(); i++){
        vec[i-k] = vec[i];
    }
    for(int i = 0; i < k; i++){
        vec[vec.size()-1-i] = cur[i];
    }
}

int main()
{
    int N;
    std::cin >> N;
    std::vector<int> beaver(N);
    std::vector<int> original(N);
    for(int i = 0; i < N; i++){
        std::cin >> beaver[i];
        original[i] = i+1;
    }
    std::reverse(beaver.begin(), beaver.end());
    //while(true){
        std::vector<int> current;
        current.reserve(N);
        for(int i = 1; i < N; i++){
            for(int j = 1; j < N; j++){
                for(int c = 1; c < N; c++){
                    current = original;
                    //Allt som hamnar i hålet ska reversas och resten hamnar längst fram
                    //for()

                    /*for(int k = 0; k < i; k++){
                        std::swap(current[current.size()-1-i+k], current[k]);
                    }
                    for(int k = 0; k < j; k++){
                        std::swap(current[current.size()-1-j+k], current[k]);
                    }
                    for(int k = 0; k < c; k++){
                        current[k] = current[current.size()-1-c+k];

                        //std::swap(current[current.size()-1-c+k], current[k]); //Den ska längst bak och allt annat ska fram ett steg
                    }*/
                    emplacevec(current, i);
                    emplacevec(current, j);
                    emplacevec(current, c);
                    if(current == beaver){
                        std::cout << i << " " << j << " " << c << std::endl;
                        return 0;
                    }

                }
            }
        }
    //}
        std::cout << "Be4 main return" << std::endl;
    return 0;
}

