#include "friends.h"
#include <cstdio>
#include <cassert>
#include <utility>
#include <vector>
#include <algorithm>
#include <set>
using namespace std;
typedef long long ll;
set<int> people;
ll totscore = 0;
void init(int N, int L, int P[]) {
	people.insert(P, P + N);
	totscore = score();
}
void jump(int A, int B) {
	ll previous = A;
	ll leftSize = 0, rightSize = 0;
	set<int>::iterator it = people.find(A);
	++it;
	for (; it != people.end(); ++it){
		if (previous + 1 == *it){
			++rightSize;
			previous = *it;
		}
		else{
			break;
		}
	}
	it = people.find(A);
	previous = A;
	for (; it != people.begin();){
		--it;
		if (previous - 1 == *it){
			++leftSize;
			previous = *it;
		}
		else{
			break;
		}
	}

	totscore += leftSize*leftSize + rightSize*rightSize - (leftSize + rightSize + 1) * (leftSize + rightSize + 1);
	people.erase(A);
	people.insert(B);

	previous = B;
	leftSize = 0, rightSize = 0;
	it = people.find(B);
	++it;
	for (; it != people.end(); ++it){
		if (previous + 1 == *it){
			++rightSize;
			previous = *it;
		}
		else{
			break;
		}
	}
	previous = B;
	it = people.find(B);
	for (; it != people.begin();){
		--it;
		if (previous - 1 == *it){
			++leftSize;
			previous = *it;
		}
		else{
			break;
		}
	}
	totscore += (leftSize + rightSize + 1) * (leftSize + rightSize + 1) - leftSize * leftSize - rightSize * rightSize;
	/*
	//set<int>::reverse_iterator it_end = people.find(B);
	ll prev = A;
	int rightSize = 0, leftSize = 0;
	for (set<int>::iterator it_start = std::next(people.find(A)); it_start != people.end(); ++it_start){
		if (prev + 1 != *it_start){
			--it_start;
			break;
		}
		else{
			++rightSize;
			prev = *it_start;
		}
	}
	prev = A;
	for (set<int>::iterator it_end = std::prev(people.find(A)); it_end != std::prev(people.begin()); --it_end){
		if (prev - 1 != *it_end){
			++it_end;
			break;
		}
		else{
			++leftSize;
			prev = *it_end;
		}
	}
	rightSize++;
	totscore += (rightSize-1)*(rightSize-1) + leftSize * leftSize - (rightSize + leftSize) * (rightSize + leftSize);
	people.erase(A);
	people.insert(B);

	prev = B;
	rightSize = 0, leftSize = 0;
	for (set<int>::iterator it_start = std::next(people.find(B)); it_start != people.end(); ++it_start){
		if (prev + 1 != *it_start){
			--it_start;
			break;
		}
		else{
			++rightSize;
			prev = *it_start;
		}
	}
	prev = B;
	for (set<int>::iterator it_end = std::prev(people.find(B)); it_end != std::prev(people.begin()); --it_end){
		if (prev - 1 != *it_end){
			++it_end;
			break;
		}
		else{
			++leftSize;
			prev = *it_end;
		}
	}
	rightSize++;
	totscore += (rightSize + leftSize) * (rightSize + leftSize) - (rightSize-1)*(rightSize-1) - leftSize * leftSize ;
	*/
}
long long score() {
	if (totscore != 0){
		return totscore;
	}
	long long score = 0;
	ll len =0, last = 0;
	for (int p : people){
		if (p - 1 == last){
			++len;
			last = p;
		}
		else{
			score += len * len;
			last = p;
			len = 1;
		}
	}
	score += len*len;
	return score;
}
/*
5 7 3 
0 1 2 3 4 
1 
0 2 5 
1 
*/
/*
1 1000000 1 
0 
1
*/