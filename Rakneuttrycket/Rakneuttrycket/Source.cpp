#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <climits>
#include <utility>
//L�gg ihop talet till h�ger, kolla operatorn och l�gg ihop dem.
//G�r sedan om str�ngen
typedef long long ll;
typedef unsigned int uint;

inline ll addNum(std::vector<char>& op, ll l, ll r, uint pos){
	//Kommer inte funka
	//Talen kan vara l�ngre �n 1 siffra
	//M�ste lagra talen i en vektor och opeartorerna i en anan vektor
	//Och anv�nda dem
    ll ret = 0;
    if (op[pos] == '*'){
        ret = l * r;

	}
	else if (op[pos] == '+'){
        ret = l+r;

	}
	else{
		//Minus
        ret = l-r;

	}

    /*cur.erase(cur.begin() + pos+1);
	op.erase(op.begin() + pos);
    if(cur.size() == 1){
        return cur[0];
    }else{
        return 0;
    }*/
    return ret;

}

ll max = 0;
ll Do(std::vector<ll> &num, std::vector<char> &op){

    /*
	if (cur.size() == 1){
		return pts;
	}
	ll best = pts;
	for (int i = 0; i < (cur.size() - 1); i++){
		std::vector<ll> c(cur);
		std::vector<char> o(op);
		best = std::max(best, Do(c, o, addNum(c, o, i)));
	}

    return best;*/
    ll N = num.size();
    std::vector<std::vector<std::pair<ll,ll>>> res(N,std::vector<std::pair<ll,ll>>(N));
    for(uint i = 0; i < N; i++){
        res[i][i] = std::make_pair(num[i], num[i]);
    }
    for(uint len = 1; len < N; len++){
        for(uint start = 0; start < (N - len); start++){
            ll max = LONG_MIN;
            ll min = LONG_MAX;
            for(uint i = 0; i < len; i++){
                ll a = addNum(op, res[start][start+i].first, res[start+i+1][start+len].first, start+i);
                ll b = addNum(op, res[start][start+i].second, res[start+i+1][start+len].first, start+i);
                ll c = addNum(op, res[start][start+i].first, res[start+i+1][start+len].second, start+i);
                ll d = addNum(op, res[start][start+i].second, res[start+i+1][start+len].second, start+i);
                min = std::min(min, std::min(std::min(a,b), std::min(c,d)));
                max = std::max(max, std::max(std::max(a,b), std::max(c,d)));
            }
            res[start][start+len] = std::make_pair(max, min);
        }
    }
    return res[0][op.size()].first;
}
/*
ll Do37(std::vector<ll> &cur, std::vector<char> &op, ll pts){
    if (cur.size() == 1){
        return pts;
    }
    //First pass, add all the numbers
    for(uint i = 0; i < (cur.size()-1); i++){
        if(op[i] == '+'){
            addNum(cur, op, i);
            i--;
        }
    }
    //Second pass multiply all the numbers
    for (uint i = 0; i < (cur.size()-1); i++){
         addNum(cur, op, i);
         i--;
    }

    return cur[0];
}
*/

int main(){
	std::string in;
	std::cin >> in;
	std::vector<ll> num;
	std::vector<char> op;
	std::reverse(in.begin(), in.end());
	ll sum = 0, len = 0;
    bool found = false;
    bool sol37 = true;
	for (uint i = 0; i < in.size(); i++){
		
		if (in[i] == '*'){
            if (found){
				num.push_back(sum);
			}
            found =false;
			sum = 0;
			len = 0;
			op.push_back(in[i]);
		}
		else if (in[i] == '-'){
            if (found){
				num.push_back(sum);
			}
            sol37 = false;
            found =false;
			sum = 0;
			len = 0;
			op.push_back(in[i]);
		}
		else if (in[i] == '+'){
            if (found){
				num.push_back(sum);
			}
            found =false;
			sum = 0;
			len = 0;
			op.push_back(in[i]);
		}
		else{
            found = true;
			sum += (in[i] - '0') * std::pow(10, len);
			len++;
		}
	}
    if (found){
		num.push_back(sum);
	}
	std::reverse(num.begin(), num.end());
	std::reverse(op.begin(), op.end());
	/*
	for (ll i : num){
		std::cout << i << " ";
	}
	std::cout << "\n";
	for (char c : op){
		std::cout << c << " ";
	}*/
    //if(!sol37)
    std::cout << Do(num, op) << std::endl;
    //else
     //   std::cout << Do37(num, op, 0) << std::endl;

#ifdef WIN32
	system("PAUSE");
#endif
	//1023*98+31
	//1023*98+31-21

}
