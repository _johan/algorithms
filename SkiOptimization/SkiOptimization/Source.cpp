#include <iostream>
#include <string>
#include <vector>
#include <climits>
#include <set>
#include <utility>

class comp{
    bool reverse;
public:
    comp(const bool& revparam = false)
    {
        reverse = revparam;
    }
    bool operator ()(const std::pair<int, int>& lhs, const std::pair<int, int>& rhs){
        if (lhs.second == rhs.second){
            return lhs.first < rhs.first;
        }
        if (lhs.second > rhs.second){
            return true;
        }
        return false;
    }

};

/*
Floyd-Warshall kommer inte fungera
*/
typedef unsigned int uint;

struct Edge{
	Edge(){}
	Edge(uint __a, uint __b){
		a = __a;
		b = __b;
	}
	uint a;
	uint b;
};
int main(){
    std::cout << "Hello" << std::endl;
	uint N, K;
	std::cin >> N >> K;
	std::vector<Edge> e(N);
    std::vector<std::vector<uint>> edge(N);
	for (uint i = 0; i < N; i++){
		uint a, b;
		std::cin >> a >> b;
		e[i] = Edge(--a, --b);
        edge[a].push_back(b);
        edge[b].push_back(a);
	}
    //std::vector<std::vector<uint>> dist(N, std::vector<uint>(N, INT_MAX));
    //std::vector<std::vector<uint>> next(N, std::vector<uint>(N, 0));

    std::vector<uint> n(N, INT_MAX);
    std::vector<uint> prev(N, 0);
    std::set<std::pair<uint,uint>> q;

    n[0] = 0;
	for (uint i = 0; i < N; i++){
        q.insert(std::make_pair(i, n[i]));
    }

    while(!q.empty()){
        std::pair<uint,uint> cur = *q.begin();
        q.erase(q.begin());

        for(uint i = 0; i < edge[cur.first].size(); i++){
            uint alt = cur.second + 1;
            if(alt < n[edge[cur.first][i]]){
                q.erase(std::make_pair(edge[cur.first][i], n[edge[cur.first][i]]));
                n[edge[cur.first][i]] = alt;
                prev[edge[cur.first][i]] = cur.first;
                q.insert(std::make_pair(edge[cur.first][i], n[edge[cur.first][i]]));
            }
        }
    }
    for(uint i = 0; i < N; i++){
        std::cout << n[i] << " ";
    }
    std::cout << std::endl;

    for(uint i = 0; i < N; i++){
        std::cout << prev[i] << " ";
    }
    std::cout << std::endl;
/*
	for (uint i = 0; i < N; i++){
		for (uint x = 0; x < N; x++){
			std::cout << dist[i][x] << " ";
		}
		std::cout << std::endl;
    }
	std::cout << std::endl << std::endl;
	for (uint i = 0; i < N; i++){
		for (uint x = 0; x < N; x++){
			std::cout << next[i][x] << " ";
		}
		std::cout << std::endl;
    }*/
#ifdef WIN32
	system("PAUSE");
#endif
}
