#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <climits>
#include <utility>
#include <iomanip>
#define all(v) v.begin(), v.end()
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef pair<int,int> pii;
typedef vector<pii> vpii;
ll N;
vector<pair<double,double>> rocks;

inline double f(double x){
    double ret = 0;
    for(int i = 0; i < N; ++i){
        ret += sqrt((rocks[i].first - x) * (rocks[i].first - x) + rocks[i].second * rocks[i].second);
    }
    return ret;
}
double prec = 1e-12;
double tern(double left, double right){
    if(abs(right - left) < prec){
        return (left + right) / 2;
    }
    double leftThird = (2*left + right) / 3, rightThird = (left + 2*right) / 3;
    if(f(leftThird) > f(rightThird)){
        return tern(leftThird, right);
    }else{
        return tern(left, rightThird);
    }
}

int main()
{
    cin >> N;
    rocks = vector<pair<double,double>>(N);
    for(int i = 0; i < N; ++i){
        cin >> rocks[i].first >> rocks[i].second;
    }/*
    double x;
    sort(all(rocks));
    int median;
    x = rocks[N/2].first;*/
    /*if(N % 2 == 0){
        int l = N / 2, r = l+1;
        x = (rocks[l-1].first + rocks[r-1].first) / 2;
    }else{
        x = rocks[N/2 - 1].first; 0 1 2 3 4
    }*/
    double x = tern(0.0, 100.0);
    double tot = 0;
    for(int i = 0; i < N; ++i){
        tot += 2*sqrt(pow(x - rocks[i].first,2)+ pow(rocks[i].second, 2));
    }
    cout << setprecision(12) << tot << endl;
}

