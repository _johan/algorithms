#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <cmath>
#include <algorithm>
using namespace std;

int R, C;
vector<vector<double>> gb;
vector<int> numpos;
int main(){
	cin >> R >> C;
	gb = vector<vector<double>>(R, vector<double>(C, (int)'.'));
	numpos = vector<int>(R);
	for (int i = 0; i < R; ++i){
		for (int j = 0; j < C; ++j){
			char c;
			cin >> c;
			if (c != '.'){
				numpos[i] = j;
				gb[i][j] = (int)c - '0';
			}
		}
	}
	double curd = 1, upperd = 1000000000000, lowerd = 1000000000000;
	bool set = false;
	for (int i = 0; i < R; ++i){
		double lastnuminrow = gb[i][0], lastindex = 0;
		bool encounterednum = false;
		for (int j = 0; j < C; ++j){
			if (gb[i][j] != (int)'.'){
				if (!encounterednum){
					lastnuminrow = gb[i][j];
					lastindex = j;
					encounterednum = true;
					continue;
				}
				double d = (lastnuminrow - gb[i][j]) / (lastindex - j);
				upperd = min(lastnuminrow - gb[i][j], upperd);
				lowerd = min(lastindex - j, lowerd);
				//Ta divider och �vre numret f�r att r�kna ut det
				if (set){
					if (curd != d){
						cout << "ej magisk" << endl;
#ifdef WIN32
						system("PAUSE");
#endif
						return 0;
					}
				}
				else{
					curd = d;
					set = true;
				}
				lastnuminrow = gb[i][j];
				lastindex = j;
			}
		}
	}
	if (lowerd < 0){
		lowerd *= -1;
		upperd *= -1;
	}
	for (int i = 0; i < R; ++i){
		double numonrow = gb[i][numpos[i]], index = numpos[i];
		for (int j = 0; j < C; ++j){
			if (gb[i][j] == (int)'.'){
				double num = numonrow - curd * (index - j);
				if (num - (int)num != 0){
					//Br�k
					double upper = numonrow - upperd * (index - j), lower = numonrow - lowerd * (index - j);
					cout << upper << "/" << lowerd << " ";
				}else
				cout << num << " ";
			}
			else
				cout << gb[i][j] << " ";
		}
		cout << endl;
	}

#ifdef WIN32
	system("PAUSE");
#endif
}