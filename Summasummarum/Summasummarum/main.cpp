#include <iostream>
#include <vector>
#include <climits>
#include <algorithm>

typedef long long ll;
int main(){
	int N;
	std::cin >> N;
    if(N == 0){
        std::cout << 0 << std::endl;
    }
	std::vector<ll> B(N);
	std::vector<ll> G(N);
	for (int i = 0; i < N; i++){
		std::cin >> B[i];
	}
	for (int i = 0; i < N; i++){
		std::cin >> G[i];
	}
	ll BSUM = 0, GSUM = 0, diff;
	for (int i = 0; i < N; i++){
		BSUM += B[i];
		GSUM += G[i];
	}
	diff = BSUM - GSUM;
	//Positiv diff: Ta fr�n B och ge till G
	//Negativ diff; Ta fr�n G och ge till B
	ll mindiff = LONG_MAX;
	std::sort(G.begin(), G.end());
	for (int b = 0; b < N; b++){
        ll tmpbsum = BSUM, bnum, requiredforzero, tmpgsum = GSUM;
		bnum = B[b];
		tmpbsum -= B[b];
        tmpgsum += B[b];
        requiredforzero = (tmpgsum - tmpbsum)/2;
		ll bestmatch = LONG_MAX;
        ll lowerbound = 0, upperbound = G.size()-1, position = (lowerbound + upperbound)/2;
		while (upperbound >= lowerbound){
			if (G[position] == requiredforzero){
				//Correct number
				bestmatch = G[position];
				break;
			}
            if (G[position] > requiredforzero){
                upperbound = position-1;
                if(G[position] - requiredforzero < std::abs(bestmatch - requiredforzero)){
                    bestmatch = G[position];
                }
			}
			else{
                lowerbound = position+1;
                if(requiredforzero - G[position] < std::abs(bestmatch - requiredforzero)){
                    bestmatch = G[position];
                }
			}
            position = (upperbound+lowerbound)/2;
		}
		tmpbsum += bestmatch;
        tmpgsum -= bestmatch;
        //ll tmpgsum = GSUM + bnum - bestmatch;
		if (std::abs(tmpbsum - tmpgsum) < mindiff){
			mindiff = std::abs(tmpbsum - tmpgsum);
		}
	}

	std::cout << mindiff << std::endl;
#ifdef WIN32
	system("PAUSE");
#endif
}
