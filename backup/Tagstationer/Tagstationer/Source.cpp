#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <queue>

typedef long long ll;

bool comp(const std::pair<ll,ll> &lhs, const std::pair<ll,ll> &rhs){
    if(lhs.first < rhs.first){
        return true;
    }
    return false;
}


void BFS(std::queue<ll> &q, const std::vector<std::pair<ll,ll>> &stat, std::vector<bool> vis, std::vector<ll> path ,ll numvis, ll pos){
    if(numvis == vis.size()){
        //We made it :D
        return;
    }
    ll posi = q.front();
    q.pop();
    for(ll i = 0; i < vis.size(); i++){
        if(!vis[i]){
            //Add to queue
            q.push(i);
        }
    }
    BFS(q, stat, vis, path, numvis+1, pos);
}

int main(){
    ll N;
    std::cin >> N;
    std::vector<std::pair<ll, ll>> s(N);
    for(ll i = 0; i < N; i++){
        ll in, out;
        std::cin >> in >> out;
        s[i] = std::make_pair(in, out);
    }

    std::sort(s.begin(), s.end(), comp);


}
