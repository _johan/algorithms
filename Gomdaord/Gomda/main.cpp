#include <iostream>
#include <string>

int main()
{
    std::string s;
    std::string out;
    std::cin >> s;
    for(int i = 0; i < s.size(); ){
        out += s[i];
        i += s[i] - 'A'+1;
    }
    std::cout << out << std::endl;
    return 0;
}

