#include <iostream>
#include <vector>
#include <fstream>
int main(){
	std::ifstream ifs("paint.in");
	std::cin.rdbuf(ifs.rdbuf());
	std::ofstream ofs("paint.out");
	std::cout.rdbuf(ofs.rdbuf());
	int a, b, c, d;
	std::cin >> a >> b >> c >> d;
	std::vector<bool> p(101, false);
	std::fill(p.begin() + a, p.begin() + b, true);
	std::fill(p.begin() + c, p.begin() + d, true);
	int counter = 0;
	for (bool b : p){
		if (b){
			counter++;
		}
	}
	std::cout << counter << std::endl;
}