#include "tshirts.h"
#include <iostream>
#include <vector>
#include <set>
#include <utility>
#include <map>
#include <queue>
using namespace std;
typedef pair<int, int> pii;
map<int, int> graph;
vector<pair<int, int>> cont;
vector<bool> hasShirt;
int last = 0, camefrom = 0;
bool bfs(vector<bool> visited, vector<int> &parent){
	queue<int> q;
	//q.push(0);
	//visited[0] = true;
	//parent[0] = -1;
	for (int i = 0; i < cont.size(); ++i){
		if (!hasShirt[i]){
			q.push(i);
		}
	}
	bool vis = false;
	while (!q.empty()){
		int cur = q.front();
		q.pop();
		for (int i = cont[cur].first; i < cont[cur].second; ++i){
			if (graph[i] > 0){
				/*
				This shouldn't work
				*/
				//parent[i] = cur;
				vis = true;
				last = i;
				camefrom = cur;
			}
		}
	}

	return (vis == true);
}

int tshirt(int N, int L[], int H[], int T[]) {
	cont.resize(N);
	hasShirt = vector<bool>(N + 1, false);
	int minShirt = 0, maxShirt = 0;
	for (int i = 0; i < N; ++i){
		cont[i] = pii(L[i], H[i]);
		++graph[T[i]];
		maxShirt = max(T[i], maxShirt);
	}
	int maxFlow = 0;
	vector<bool> vis(N);
	vector<int> parent(N + 1, -1);
	while (bfs(vis, parent)){
		--graph[last];
		++maxFlow;
		hasShirt[camefrom] = true;
	}
	return maxFlow;
	//cont[0] = pii(minShirt, maxShirt);
}
