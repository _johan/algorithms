#include <iostream>
using namespace std;
int main(){
	int N, sum = 0;
	cin >> N;
	for (int i = 0; i < N; ++i){
		int have, need, cost;
		cin >> have >> need >> cost;
		if (need > have){
			sum += (need - have) * cost;
		}
	}
	cout << sum << endl;
#ifdef WIN32
	system("PAUSE");
#endif
}