#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cmath>
#include <climits>
#include <utility>
#include <iomanip>
#include <fstream>

#define all(v) v.begin(), v.end()
using namespace std;
typedef long long ll;
typedef vector<int> vi;
typedef pair<int,int> pii;
typedef vector<pii> vpii;
int N, H;
vi len; vi freespace;

//I need DP I think
vector<vector<vector<vector<int>>>> dp;
int Do(int index){
    if(index >= N){
        return index;
    }
    if(dp[freespace[0]][freespace[1]][freespace[2]][index] != -1){
        return dp[freespace[0]][freespace[1]][freespace[2]][index];
    }
    int num = index;
    for(int i = 0; i < 4; ++i){
        //Does it fit?
        if(freespace[i] >= len[index]){
            //Choose this
            int lastfree = freespace[i];
            freespace[i] = max(freespace[i] - len[index] - 1, 0);
            num = max(num, Do(index+1));
            freespace[i] = lastfree;
        }
    }
    dp[freespace[0]][freespace[1]][freespace[2]][index] = num;
    return num;
}

int main()
{
    cin >> N >> H;
    dp = vector<vector<vector<vector<int>>>>(H+1, vector<vector<vector<int>>>(H+1, vector<vector<int>>(H+1, vector<int>(N+1, -1))));
    len = vi(N);
    freespace = vi(4, H);
    for(int i = 0; i < N; ++i){
        cin >> len[i];
    }
    cout << Do(0) << endl;
}

