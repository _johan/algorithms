#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <climits>
#include <set>
struct Rocket{
    Rocket(){}
    Rocket(float score, float price, float efficency, int index){
        this->score = score;
        this->price = price;
        this->efficency = efficency;
        this->index = index;
    }
    Rocket(float score, float price, float efficency, int index, int cp, int cc, int nr, std::vector<float> v){
        this->score = score;
        this->price = price;
        this->efficency = efficency;
        this->index = index;
        currentprice = cp;
        currentscore = cc;
        numrockets = nr;
        used = v;
    }

    float score = 0;
    float price = 0;
    float efficency = 0;
    int index = 0;
    int currentprice = 0;
    int currentscore = 0;
    int numrockets = 0;
    std::vector<float> used;

    bool operator< (const Rocket &rhs) const
    {
        if(currentprice == rhs.currentprice){
            if(currentscore == rhs.currentscore){
                if(numrockets == rhs.numrockets){
                    if(efficency == rhs.efficency){
                        if(price == rhs.price){
                            if(score == rhs.score){
                                return index < rhs.index;
                            }
                            return score < rhs.score;
                        }
                        return price < rhs.price;
                    }
                    return efficency < rhs.efficency;
                }
                return numrockets < rhs.numrockets;
            }
            return currentscore < rhs.currentscore;
        }
        return currentprice < rhs.currentprice;
    }
};

bool comp(const Rocket &lhs, const Rocket &rhs){
    if(lhs.efficency == rhs.efficency){
        if(lhs.price == rhs.price){
            return lhs.score > rhs.score;
        }
        return lhs.price < rhs.price;
    }
    return lhs.efficency > rhs.efficency;
}

typedef std::pair<int,int> pii;

std::vector<float> usedresult;
float lowestcost = INT_MAX, lowestrockets = 0;

float Do(const std::vector<Rocket> &rockets, std::vector<float> &used, float currentprice, float currentscore, float numused, const float reqscore){
    if(currentprice > lowestcost){
        return INT_MAX;
    }
    if(currentscore >= reqscore){
        if(currentprice < lowestcost){
            usedresult = used;
            lowestrockets = numused;
            lowestcost = currentprice;
        }
        return currentprice;
    }
    float add = INT_MAX;
    for(int i = 0; i < rockets.size(); i++){
        if(used[i] == 0 || used[i] / numused < 0.5){
            ++used[i];
            add = std::min(add, Do(rockets, used, currentprice+rockets[i].price, currentscore+rockets[i].score, numused+1, reqscore));
            --used[i];
        }
    }
    return add;
}

float clamp(float number, float clampvalue){
    return std::max(number, clampvalue);
}

int main(){
    int T,S;
    std::cin >> T >> S;
    std::vector<Rocket> rockets(T);
    usedresult.resize(S);
    //first = "score", second = "price"
    float currentprice = 0, currentscore = 0, minimum = INT_MAX, maximum = 0, left = 0;
    for(int i = 0; i < T; i++){
        float sc, pc;
        std::cin >> sc >> pc;
        rockets[i] = Rocket(sc, pc, std::min((float)S, sc)/pc, i); //Allt över S spelar ingen roll, vi har ingen nyttja av det
        minimum = std::min(minimum, sc);
        maximum = std::max(maximum, sc);
        left += sc;
        //std::cin >> rockets[i].first >> rockets[i].second;
    }
    std::sort(rockets.begin(), rockets.end(), comp);
    std::vector<float> used(T, 0);
    float numrockets = 0; //Use floats insead of int's cause casting sucks
    //Start with highest efficency
    //Om någon kan ta mig till målet, vill jag ta den billigaste
    //Annars vill jag ta den effektivaste kostnadsvis som är tillgänglig
    //std::cout << Do(rockets, used, 0, S, 0) << std::endl;

    /*
    while(true){//S - currentscore <= 0
        if((S - currentscore >= 0 && S - currentscore <= maximum) ){
            //Find a sequence of these with the lowest cost that is greater then S - currentscore
            currentprice = Do(rockets, used, currentprice, 0, numrockets, S - currentscore);
            used = usedresult;
            numrockets = lowestrockets;
            //Verify
            bool valid = true;
            int lowestcost = INT_MAX, lowestindex = 0;
            for(int i = 0; i < T; i++){
                if(used[i] == 0 || used[i] / numrockets < 0.5){
                    if(rockets[i].price < lowestcost){
                        lowestindex = i;
                        lowestcost = rockets[i].price;
                    }
                }else{
                    valid = false;
                    //break;
                }
            }

            if(!valid){
                currentscore += rockets[lowestindex].score;
                currentprice += rockets[lowestindex].price;
                ++used[lowestindex];
                ++numrockets;
            }
            valid = true;
            lowestcost = INT_MAX, lowestindex = 0;
            for(int i = 0; i < T; i++){
                if(used[i] == 0 || used[i] / numrockets < 0.5){
                    if(rockets[i].price < lowestcost){
                        lowestindex = i;
                        lowestcost = rockets[i].price;
                    }
                }else{
                    valid = false;
                    //break;
                }
            }
            if(!valid){
                currentscore += rockets[lowestindex].score;
                currentprice += rockets[lowestindex].price;
                ++used[lowestindex];
                ++numrockets;
            }
            std::cout << currentprice << std::endl;
            return 0;

        }else if(S - currentscore > 0){
            //As far as possible, use the ne with the highest possible efficency
            for(int i = 0; i < T; i++){
                if(used[i] == 0 || used[i] / numrockets < 0.5){
                    ++numrockets;
                    ++used[i];
                    currentprice += rockets[i].price;
                    currentscore += rockets[i].score;
                    break;
                }
            }
        }
    }*/
    std::set<Rocket> q;
    Rocket beginrocket = rockets.front();
    beginrocket.used = used;
    q.insert(beginrocket);
    //++used[rockets.front().index];
    //++numrockets;
    float lowestsofar = INT_MAX;
    while(q.size() > 0){
        Rocket r = *q.begin();
        //++used[r.front().index];
        //++numrockets;
        if(r.currentscore >= S){
            //Verfiy...
            bool valid = true;
            int lowestcost = INT_MAX, lowestindex = 0;
            for(int i = 0; i < T; i++){
                if(r.used[rockets[i].index] == 0 || r.used[i] / r.numrockets < 0.5){
                    if(rockets[i].price < lowestcost){
                        lowestindex = i;
                        lowestcost = rockets[i].price;
                    }
                }else{
                    valid = false;
                    //break;
                }
            }

            if(!valid){
                r.currentscore += rockets[lowestindex].score;
                r.currentprice += rockets[lowestindex].price;
                ++r.used[lowestindex];
                ++r.numrockets;
            }
            valid = true;
            lowestcost = INT_MAX, lowestindex = 0;
            for(int i = 0; i < T; i++){
                if(r.used[rockets[i].index] == 0 || r.used[rockets[i].index] / r.numrockets < 0.5){
                    if(rockets[i].price < lowestcost){
                        lowestindex = i;
                        lowestcost = rockets[i].price;
                    }
                }else{
                    valid = false;
                    //break;
                }
            }
            if(!valid){
                r.currentscore += rockets[lowestindex].score;
                r.currentprice += rockets[lowestindex].price;
                ++r.used[lowestindex];
                ++r.numrockets;
            }

            //Terminatus
            lowestsofar = std::min(lowestsofar, (float)r.currentprice);
            std::cout << lowestsofar << std::endl;
            return 0;
        }
        q.erase(q.begin());
        for(int i = 0; i < T; i++){
            if(r.used[rockets[i].index] == 0 || r.used[rockets[i].index] / r.numrockets < 0.5){
                ++r.used[rockets[i].index];
                ++r.numrockets;
                r.currentprice += rockets[i].price;
                r.currentscore += rockets[i].score;
                /*
                Verify here and save the lowest value
*/
                if(r.currentscore >= S){
                bool valid = true;
                    for(int c = 0; c < T; c++){
                        if(r.used[rockets[c].index] != 0 && r.used[rockets[c].index] / r.numrockets >= 0.5){
                            valid = false;
                        }
                    }
                    if(valid){
                        lowestsofar = std::min(lowestsofar, (float)r.currentprice);
                    }
                }
                q.insert(r);
                --r.used[rockets[i].index];
                --r.numrockets;
                r.currentprice -= rockets[i].price;
                r.currentscore -= rockets[i].score;
            }
        }
    }
    /*
    while(true){
        bool done = false;
        if(S - currentscore >= minimum && S - currentscore <= maximum){
            //Recalculate the efficency using std::min(S-currentscore, rockets[i].score)
            int lowestindex = 0, lowestcost = INT_MAX;
            for(int i = 0; i < T; i++){
                /*if(rockets[i].score == S - currentscore && lowestcost > rockets[i].score){
                    lowestindex = i;
                    lowestcost = rockets[i].price;
                }
                rockets[i].efficency = std::min(rockets[i].score, S-currentscore) / rockets[i].price;
                //Räkna ut effektiviteten så här
            }/*
            if(lowestcost != INT_MAX){
                ++used[lowestindex];
                ++numrockets;
                currentprice += rockets[lowestindex].price;
                currentscore += rockets[lowestindex].score;
            }*/
            //std::sort(rockets.begin(), rockets.end(), comp);
/*
            currentprice = Do(rockets, used, currentprice, currentscore, numrockets, S);
            done = true;
            used = usedresult;
            numrockets = lowestrockets;
*/          //Do a recursive function here. That should be engouht, right?
            /*
            int lowestcost = INT_MAX, lowestindex = -1; //This is the only part that could cause a seg fault
            for(int i = 0; i < T; i++){
                if(used[i] == 0 || used[i] / numrockets < 0.5 && S-currentscore-rockets[i].score <= 0 && rockets[i].price < lowestcost){
                    lowestcost = rockets[i].price;
                    lowestindex = i;
                }
            }
            used[lowestindex]++;
            numrockets++;
            currentprice += rockets[lowestindex].price;
            currentscore += rockets[lowestindex].score;

        }
        if(currentscore >= S || done){
            bool valid = true;
            int lowestcost = INT_MAX, lowestindex = 0;
            for(int i = 0; i < T; i++){
                if(used[i] == 0 || used[i] / numrockets < 0.5){
                    if(rockets[i].price < lowestcost){
                        lowestindex = i;
                        lowestcost = rockets[i].price;
                    }
                }else{
                    valid = false;
                    //break;
                }
            }

            if(!valid){
                currentscore += rockets[lowestindex].score;
                currentprice += rockets[lowestindex].price;
                ++used[lowestindex];
                ++numrockets;
            }
            valid = true;
            lowestcost = INT_MAX, lowestindex = 0;
            for(int i = 0; i < T; i++){
                if(used[i] == 0 || used[i] / numrockets < 0.5){
                    if(rockets[i].price < lowestcost){
                        lowestindex = i;
                        lowestcost = rockets[i].price;
                    }
                }else{
                    valid = false;
                    //break;
                }
            }
            if(!valid){
                currentscore += rockets[lowestindex].score;
                currentprice += rockets[lowestindex].price;
                ++used[lowestindex];
                ++numrockets;
            }
            std::cout << currentprice << std::endl;
            return 0;
        }
        for(int i = 0; i < T; i++){
            if(used[i] == 0 || used[i] / numrockets < 0.5){
                currentprice += rockets[i].price;
                currentscore += rockets[i].score;
                ++used[i];
                ++numrockets;
                break;
            }
        }
    }*/
    //std::cout << "Before main end"<<std::endl;
    return -1;
}
/*
6 51
48 1
1 1
1 1
2 2
3 2
20 10

res: 3
Den här blir fel
*/
/*
6 10
9 6
1 1
1 1
1 1
1 1
2 1

2*3 (6)  (3)
1*4 (10) (7)


20 200
13 31
47 124
38 98
123 42
23 4
23 124
34 56
12 87
98 72
64 78
98 77
72 83
76 53
63 56
32 23
12 24
42 74
91 87
54 25
3 1

20 200
100 1000
2 1
*/
