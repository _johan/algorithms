#include <iostream>
#include <string>

char S = 'S';
char N = 'N';
char B = 'B';
char SCHOOL = 'V';

char findNext(std::string s, int i){
	for (--i; i >= 0; --i){
		if (s[i] != B){
			return s[i];
		}
	}
}

int main(){
	std::string road;
	std::cin >> road;
	road.insert(road.begin(), SCHOOL);
	bool north = true;
	int crossedroads = 0;
	for (int i = road.size() - 1; i >= 0; --i){
		if (road[i] == SCHOOL){
			if (!north){
				crossedroads++;
				break;
			}
			break;
		}
		if (road[i] == B){
			crossedroads++;
			continue;
		}
		if (north){
			if (road[i] == N){
				if (findNext(road, i) == N){
					//Tj�nar p� att byta
					north = false;
					crossedroads++;
				}
				else{
					crossedroads++;
				}
			}
		}
		else{
			if (road[i] == S){
				if (findNext(road, i) == S || findNext(road, i) == SCHOOL){
					north = true;
					crossedroads++;
				}
				else{
					crossedroads++;
				}
			}
		}
	}
	std::cout << crossedroads << std::endl;
#ifdef WIN32
	system("PAUSE");
#endif
}


/*
SBSNNBSNNSSSNNNB
*/
/*
 | ||| ||   ||||
----------------
|||  ||  |||   |
*/

