#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
#include <climits>
#include <set>
#include <iomanip>
#include <limits>

typedef std::pair<int,int> pii;
typedef std::vector<pii> vpii;
typedef std::vector<int> vi;
typedef std::vector<vi> vvi;
typedef std::vector<bool> vb;
typedef std::vector<double> vd;
typedef std::vector<vd> vvd;
typedef std::vector<std::pair<double,double>> vpdd;
using namespace std;
int N;
double d;

struct Edge{
    int lhs, rhs;
    double cost = INT_MAX;
    bool available = false;
    Edge(){}
    //Edge(int l, int r, double _k, double _m) : lhs(l), rhs(r), k(_k), m(_m){}
    //Edge(int l, int r, double lx, double ly, double rx, double ry, bool av) : lhs(l), rhs(r), lhsx(lx), lhsy(ly), rhsx(rx), rhsy(ry), available(av){}
    Edge(int l, int r, double _cost) : lhs(l), rhs(r), cost(_cost){}
};

vector<vector<Edge>> edges;
vpdd coords;
vpdd eq;
vb available;
vd dp;

double Do(int pos, double len){
    if(pos >= N){
        return len;
    }
    if(dp[pos] != -1){
        return dp[pos];
    }

    double lowest = INT_MAX;
    for(int i = pos; i < N; i++){
        for(int j = i; j < N; j++){
            if(edges[i][j].lhs == i && (edges[i][j].lhs != edges[i][j].rhs) && edges[i][j].available){
                //Pick edge
                lowest = min(lowest, Do(edges[i][j].rhs, len + edges[i][j].cost));
                //Disregard edge
            }
        }
    }
    return dp[pos] = lowest;
}

int main()
{
    std::cout.precision(std::numeric_limits< double >::max_digits10);
    cin >> N >> d;
    coords = vpdd(N);
    eq = vpdd(N);
    available = vb(N, true);
    //dp = vvd(N, vd(N, -1));
    dp = vd(N, -1);
    edges = vector<vector<Edge>>(N, vector<Edge>(N));
    for(int i = 0; i < N; i++){
        cin >> coords[i].first >> coords[i].second;
        if(i > 0){
            double k = (coords[i].second - coords[i-1].second) / (coords[i].first - coords[i-1].first);
            eq[i-1] = make_pair(k, coords[i].second - k * coords[i].first);
            if(abs(k) > d){
                available[i-1] = false;
                if(k > 0 && i > 1){
                    available[i-2] = false;
                }
            }else{
                edges[i-1][i] = Edge(i-1, i, 0);
                edges[i-1][i].available = true;

            }
        }
    }
    vector<std::pair<double,double>> graph(N, std::make_pair(INT_MAX, 0));
    vb visited(N, false);
    for(int i = 0; i < N; i++){
        double highest = 0;
        graph[i].second = i;
        for(int j = i+1; j < N; j++){
            //Generate edge
            //if(coords[j].second >= highest){
                double k = (coords[i].second - coords[j].second) / (coords[i].first - coords[j].first);
                //Do all basic checks first
                if(abs(k) > d || edges[i][j].cost == 0){
                    //highest = max(highest, coords[j].second);
                    continue;
                }
                if(i < N && eq[i].first > k){
                    continue;
                }
                bool cont = false;
                double m = coords[i].second - k * coords[i].first;
                for(int c = i+1; c < j; ++c){
                    //Check intersects
                    // { (k2 + m2 - m1) / k1 } och kolla om den är i intervallet
                    double k2 = eq[c-1].first, m2 = eq[c-1].second;
                    double inter = (m2 - m) / (k - k2), yinter = k * inter + m;//(k2 * coords[c].first + m2 - m) / k
                    if(inter > coords[i].first && inter < coords[j].first && yinter <= coords[c].second){
                        cont = true;
                        break;
                    }
                }
                /*
Just nu kan den gå under berget... How I solve?
*/
                if(cont){
                    continue;
                }

                edges[i][j] = Edge(i,j, sqrt((coords[i].second - coords[j].second) * (coords[i].second - coords[j].second) + (coords[i].first - coords[j].first) * (coords[i].first - coords[j].first)));
                edges[i][j].available = true;
                //double m = coords[i].second - k * coords[i].first;
                //edges[i][j] = Edge(i,j, coords[i].first, coords[i].second, coords[j].first, coords[j].second, true);
            //}
            //highest = max(highest, coords[j].second);
        }
    }
    /*
First generate ALL edges, and then simply go and pick so that I get from A to B
*/
    set<std::pair<double,double>> q;
    graph[0].first = 0;

    q.insert(graph[0]);
    /*if(!available.front()){
        cout << -1 << endl;
        return 0;
    }*/

    std::vector<double> prev(N, -1);
    while(!q.empty()){
        std::pair<double,double> cur = *q.begin();
        visited[cur.second] = true;
        q.erase(q.begin());
        //std::cout << "Now node: " << cur.second << endl;

        for(int i = 0; i < edges[cur.second].size(); i++){
            if(!visited[edges[cur.second][i].rhs] && cur.first + edges[cur.second][i].cost < graph[edges[cur.second][i].rhs].first && edges[cur.second][i].available){
                std::set<std::pair<double,double>>::iterator it = q.find(std::make_pair(graph[edges[cur.second][i].rhs].first, edges[cur.second][i].rhs));
                if(it != q.end()){
                    q.erase(it);
                    q.insert(std::make_pair(edges[cur.second][i].cost + cur.first, edges[cur.second][i].rhs));
                    graph[edges[cur.second][i].rhs].first = edges[cur.second][i].cost + cur.first;
                    prev[edges[cur.second][i].rhs] = cur.second;
                }else{
                    //Add this to queue
                    q.insert(std::make_pair(edges[cur.second][i].cost + cur.first, edges[cur.second][i].rhs));
                    graph[edges[cur.second][i].rhs].first = edges[cur.second][i].cost + cur.first;
                    prev[edges[cur.second][i].rhs] = cur.second;
                }
            }
        }
    }
    if(graph.back().first >= INT_MAX){
        std::cout << -1 << std::endl;
        return 0;
    }
    int index = N-1;
   /* while(index != -1){
        std::cout << prev[index] << std::endl;
        index = prev[index];
    }*/
    std::cout << fixed << graph.back().first << std::endl;
    /*std::cout << "edgeinfo::\n\n";
    for(int y = 0; y < N; y++){
        std::cout << "Edge: " << y << "\n";
        int nedges = 0;
        for(int x = 0; x < N; x++){
            if(edges[y][x].available){
                std::cout << "Edge from: " << edges[y][x].lhs <<" to " << edges[y][x].rhs << ", cost: " << edges[y][x].cost << std::endl;
                ++nedges;
            }
        }
        std::cout << "Number of edges: " << nedges << "\n\n";
    }*/
}

