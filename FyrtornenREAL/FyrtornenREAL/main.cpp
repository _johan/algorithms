#include <iostream>
#include <vector>
#include <map>
#include <set>
using namespace std;

int main(){
	//y, x
	map<int, set<int>> gb;
	int N, M, K;
	cin >> N >> M >> K;
	for (int i = 0; i < M; ++i){
		int x, y;
		cin >> x >> y;
		gb[y].insert(x);
	}
	if (K >= M){
		cout << M << endl;
		return 0;
	}
}