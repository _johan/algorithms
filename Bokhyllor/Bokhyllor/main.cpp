#include <iostream>
#include <vector>
#include <algorithm>

int main(){
	int small, middle, big, size;
	std::cin >> small >> middle >> big >> size;
	int tot = 0;
	while (big > 0){
		tot++;
		int bsize = size;
		int amt = std::min(size / 3, big);
		bsize -= amt * 3;
		big -= amt;

		amt = std::min(bsize / 2, middle);
		bsize -= amt * 2;
		middle -= amt;

		amt = std::min(bsize, small);
		bsize -= amt;
		small -= amt;
	}

	while (middle > 0){
		tot++;
		int bsize = size;
		int amt = std::min(size / 2, middle);
		bsize -= amt * 2;
		middle -= amt;

		amt = std::min(bsize, small);
		bsize -= amt;
		small -= amt;
	}

	while (small > 0){
		tot++;
		int bsize = size;
		int amt = std::min(size, small);
		bsize -= amt;
		small -= amt;
	}

	std::cout << tot << std::endl;
#ifdef WIN32
	system("PAUSE");
#endif
}