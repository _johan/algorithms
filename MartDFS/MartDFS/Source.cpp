#include <iostream>
#include <string>
#include <vector>

typedef unsigned int uint;
typedef long long ll;

struct Edge{
	Edge(){}
	Edge(uint __a, uint __b){
		a = __a;
		b = __b;
	}
	uint a;
	uint b;
};

void DFSTest(std::vector<uint> &in, std::vector<bool> &vis, std::vector<std::vector<uint>> &e, uint pos, uint &inpos){
	inpos++;
	if (inpos >= in.size() || pos >= e.size()){
		vis[pos] = true;
		return;
	}

	
    vis[pos] = true;
    for (uint i = 0; i < e[pos].size(); i++){
		if (inpos >= in.size()){
			return;
		}
            if (!vis[e[pos][i]] && in[inpos] == e[pos][i]){
                    //vis[pos] = true;
                    DFSTest(in, vis, e, e[pos][i], inpos);
                    i = 0;
            }
    }
}

/*
 * couchtuner.la
}*/
int main(){
#ifndef WIN32
	std::ios_base::sync_with_stdio(false);
	std::cin.tie(NULL);
#endif
	uint numN, numE;
	std::cin >> numN >> numE;
	std::vector<bool> vis(numN, false);
	std::vector < std::vector<uint>> e(numN);
	for (uint i = 0; i < numE; i++){
		uint a, b;
		std::cin >> a >> b;
		e[a].push_back(b);
		e[b].push_back(a);
	}

	std::vector<uint> in(numN);
	std::string input;
	//std::cin >> input;
	std::cin.ignore();
	std::getline(std::cin, input);
	for (uint i = 0, it = 0; i < input.size(); i++){
		if (input[i] != ' '){
			if (it == in.size()){
				in.push_back(input[i] - '0');
			}
			else{
				in[it] = input[i] - '0';
                        }
                        it++;
                }
        }
	if (in.size() != numN){
		std::cout << "NO" << std::endl;
#ifdef WIN32
		system("PAUSE");
#endif
		return 0;
	}
        uint inpos = 0;
        DFSTest(in, vis, e, in[0], inpos);
        for(uint i = 0; i < vis.size(); i++){
            if(!vis[i]){
                std::cout << "NO" << std::endl;
#ifdef WIN32
				system("PAUSE");
#endif
                return 0;
            }
        }
	std::cout << "YES" << std::endl;
#ifdef WIN32
	system("PAUSE");
#endif

}
