#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <algorithm>

using namespace std;
string konamikod = "UUNNVHVHBA";
string in;
vector < vector<pair<int, int>>> pairs;
//UU, NN, VH, BA

int main(){
	cin >> in;
	pairs.resize(4);
	for (int i = 0; i < 4; ++i){
		pairs[i].reserve(in.size());
	}
	int start = 0;
	bool foundfirst = false;
	for (int i = 0; i < in.size(); ++i){
		if (in[i] == 'U' && !foundfirst){
			foundfirst = true;
			start = i;
		}
		else if (in[i] == 'U' && foundfirst){
			pairs[0].push_back(make_pair(start, i - start-1));
			start = i;
		}
	}
	foundfirst = false;
	start = 0;
	for (int i = 0; i < in.size(); ++i){
		if (in[i] == 'N' && !foundfirst){
			foundfirst = true;
			start = i;
		}
		else if (in[i] == 'N' && foundfirst){
			pairs[1].push_back(make_pair(start, i - start - 1));
			start = i;
		}
	}
	foundfirst = false;
	start = 0;
	vector<int> pos;
	pos.reserve(in.size());
	for (int i = 0; i < in.size(); ++i){
		if (in[i] == 'V'){
			start = i;
		}
		else if (in[i] == 'H' && start != -1){
			pairs[2].push_back(make_pair(start, i - start - 1));
			start = -1;
		}
	}
	foundfirst = false;
	start = 0;
	for (int i = 0; i < in.size(); ++i){
		if (in[i] == 'B'){
			start = i;
		}
		else if (in[i] == 'A' && start != -1){
			pairs[3].push_back(make_pair(start, i - start - 1));
			start = -1;
		}
	}
	int minK = 100000000;
	for (int i = 0; i < pairs[0].size(); ++i){
		int cK = pairs[0][i].second;
		int end = pairs[0][i].first + pairs[0][i].second+1;
		std::vector<pair<int, int>>::iterator it = lower_bound(pairs[1].begin(), pairs[1].end(), make_pair(end+1, 0));
		if (it == pairs[1].end())
			continue;

		cK += it->second + (it->first - end - 1);
		end = it->second + it->first+1;

		it = lower_bound(pairs[2].begin(), pairs[2].end(), make_pair(end+1, 0));
		if (it == pairs[2].end())
			continue;
		
		cK += it->second + (it->first - end - 1);
		end = it->second + it->first+1;

		it = lower_bound(pairs[2].begin(), pairs[2].end(), make_pair(end+1, 0));
		if (it == pairs[2].end())
			continue;

		cK += it->second + (it->first - end - 1);
		end = it->second + it->first+1;

		it = lower_bound(pairs[3].begin(), pairs[3].end(), make_pair(end+1, 0));
		if (it == pairs[3].end())
			continue;

		cK += it->second + (it->first - end - 1);
		end = it->second + it->first+1;

		minK = min(minK, cK);
	}
	cout << minK << endl;
#ifdef WIN32
	system("PAUSE");
#endif
}
//VHUUUUNNVUUHVHBA
//UUUUUUUUUUNVNVBBHVHBA
//BBVUUNVNVBBHVHBBAAB

//UUNVBBNHVHVHBA

//UUBBBBNNVHVHBA