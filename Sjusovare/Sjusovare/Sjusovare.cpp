//#include "stdafx.h"

#include <iostream>
#include <vector>
#include <set>
typedef unsigned int uint;

struct TimeNode{
	TimeNode(){}
	TimeNode(uint hh, uint mm, uint ns, uint ts){
		hour = hh;
		minute = mm;
		this->ns = ns;
		this->ts = ts;
	}
	void set(uint hh, uint mm, uint ns, uint ts){
		hour = hh;
		minute = mm;
		this->ns = ns;
		this->ts = ts;
	}


	unsigned int hour;
	unsigned int minute;
	unsigned int ns;
	unsigned int ts;
};


int main(){
	unsigned int numberPeople = 0;
	std::cin >> numberPeople;
	std::vector<TimeNode> tNodes(numberPeople);
	std::set<int> indexes;
	for (unsigned int i = 0; i < numberPeople; i++){
		uint hh, mm, ns, ts;
		std::cin >> hh >> mm >> ns >> ts;
		tNodes[i].set(hh, mm, ns, ts);
	}
	unsigned int index = 1;
	for (TimeNode tn : tNodes){
		//for (unsigned int i = 0; i < tn.ns; i++){
		//std::cout << "tn minute before: " << tn.minute << ", timesnooze: " << tn.ts << ", numbersnooze: " << tn.ns << std::endl;
			tn.minute += tn.ts * tn.ns;
			//std::cout << "tn minute after: " << tn.minute << std::endl;
			if (tn.minute >= 60){
				//std::cout << "in if" << std::endl;
				//Add in all the hours there
				uint nAdd = tn.minute / 60;
				//std::cout << nAdd << std::endl;
				tn.hour += nAdd;
				tn.minute = (tn.minute - nAdd * 60);
			}
		//}
			if (tn.hour >= 7){
				indexes.insert(index);
			}
			index++;
	}
	for (uint i : indexes){
		std::cout << i << std::endl;
	}
	//system("PAUSE");
}